package com.quix.aspireloan.web.rest;

import com.quix.aspireloan.AspireloanApp;

import com.quix.aspireloan.domain.ReferralStatus;
import com.quix.aspireloan.repository.ReferralStatusRepository;
import com.quix.aspireloan.service.dto.ReferralStatusDTO;
import com.quix.aspireloan.service.mapper.ReferralStatusMapper;
import com.quix.aspireloan.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.quix.aspireloan.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ReferralStatusResource REST controller.
 *
 * @see ReferralStatusResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AspireloanApp.class)
public class ReferralStatusResourceIntTest {

    private static final Integer DEFAULT_APPLICATIONID = 1;
    private static final Integer UPDATED_APPLICATIONID = 2;

    private static final Integer DEFAULT_REFERRALAMOUNT = 1;
    private static final Integer UPDATED_REFERRALAMOUNT = 2;

    private static final LocalDate DEFAULT_VALIDITYFROMDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VALIDITYFROMDATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_VALIDITYTODATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VALIDITYTODATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_LASTREMARK = "AAAAAAAAAA";
    private static final String UPDATED_LASTREMARK = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_APPLYDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_APPLYDATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_UPDATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private ReferralStatusRepository referralStatusRepository;

    @Autowired
    private ReferralStatusMapper referralStatusMapper;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restReferralStatusMockMvc;

    private ReferralStatus referralStatus;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ReferralStatusResource referralStatusResource = new ReferralStatusResource(referralStatusRepository, referralStatusMapper);
        this.restReferralStatusMockMvc = MockMvcBuilders.standaloneSetup(referralStatusResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ReferralStatus createEntity(EntityManager em) {
        ReferralStatus referralStatus = new ReferralStatus()
            .applicationid(DEFAULT_APPLICATIONID)
            .referralamount(DEFAULT_REFERRALAMOUNT)
            .validityfromdate(DEFAULT_VALIDITYFROMDATE)
            .validitytodate(DEFAULT_VALIDITYTODATE)
            .status(DEFAULT_STATUS)
            .lastremark(DEFAULT_LASTREMARK)
            .applydate(DEFAULT_APPLYDATE)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .updatedBy(DEFAULT_UPDATED_BY)
            .updatedDate(DEFAULT_UPDATED_DATE);
        return referralStatus;
    }

    @Before
    public void initTest() {
        referralStatus = createEntity(em);
    }

    @Test
    @Transactional
    public void createReferralStatus() throws Exception {
        int databaseSizeBeforeCreate = referralStatusRepository.findAll().size();

        // Create the ReferralStatus
        ReferralStatusDTO referralStatusDTO = referralStatusMapper.toDto(referralStatus);
        restReferralStatusMockMvc.perform(post("/api/referral-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(referralStatusDTO)))
            .andExpect(status().isCreated());

        // Validate the ReferralStatus in the database
        List<ReferralStatus> referralStatusList = referralStatusRepository.findAll();
        assertThat(referralStatusList).hasSize(databaseSizeBeforeCreate + 1);
        ReferralStatus testReferralStatus = referralStatusList.get(referralStatusList.size() - 1);
        assertThat(testReferralStatus.getApplicationid()).isEqualTo(DEFAULT_APPLICATIONID);
        assertThat(testReferralStatus.getReferralamount()).isEqualTo(DEFAULT_REFERRALAMOUNT);
        assertThat(testReferralStatus.getValidityfromdate()).isEqualTo(DEFAULT_VALIDITYFROMDATE);
        assertThat(testReferralStatus.getValiditytodate()).isEqualTo(DEFAULT_VALIDITYTODATE);
        assertThat(testReferralStatus.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testReferralStatus.getLastremark()).isEqualTo(DEFAULT_LASTREMARK);
        assertThat(testReferralStatus.getApplydate()).isEqualTo(DEFAULT_APPLYDATE);
        assertThat(testReferralStatus.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testReferralStatus.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testReferralStatus.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
        assertThat(testReferralStatus.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void createReferralStatusWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = referralStatusRepository.findAll().size();

        // Create the ReferralStatus with an existing ID
        referralStatus.setId(1L);
        ReferralStatusDTO referralStatusDTO = referralStatusMapper.toDto(referralStatus);

        // An entity with an existing ID cannot be created, so this API call must fail
        restReferralStatusMockMvc.perform(post("/api/referral-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(referralStatusDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ReferralStatus in the database
        List<ReferralStatus> referralStatusList = referralStatusRepository.findAll();
        assertThat(referralStatusList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllReferralStatuses() throws Exception {
        // Initialize the database
        referralStatusRepository.saveAndFlush(referralStatus);

        // Get all the referralStatusList
        restReferralStatusMockMvc.perform(get("/api/referral-statuses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(referralStatus.getId().intValue())))
            .andExpect(jsonPath("$.[*].applicationid").value(hasItem(DEFAULT_APPLICATIONID)))
            .andExpect(jsonPath("$.[*].referralamount").value(hasItem(DEFAULT_REFERRALAMOUNT)))
            .andExpect(jsonPath("$.[*].validityfromdate").value(hasItem(DEFAULT_VALIDITYFROMDATE.toString())))
            .andExpect(jsonPath("$.[*].validitytodate").value(hasItem(DEFAULT_VALIDITYTODATE.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].lastremark").value(hasItem(DEFAULT_LASTREMARK.toString())))
            .andExpect(jsonPath("$.[*].applydate").value(hasItem(DEFAULT_APPLYDATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    @Transactional
    public void getReferralStatus() throws Exception {
        // Initialize the database
        referralStatusRepository.saveAndFlush(referralStatus);

        // Get the referralStatus
        restReferralStatusMockMvc.perform(get("/api/referral-statuses/{id}", referralStatus.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(referralStatus.getId().intValue()))
            .andExpect(jsonPath("$.applicationid").value(DEFAULT_APPLICATIONID))
            .andExpect(jsonPath("$.referralamount").value(DEFAULT_REFERRALAMOUNT))
            .andExpect(jsonPath("$.validityfromdate").value(DEFAULT_VALIDITYFROMDATE.toString()))
            .andExpect(jsonPath("$.validitytodate").value(DEFAULT_VALIDITYTODATE.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.lastremark").value(DEFAULT_LASTREMARK.toString()))
            .andExpect(jsonPath("$.applydate").value(DEFAULT_APPLYDATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY.toString()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingReferralStatus() throws Exception {
        // Get the referralStatus
        restReferralStatusMockMvc.perform(get("/api/referral-statuses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateReferralStatus() throws Exception {
        // Initialize the database
        referralStatusRepository.saveAndFlush(referralStatus);
        int databaseSizeBeforeUpdate = referralStatusRepository.findAll().size();

        // Update the referralStatus
        ReferralStatus updatedReferralStatus = referralStatusRepository.findOne(referralStatus.getId());
        updatedReferralStatus
            .applicationid(UPDATED_APPLICATIONID)
            .referralamount(UPDATED_REFERRALAMOUNT)
            .validityfromdate(UPDATED_VALIDITYFROMDATE)
            .validitytodate(UPDATED_VALIDITYTODATE)
            .status(UPDATED_STATUS)
            .lastremark(UPDATED_LASTREMARK)
            .applydate(UPDATED_APPLYDATE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE);
        ReferralStatusDTO referralStatusDTO = referralStatusMapper.toDto(updatedReferralStatus);

        restReferralStatusMockMvc.perform(put("/api/referral-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(referralStatusDTO)))
            .andExpect(status().isOk());

        // Validate the ReferralStatus in the database
        List<ReferralStatus> referralStatusList = referralStatusRepository.findAll();
        assertThat(referralStatusList).hasSize(databaseSizeBeforeUpdate);
        ReferralStatus testReferralStatus = referralStatusList.get(referralStatusList.size() - 1);
        assertThat(testReferralStatus.getApplicationid()).isEqualTo(UPDATED_APPLICATIONID);
        assertThat(testReferralStatus.getReferralamount()).isEqualTo(UPDATED_REFERRALAMOUNT);
        assertThat(testReferralStatus.getValidityfromdate()).isEqualTo(UPDATED_VALIDITYFROMDATE);
        assertThat(testReferralStatus.getValiditytodate()).isEqualTo(UPDATED_VALIDITYTODATE);
        assertThat(testReferralStatus.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testReferralStatus.getLastremark()).isEqualTo(UPDATED_LASTREMARK);
        assertThat(testReferralStatus.getApplydate()).isEqualTo(UPDATED_APPLYDATE);
        assertThat(testReferralStatus.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testReferralStatus.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testReferralStatus.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testReferralStatus.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingReferralStatus() throws Exception {
        int databaseSizeBeforeUpdate = referralStatusRepository.findAll().size();

        // Create the ReferralStatus
        ReferralStatusDTO referralStatusDTO = referralStatusMapper.toDto(referralStatus);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restReferralStatusMockMvc.perform(put("/api/referral-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(referralStatusDTO)))
            .andExpect(status().isCreated());

        // Validate the ReferralStatus in the database
        List<ReferralStatus> referralStatusList = referralStatusRepository.findAll();
        assertThat(referralStatusList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteReferralStatus() throws Exception {
        // Initialize the database
        referralStatusRepository.saveAndFlush(referralStatus);
        int databaseSizeBeforeDelete = referralStatusRepository.findAll().size();

        // Get the referralStatus
        restReferralStatusMockMvc.perform(delete("/api/referral-statuses/{id}", referralStatus.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ReferralStatus> referralStatusList = referralStatusRepository.findAll();
        assertThat(referralStatusList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ReferralStatus.class);
        ReferralStatus referralStatus1 = new ReferralStatus();
        referralStatus1.setId(1L);
        ReferralStatus referralStatus2 = new ReferralStatus();
        referralStatus2.setId(referralStatus1.getId());
        assertThat(referralStatus1).isEqualTo(referralStatus2);
        referralStatus2.setId(2L);
        assertThat(referralStatus1).isNotEqualTo(referralStatus2);
        referralStatus1.setId(null);
        assertThat(referralStatus1).isNotEqualTo(referralStatus2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ReferralStatusDTO.class);
        ReferralStatusDTO referralStatusDTO1 = new ReferralStatusDTO();
        referralStatusDTO1.setId(1L);
        ReferralStatusDTO referralStatusDTO2 = new ReferralStatusDTO();
        assertThat(referralStatusDTO1).isNotEqualTo(referralStatusDTO2);
        referralStatusDTO2.setId(referralStatusDTO1.getId());
        assertThat(referralStatusDTO1).isEqualTo(referralStatusDTO2);
        referralStatusDTO2.setId(2L);
        assertThat(referralStatusDTO1).isNotEqualTo(referralStatusDTO2);
        referralStatusDTO1.setId(null);
        assertThat(referralStatusDTO1).isNotEqualTo(referralStatusDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(referralStatusMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(referralStatusMapper.fromId(null)).isNull();
    }
}
