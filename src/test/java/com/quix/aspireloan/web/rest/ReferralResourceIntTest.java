package com.quix.aspireloan.web.rest;

import com.quix.aspireloan.AspireloanApp;

import com.quix.aspireloan.domain.Referral;
import com.quix.aspireloan.repository.ReferralRepository;
import com.quix.aspireloan.service.dto.ReferralDTO;
import com.quix.aspireloan.service.mapper.ReferralMapper;
import com.quix.aspireloan.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.quix.aspireloan.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ReferralResource REST controller.
 *
 * @see ReferralResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AspireloanApp.class)
public class ReferralResourceIntTest {

    private static final Integer DEFAULT_NUMBEROFSUCCESSFULLREFERRAL = 1;
    private static final Integer UPDATED_NUMBEROFSUCCESSFULLREFERRAL = 2;

    private static final String DEFAULT_REFERRALBONUS = "AAAAAAAAAA";
    private static final String UPDATED_REFERRALBONUS = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_VALIDITYFROMDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VALIDITYFROMDATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_VALIDITYTODATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VALIDITYTODATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_UPDATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private ReferralRepository referralRepository;

    @Autowired
    private ReferralMapper referralMapper;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restReferralMockMvc;

    private Referral referral;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ReferralResource referralResource = new ReferralResource(referralRepository, referralMapper);
        this.restReferralMockMvc = MockMvcBuilders.standaloneSetup(referralResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Referral createEntity(EntityManager em) {
        Referral referral = new Referral()
            .numberofsuccessfullreferral(DEFAULT_NUMBEROFSUCCESSFULLREFERRAL)
            .referralbonus(DEFAULT_REFERRALBONUS)
            .validityfromdate(DEFAULT_VALIDITYFROMDATE)
            .validitytodate(DEFAULT_VALIDITYTODATE)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .updatedBy(DEFAULT_UPDATED_BY)
            .updatedDate(DEFAULT_UPDATED_DATE);
        return referral;
    }

    @Before
    public void initTest() {
        referral = createEntity(em);
    }

    @Test
    @Transactional
    public void createReferral() throws Exception {
        int databaseSizeBeforeCreate = referralRepository.findAll().size();

        // Create the Referral
        ReferralDTO referralDTO = referralMapper.toDto(referral);
        restReferralMockMvc.perform(post("/api/referrals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(referralDTO)))
            .andExpect(status().isCreated());

        // Validate the Referral in the database
        List<Referral> referralList = referralRepository.findAll();
        assertThat(referralList).hasSize(databaseSizeBeforeCreate + 1);
        Referral testReferral = referralList.get(referralList.size() - 1);
        assertThat(testReferral.getNumberofsuccessfullreferral()).isEqualTo(DEFAULT_NUMBEROFSUCCESSFULLREFERRAL);
        assertThat(testReferral.getReferralbonus()).isEqualTo(DEFAULT_REFERRALBONUS);
        assertThat(testReferral.getValidityfromdate()).isEqualTo(DEFAULT_VALIDITYFROMDATE);
        assertThat(testReferral.getValiditytodate()).isEqualTo(DEFAULT_VALIDITYTODATE);
        assertThat(testReferral.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testReferral.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testReferral.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
        assertThat(testReferral.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void createReferralWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = referralRepository.findAll().size();

        // Create the Referral with an existing ID
        referral.setId(1L);
        ReferralDTO referralDTO = referralMapper.toDto(referral);

        // An entity with an existing ID cannot be created, so this API call must fail
        restReferralMockMvc.perform(post("/api/referrals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(referralDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Referral in the database
        List<Referral> referralList = referralRepository.findAll();
        assertThat(referralList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllReferrals() throws Exception {
        // Initialize the database
        referralRepository.saveAndFlush(referral);

        // Get all the referralList
        restReferralMockMvc.perform(get("/api/referrals?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(referral.getId().intValue())))
            .andExpect(jsonPath("$.[*].numberofsuccessfullreferral").value(hasItem(DEFAULT_NUMBEROFSUCCESSFULLREFERRAL)))
            .andExpect(jsonPath("$.[*].referralbonus").value(hasItem(DEFAULT_REFERRALBONUS.toString())))
            .andExpect(jsonPath("$.[*].validityfromdate").value(hasItem(DEFAULT_VALIDITYFROMDATE.toString())))
            .andExpect(jsonPath("$.[*].validitytodate").value(hasItem(DEFAULT_VALIDITYTODATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    @Transactional
    public void getReferral() throws Exception {
        // Initialize the database
        referralRepository.saveAndFlush(referral);

        // Get the referral
        restReferralMockMvc.perform(get("/api/referrals/{id}", referral.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(referral.getId().intValue()))
            .andExpect(jsonPath("$.numberofsuccessfullreferral").value(DEFAULT_NUMBEROFSUCCESSFULLREFERRAL))
            .andExpect(jsonPath("$.referralbonus").value(DEFAULT_REFERRALBONUS.toString()))
            .andExpect(jsonPath("$.validityfromdate").value(DEFAULT_VALIDITYFROMDATE.toString()))
            .andExpect(jsonPath("$.validitytodate").value(DEFAULT_VALIDITYTODATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY.toString()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingReferral() throws Exception {
        // Get the referral
        restReferralMockMvc.perform(get("/api/referrals/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateReferral() throws Exception {
        // Initialize the database
        referralRepository.saveAndFlush(referral);
        int databaseSizeBeforeUpdate = referralRepository.findAll().size();

        // Update the referral
        Referral updatedReferral = referralRepository.findOne(referral.getId());
        updatedReferral
            .numberofsuccessfullreferral(UPDATED_NUMBEROFSUCCESSFULLREFERRAL)
            .referralbonus(UPDATED_REFERRALBONUS)
            .validityfromdate(UPDATED_VALIDITYFROMDATE)
            .validitytodate(UPDATED_VALIDITYTODATE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE);
        ReferralDTO referralDTO = referralMapper.toDto(updatedReferral);

        restReferralMockMvc.perform(put("/api/referrals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(referralDTO)))
            .andExpect(status().isOk());

        // Validate the Referral in the database
        List<Referral> referralList = referralRepository.findAll();
        assertThat(referralList).hasSize(databaseSizeBeforeUpdate);
        Referral testReferral = referralList.get(referralList.size() - 1);
        assertThat(testReferral.getNumberofsuccessfullreferral()).isEqualTo(UPDATED_NUMBEROFSUCCESSFULLREFERRAL);
        assertThat(testReferral.getReferralbonus()).isEqualTo(UPDATED_REFERRALBONUS);
        assertThat(testReferral.getValidityfromdate()).isEqualTo(UPDATED_VALIDITYFROMDATE);
        assertThat(testReferral.getValiditytodate()).isEqualTo(UPDATED_VALIDITYTODATE);
        assertThat(testReferral.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testReferral.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testReferral.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testReferral.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingReferral() throws Exception {
        int databaseSizeBeforeUpdate = referralRepository.findAll().size();

        // Create the Referral
        ReferralDTO referralDTO = referralMapper.toDto(referral);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restReferralMockMvc.perform(put("/api/referrals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(referralDTO)))
            .andExpect(status().isCreated());

        // Validate the Referral in the database
        List<Referral> referralList = referralRepository.findAll();
        assertThat(referralList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteReferral() throws Exception {
        // Initialize the database
        referralRepository.saveAndFlush(referral);
        int databaseSizeBeforeDelete = referralRepository.findAll().size();

        // Get the referral
        restReferralMockMvc.perform(delete("/api/referrals/{id}", referral.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Referral> referralList = referralRepository.findAll();
        assertThat(referralList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Referral.class);
        Referral referral1 = new Referral();
        referral1.setId(1L);
        Referral referral2 = new Referral();
        referral2.setId(referral1.getId());
        assertThat(referral1).isEqualTo(referral2);
        referral2.setId(2L);
        assertThat(referral1).isNotEqualTo(referral2);
        referral1.setId(null);
        assertThat(referral1).isNotEqualTo(referral2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ReferralDTO.class);
        ReferralDTO referralDTO1 = new ReferralDTO();
        referralDTO1.setId(1L);
        ReferralDTO referralDTO2 = new ReferralDTO();
        assertThat(referralDTO1).isNotEqualTo(referralDTO2);
        referralDTO2.setId(referralDTO1.getId());
        assertThat(referralDTO1).isEqualTo(referralDTO2);
        referralDTO2.setId(2L);
        assertThat(referralDTO1).isNotEqualTo(referralDTO2);
        referralDTO1.setId(null);
        assertThat(referralDTO1).isNotEqualTo(referralDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(referralMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(referralMapper.fromId(null)).isNull();
    }
}
