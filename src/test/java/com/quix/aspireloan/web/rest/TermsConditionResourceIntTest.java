package com.quix.aspireloan.web.rest;

import com.quix.aspireloan.AspireloanApp;

import com.quix.aspireloan.domain.TermsCondition;
import com.quix.aspireloan.repository.TermsConditionRepository;
import com.quix.aspireloan.service.dto.TermsConditionDTO;
import com.quix.aspireloan.service.mapper.TermsConditionMapper;
import com.quix.aspireloan.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.quix.aspireloan.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TermsConditionResource REST controller.
 *
 * @see TermsConditionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AspireloanApp.class)
public class TermsConditionResourceIntTest {

    private static final LocalDate DEFAULT_SUBMISSIONDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_SUBMISSIONDATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_TERMS_AND_CONDITIONS = "AAAAAAAAAA";
    private static final String UPDATED_TERMS_AND_CONDITIONS = "BBBBBBBBBB";

    private static final String DEFAULT_VERSIONNUMBER = "AAAAAAAAAA";
    private static final String UPDATED_VERSIONNUMBER = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_FROM_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_FROM_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_TO_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TO_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_UPDATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private TermsConditionRepository termsConditionRepository;

    @Autowired
    private TermsConditionMapper termsConditionMapper;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTermsConditionMockMvc;

    private TermsCondition termsCondition;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TermsConditionResource termsConditionResource = new TermsConditionResource(termsConditionRepository, termsConditionMapper);
        this.restTermsConditionMockMvc = MockMvcBuilders.standaloneSetup(termsConditionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TermsCondition createEntity(EntityManager em) {
        TermsCondition termsCondition = new TermsCondition()
            .submissiondate(DEFAULT_SUBMISSIONDATE)
            .termsAndConditions(DEFAULT_TERMS_AND_CONDITIONS)
            .versionnumber(DEFAULT_VERSIONNUMBER)
            .fromDate(DEFAULT_FROM_DATE)
            .toDate(DEFAULT_TO_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .updatedBy(DEFAULT_UPDATED_BY)
            .updatedDate(DEFAULT_UPDATED_DATE);
        return termsCondition;
    }

    @Before
    public void initTest() {
        termsCondition = createEntity(em);
    }

    @Test
    @Transactional
    public void createTermsCondition() throws Exception {
        int databaseSizeBeforeCreate = termsConditionRepository.findAll().size();

        // Create the TermsCondition
        TermsConditionDTO termsConditionDTO = termsConditionMapper.toDto(termsCondition);
        restTermsConditionMockMvc.perform(post("/api/terms-conditions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(termsConditionDTO)))
            .andExpect(status().isCreated());

        // Validate the TermsCondition in the database
        List<TermsCondition> termsConditionList = termsConditionRepository.findAll();
        assertThat(termsConditionList).hasSize(databaseSizeBeforeCreate + 1);
        TermsCondition testTermsCondition = termsConditionList.get(termsConditionList.size() - 1);
        assertThat(testTermsCondition.getSubmissiondate()).isEqualTo(DEFAULT_SUBMISSIONDATE);
        assertThat(testTermsCondition.getTermsAndConditions()).isEqualTo(DEFAULT_TERMS_AND_CONDITIONS);
        assertThat(testTermsCondition.getVersionnumber()).isEqualTo(DEFAULT_VERSIONNUMBER);
        assertThat(testTermsCondition.getFromDate()).isEqualTo(DEFAULT_FROM_DATE);
        assertThat(testTermsCondition.getToDate()).isEqualTo(DEFAULT_TO_DATE);
        assertThat(testTermsCondition.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testTermsCondition.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testTermsCondition.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
        assertThat(testTermsCondition.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void createTermsConditionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = termsConditionRepository.findAll().size();

        // Create the TermsCondition with an existing ID
        termsCondition.setId(1L);
        TermsConditionDTO termsConditionDTO = termsConditionMapper.toDto(termsCondition);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTermsConditionMockMvc.perform(post("/api/terms-conditions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(termsConditionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TermsCondition in the database
        List<TermsCondition> termsConditionList = termsConditionRepository.findAll();
        assertThat(termsConditionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllTermsConditions() throws Exception {
        // Initialize the database
        termsConditionRepository.saveAndFlush(termsCondition);

        // Get all the termsConditionList
        restTermsConditionMockMvc.perform(get("/api/terms-conditions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(termsCondition.getId().intValue())))
            .andExpect(jsonPath("$.[*].submissiondate").value(hasItem(DEFAULT_SUBMISSIONDATE.toString())))
            .andExpect(jsonPath("$.[*].termsAndConditions").value(hasItem(DEFAULT_TERMS_AND_CONDITIONS.toString())))
            .andExpect(jsonPath("$.[*].versionnumber").value(hasItem(DEFAULT_VERSIONNUMBER.toString())))
            .andExpect(jsonPath("$.[*].fromDate").value(hasItem(DEFAULT_FROM_DATE.toString())))
            .andExpect(jsonPath("$.[*].toDate").value(hasItem(DEFAULT_TO_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    @Transactional
    public void getTermsCondition() throws Exception {
        // Initialize the database
        termsConditionRepository.saveAndFlush(termsCondition);

        // Get the termsCondition
        restTermsConditionMockMvc.perform(get("/api/terms-conditions/{id}", termsCondition.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(termsCondition.getId().intValue()))
            .andExpect(jsonPath("$.submissiondate").value(DEFAULT_SUBMISSIONDATE.toString()))
            .andExpect(jsonPath("$.termsAndConditions").value(DEFAULT_TERMS_AND_CONDITIONS.toString()))
            .andExpect(jsonPath("$.versionnumber").value(DEFAULT_VERSIONNUMBER.toString()))
            .andExpect(jsonPath("$.fromDate").value(DEFAULT_FROM_DATE.toString()))
            .andExpect(jsonPath("$.toDate").value(DEFAULT_TO_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY.toString()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTermsCondition() throws Exception {
        // Get the termsCondition
        restTermsConditionMockMvc.perform(get("/api/terms-conditions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTermsCondition() throws Exception {
        // Initialize the database
        termsConditionRepository.saveAndFlush(termsCondition);
        int databaseSizeBeforeUpdate = termsConditionRepository.findAll().size();

        // Update the termsCondition
        TermsCondition updatedTermsCondition = termsConditionRepository.findOne(termsCondition.getId());
        updatedTermsCondition
            .submissiondate(UPDATED_SUBMISSIONDATE)
            .termsAndConditions(UPDATED_TERMS_AND_CONDITIONS)
            .versionnumber(UPDATED_VERSIONNUMBER)
            .fromDate(UPDATED_FROM_DATE)
            .toDate(UPDATED_TO_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE);
        TermsConditionDTO termsConditionDTO = termsConditionMapper.toDto(updatedTermsCondition);

        restTermsConditionMockMvc.perform(put("/api/terms-conditions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(termsConditionDTO)))
            .andExpect(status().isOk());

        // Validate the TermsCondition in the database
        List<TermsCondition> termsConditionList = termsConditionRepository.findAll();
        assertThat(termsConditionList).hasSize(databaseSizeBeforeUpdate);
        TermsCondition testTermsCondition = termsConditionList.get(termsConditionList.size() - 1);
        assertThat(testTermsCondition.getSubmissiondate()).isEqualTo(UPDATED_SUBMISSIONDATE);
        assertThat(testTermsCondition.getTermsAndConditions()).isEqualTo(UPDATED_TERMS_AND_CONDITIONS);
        assertThat(testTermsCondition.getVersionnumber()).isEqualTo(UPDATED_VERSIONNUMBER);
        assertThat(testTermsCondition.getFromDate()).isEqualTo(UPDATED_FROM_DATE);
        assertThat(testTermsCondition.getToDate()).isEqualTo(UPDATED_TO_DATE);
        assertThat(testTermsCondition.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testTermsCondition.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testTermsCondition.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testTermsCondition.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingTermsCondition() throws Exception {
        int databaseSizeBeforeUpdate = termsConditionRepository.findAll().size();

        // Create the TermsCondition
        TermsConditionDTO termsConditionDTO = termsConditionMapper.toDto(termsCondition);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTermsConditionMockMvc.perform(put("/api/terms-conditions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(termsConditionDTO)))
            .andExpect(status().isCreated());

        // Validate the TermsCondition in the database
        List<TermsCondition> termsConditionList = termsConditionRepository.findAll();
        assertThat(termsConditionList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTermsCondition() throws Exception {
        // Initialize the database
        termsConditionRepository.saveAndFlush(termsCondition);
        int databaseSizeBeforeDelete = termsConditionRepository.findAll().size();

        // Get the termsCondition
        restTermsConditionMockMvc.perform(delete("/api/terms-conditions/{id}", termsCondition.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<TermsCondition> termsConditionList = termsConditionRepository.findAll();
        assertThat(termsConditionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TermsCondition.class);
        TermsCondition termsCondition1 = new TermsCondition();
        termsCondition1.setId(1L);
        TermsCondition termsCondition2 = new TermsCondition();
        termsCondition2.setId(termsCondition1.getId());
        assertThat(termsCondition1).isEqualTo(termsCondition2);
        termsCondition2.setId(2L);
        assertThat(termsCondition1).isNotEqualTo(termsCondition2);
        termsCondition1.setId(null);
        assertThat(termsCondition1).isNotEqualTo(termsCondition2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TermsConditionDTO.class);
        TermsConditionDTO termsConditionDTO1 = new TermsConditionDTO();
        termsConditionDTO1.setId(1L);
        TermsConditionDTO termsConditionDTO2 = new TermsConditionDTO();
        assertThat(termsConditionDTO1).isNotEqualTo(termsConditionDTO2);
        termsConditionDTO2.setId(termsConditionDTO1.getId());
        assertThat(termsConditionDTO1).isEqualTo(termsConditionDTO2);
        termsConditionDTO2.setId(2L);
        assertThat(termsConditionDTO1).isNotEqualTo(termsConditionDTO2);
        termsConditionDTO1.setId(null);
        assertThat(termsConditionDTO1).isNotEqualTo(termsConditionDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(termsConditionMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(termsConditionMapper.fromId(null)).isNull();
    }
}
