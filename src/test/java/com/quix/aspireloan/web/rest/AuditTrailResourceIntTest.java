package com.quix.aspireloan.web.rest;

import com.quix.aspireloan.AspireloanApp;

import com.quix.aspireloan.domain.AuditTrail;
import com.quix.aspireloan.repository.AuditTrailRepository;
import com.quix.aspireloan.service.dto.AuditTrailDTO;
import com.quix.aspireloan.service.mapper.AuditTrailMapper;
import com.quix.aspireloan.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.quix.aspireloan.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AuditTrailResource REST controller.
 *
 * @see AuditTrailResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AspireloanApp.class)
public class AuditTrailResourceIntTest {

    private static final String DEFAULT_USER_CODE = "AAAAAAAAAA";
    private static final String UPDATED_USER_CODE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LOG_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LOG_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_LOG_TIME = "AAAAAAAAAA";
    private static final String UPDATED_LOG_TIME = "BBBBBBBBBB";

    private static final String DEFAULT_MODULE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_MODULE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ACTION = "AAAAAAAAAA";
    private static final String UPDATED_ACTION = "BBBBBBBBBB";

    private static final String DEFAULT_ACTIONSTATUS = "AAAAAAAAAA";
    private static final String UPDATED_ACTIONSTATUS = "BBBBBBBBBB";

    private static final String DEFAULT_ITEMCODE = "AAAAAAAAAA";
    private static final String UPDATED_ITEMCODE = "BBBBBBBBBB";

    @Autowired
    private AuditTrailRepository auditTrailRepository;

    @Autowired
    private AuditTrailMapper auditTrailMapper;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAuditTrailMockMvc;

    private AuditTrail auditTrail;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AuditTrailResource auditTrailResource = new AuditTrailResource(auditTrailRepository, auditTrailMapper);
        this.restAuditTrailMockMvc = MockMvcBuilders.standaloneSetup(auditTrailResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AuditTrail createEntity(EntityManager em) {
        AuditTrail auditTrail = new AuditTrail()
            .userCode(DEFAULT_USER_CODE)
            .logDate(DEFAULT_LOG_DATE)
            .logTime(DEFAULT_LOG_TIME)
            .moduleName(DEFAULT_MODULE_NAME)
            .action(DEFAULT_ACTION)
            .actionstatus(DEFAULT_ACTIONSTATUS)
            .itemcode(DEFAULT_ITEMCODE);
        return auditTrail;
    }

    @Before
    public void initTest() {
        auditTrail = createEntity(em);
    }

    @Test
    @Transactional
    public void createAuditTrail() throws Exception {
        int databaseSizeBeforeCreate = auditTrailRepository.findAll().size();

        // Create the AuditTrail
        AuditTrailDTO auditTrailDTO = auditTrailMapper.toDto(auditTrail);
        restAuditTrailMockMvc.perform(post("/api/audit-trails")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(auditTrailDTO)))
            .andExpect(status().isCreated());

        // Validate the AuditTrail in the database
        List<AuditTrail> auditTrailList = auditTrailRepository.findAll();
        assertThat(auditTrailList).hasSize(databaseSizeBeforeCreate + 1);
        AuditTrail testAuditTrail = auditTrailList.get(auditTrailList.size() - 1);
        assertThat(testAuditTrail.getUserCode()).isEqualTo(DEFAULT_USER_CODE);
        assertThat(testAuditTrail.getLogDate()).isEqualTo(DEFAULT_LOG_DATE);
        assertThat(testAuditTrail.getLogTime()).isEqualTo(DEFAULT_LOG_TIME);
        assertThat(testAuditTrail.getModuleName()).isEqualTo(DEFAULT_MODULE_NAME);
        assertThat(testAuditTrail.getAction()).isEqualTo(DEFAULT_ACTION);
        assertThat(testAuditTrail.getActionstatus()).isEqualTo(DEFAULT_ACTIONSTATUS);
        assertThat(testAuditTrail.getItemcode()).isEqualTo(DEFAULT_ITEMCODE);
    }

    @Test
    @Transactional
    public void createAuditTrailWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = auditTrailRepository.findAll().size();

        // Create the AuditTrail with an existing ID
        auditTrail.setId(1L);
        AuditTrailDTO auditTrailDTO = auditTrailMapper.toDto(auditTrail);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAuditTrailMockMvc.perform(post("/api/audit-trails")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(auditTrailDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AuditTrail in the database
        List<AuditTrail> auditTrailList = auditTrailRepository.findAll();
        assertThat(auditTrailList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllAuditTrails() throws Exception {
        // Initialize the database
        auditTrailRepository.saveAndFlush(auditTrail);

        // Get all the auditTrailList
        restAuditTrailMockMvc.perform(get("/api/audit-trails?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(auditTrail.getId().intValue())))
            .andExpect(jsonPath("$.[*].userCode").value(hasItem(DEFAULT_USER_CODE.toString())))
            .andExpect(jsonPath("$.[*].logDate").value(hasItem(DEFAULT_LOG_DATE.toString())))
            .andExpect(jsonPath("$.[*].logTime").value(hasItem(DEFAULT_LOG_TIME.toString())))
            .andExpect(jsonPath("$.[*].moduleName").value(hasItem(DEFAULT_MODULE_NAME.toString())))
            .andExpect(jsonPath("$.[*].action").value(hasItem(DEFAULT_ACTION.toString())))
            .andExpect(jsonPath("$.[*].actionstatus").value(hasItem(DEFAULT_ACTIONSTATUS.toString())))
            .andExpect(jsonPath("$.[*].itemcode").value(hasItem(DEFAULT_ITEMCODE.toString())));
    }

    @Test
    @Transactional
    public void getAuditTrail() throws Exception {
        // Initialize the database
        auditTrailRepository.saveAndFlush(auditTrail);

        // Get the auditTrail
        restAuditTrailMockMvc.perform(get("/api/audit-trails/{id}", auditTrail.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(auditTrail.getId().intValue()))
            .andExpect(jsonPath("$.userCode").value(DEFAULT_USER_CODE.toString()))
            .andExpect(jsonPath("$.logDate").value(DEFAULT_LOG_DATE.toString()))
            .andExpect(jsonPath("$.logTime").value(DEFAULT_LOG_TIME.toString()))
            .andExpect(jsonPath("$.moduleName").value(DEFAULT_MODULE_NAME.toString()))
            .andExpect(jsonPath("$.action").value(DEFAULT_ACTION.toString()))
            .andExpect(jsonPath("$.actionstatus").value(DEFAULT_ACTIONSTATUS.toString()))
            .andExpect(jsonPath("$.itemcode").value(DEFAULT_ITEMCODE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAuditTrail() throws Exception {
        // Get the auditTrail
        restAuditTrailMockMvc.perform(get("/api/audit-trails/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAuditTrail() throws Exception {
        // Initialize the database
        auditTrailRepository.saveAndFlush(auditTrail);
        int databaseSizeBeforeUpdate = auditTrailRepository.findAll().size();

        // Update the auditTrail
        AuditTrail updatedAuditTrail = auditTrailRepository.findOne(auditTrail.getId());
        updatedAuditTrail
            .userCode(UPDATED_USER_CODE)
            .logDate(UPDATED_LOG_DATE)
            .logTime(UPDATED_LOG_TIME)
            .moduleName(UPDATED_MODULE_NAME)
            .action(UPDATED_ACTION)
            .actionstatus(UPDATED_ACTIONSTATUS)
            .itemcode(UPDATED_ITEMCODE);
        AuditTrailDTO auditTrailDTO = auditTrailMapper.toDto(updatedAuditTrail);

        restAuditTrailMockMvc.perform(put("/api/audit-trails")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(auditTrailDTO)))
            .andExpect(status().isOk());

        // Validate the AuditTrail in the database
        List<AuditTrail> auditTrailList = auditTrailRepository.findAll();
        assertThat(auditTrailList).hasSize(databaseSizeBeforeUpdate);
        AuditTrail testAuditTrail = auditTrailList.get(auditTrailList.size() - 1);
        assertThat(testAuditTrail.getUserCode()).isEqualTo(UPDATED_USER_CODE);
        assertThat(testAuditTrail.getLogDate()).isEqualTo(UPDATED_LOG_DATE);
        assertThat(testAuditTrail.getLogTime()).isEqualTo(UPDATED_LOG_TIME);
        assertThat(testAuditTrail.getModuleName()).isEqualTo(UPDATED_MODULE_NAME);
        assertThat(testAuditTrail.getAction()).isEqualTo(UPDATED_ACTION);
        assertThat(testAuditTrail.getActionstatus()).isEqualTo(UPDATED_ACTIONSTATUS);
        assertThat(testAuditTrail.getItemcode()).isEqualTo(UPDATED_ITEMCODE);
    }

    @Test
    @Transactional
    public void updateNonExistingAuditTrail() throws Exception {
        int databaseSizeBeforeUpdate = auditTrailRepository.findAll().size();

        // Create the AuditTrail
        AuditTrailDTO auditTrailDTO = auditTrailMapper.toDto(auditTrail);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAuditTrailMockMvc.perform(put("/api/audit-trails")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(auditTrailDTO)))
            .andExpect(status().isCreated());

        // Validate the AuditTrail in the database
        List<AuditTrail> auditTrailList = auditTrailRepository.findAll();
        assertThat(auditTrailList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAuditTrail() throws Exception {
        // Initialize the database
        auditTrailRepository.saveAndFlush(auditTrail);
        int databaseSizeBeforeDelete = auditTrailRepository.findAll().size();

        // Get the auditTrail
        restAuditTrailMockMvc.perform(delete("/api/audit-trails/{id}", auditTrail.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<AuditTrail> auditTrailList = auditTrailRepository.findAll();
        assertThat(auditTrailList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AuditTrail.class);
        AuditTrail auditTrail1 = new AuditTrail();
        auditTrail1.setId(1L);
        AuditTrail auditTrail2 = new AuditTrail();
        auditTrail2.setId(auditTrail1.getId());
        assertThat(auditTrail1).isEqualTo(auditTrail2);
        auditTrail2.setId(2L);
        assertThat(auditTrail1).isNotEqualTo(auditTrail2);
        auditTrail1.setId(null);
        assertThat(auditTrail1).isNotEqualTo(auditTrail2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AuditTrailDTO.class);
        AuditTrailDTO auditTrailDTO1 = new AuditTrailDTO();
        auditTrailDTO1.setId(1L);
        AuditTrailDTO auditTrailDTO2 = new AuditTrailDTO();
        assertThat(auditTrailDTO1).isNotEqualTo(auditTrailDTO2);
        auditTrailDTO2.setId(auditTrailDTO1.getId());
        assertThat(auditTrailDTO1).isEqualTo(auditTrailDTO2);
        auditTrailDTO2.setId(2L);
        assertThat(auditTrailDTO1).isNotEqualTo(auditTrailDTO2);
        auditTrailDTO1.setId(null);
        assertThat(auditTrailDTO1).isNotEqualTo(auditTrailDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(auditTrailMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(auditTrailMapper.fromId(null)).isNull();
    }
}
