package com.quix.aspireloan.web.rest;

import com.quix.aspireloan.AspireloanApp;

import com.quix.aspireloan.domain.AccessControl;
import com.quix.aspireloan.repository.AccessControlRepository;
import com.quix.aspireloan.service.dto.AccessControlDTO;
import com.quix.aspireloan.service.mapper.AccessControlMapper;
import com.quix.aspireloan.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.quix.aspireloan.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AccessControlResource REST controller.
 *
 * @see AccessControlResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AspireloanApp.class)
public class AccessControlResourceIntTest {

    private static final String DEFAULT_USER_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_USER_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_PERMISSION_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_PERMISSION_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_UPDATE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATE_DATE = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private AccessControlRepository accessControlRepository;

    @Autowired
    private AccessControlMapper accessControlMapper;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAccessControlMockMvc;

    private AccessControl accessControl;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AccessControlResource accessControlResource = new AccessControlResource(accessControlRepository, accessControlMapper);
        this.restAccessControlMockMvc = MockMvcBuilders.standaloneSetup(accessControlResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AccessControl createEntity(EntityManager em) {
        AccessControl accessControl = new AccessControl()
            .userType(DEFAULT_USER_TYPE)
            .permissionType(DEFAULT_PERMISSION_TYPE)
            .status(DEFAULT_STATUS)
            .createdBy(DEFAULT_CREATED_BY)
            .updatedBy(DEFAULT_UPDATED_BY)
            .createDate(DEFAULT_CREATE_DATE)
            .updateDate(DEFAULT_UPDATE_DATE);
        return accessControl;
    }

    @Before
    public void initTest() {
        accessControl = createEntity(em);
    }

    @Test
    @Transactional
    public void createAccessControl() throws Exception {
        int databaseSizeBeforeCreate = accessControlRepository.findAll().size();

        // Create the AccessControl
        AccessControlDTO accessControlDTO = accessControlMapper.toDto(accessControl);
        restAccessControlMockMvc.perform(post("/api/access-controls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(accessControlDTO)))
            .andExpect(status().isCreated());

        // Validate the AccessControl in the database
        List<AccessControl> accessControlList = accessControlRepository.findAll();
        assertThat(accessControlList).hasSize(databaseSizeBeforeCreate + 1);
        AccessControl testAccessControl = accessControlList.get(accessControlList.size() - 1);
        assertThat(testAccessControl.getUserType()).isEqualTo(DEFAULT_USER_TYPE);
        assertThat(testAccessControl.getPermissionType()).isEqualTo(DEFAULT_PERMISSION_TYPE);
        assertThat(testAccessControl.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testAccessControl.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testAccessControl.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
        assertThat(testAccessControl.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
        assertThat(testAccessControl.getUpdateDate()).isEqualTo(DEFAULT_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void createAccessControlWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = accessControlRepository.findAll().size();

        // Create the AccessControl with an existing ID
        accessControl.setId(1L);
        AccessControlDTO accessControlDTO = accessControlMapper.toDto(accessControl);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAccessControlMockMvc.perform(post("/api/access-controls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(accessControlDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AccessControl in the database
        List<AccessControl> accessControlList = accessControlRepository.findAll();
        assertThat(accessControlList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllAccessControls() throws Exception {
        // Initialize the database
        accessControlRepository.saveAndFlush(accessControl);

        // Get all the accessControlList
        restAccessControlMockMvc.perform(get("/api/access-controls?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(accessControl.getId().intValue())))
            .andExpect(jsonPath("$.[*].userType").value(hasItem(DEFAULT_USER_TYPE.toString())))
            .andExpect(jsonPath("$.[*].permissionType").value(hasItem(DEFAULT_PERMISSION_TYPE.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY.toString())))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].updateDate").value(hasItem(DEFAULT_UPDATE_DATE.toString())));
    }

    @Test
    @Transactional
    public void getAccessControl() throws Exception {
        // Initialize the database
        accessControlRepository.saveAndFlush(accessControl);

        // Get the accessControl
        restAccessControlMockMvc.perform(get("/api/access-controls/{id}", accessControl.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(accessControl.getId().intValue()))
            .andExpect(jsonPath("$.userType").value(DEFAULT_USER_TYPE.toString()))
            .andExpect(jsonPath("$.permissionType").value(DEFAULT_PERMISSION_TYPE.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY.toString()))
            .andExpect(jsonPath("$.createDate").value(DEFAULT_CREATE_DATE.toString()))
            .andExpect(jsonPath("$.updateDate").value(DEFAULT_UPDATE_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAccessControl() throws Exception {
        // Get the accessControl
        restAccessControlMockMvc.perform(get("/api/access-controls/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAccessControl() throws Exception {
        // Initialize the database
        accessControlRepository.saveAndFlush(accessControl);
        int databaseSizeBeforeUpdate = accessControlRepository.findAll().size();

        // Update the accessControl
        AccessControl updatedAccessControl = accessControlRepository.findOne(accessControl.getId());
        updatedAccessControl
            .userType(UPDATED_USER_TYPE)
            .permissionType(UPDATED_PERMISSION_TYPE)
            .status(UPDATED_STATUS)
            .createdBy(UPDATED_CREATED_BY)
            .updatedBy(UPDATED_UPDATED_BY)
            .createDate(UPDATED_CREATE_DATE)
            .updateDate(UPDATED_UPDATE_DATE);
        AccessControlDTO accessControlDTO = accessControlMapper.toDto(updatedAccessControl);

        restAccessControlMockMvc.perform(put("/api/access-controls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(accessControlDTO)))
            .andExpect(status().isOk());

        // Validate the AccessControl in the database
        List<AccessControl> accessControlList = accessControlRepository.findAll();
        assertThat(accessControlList).hasSize(databaseSizeBeforeUpdate);
        AccessControl testAccessControl = accessControlList.get(accessControlList.size() - 1);
        assertThat(testAccessControl.getUserType()).isEqualTo(UPDATED_USER_TYPE);
        assertThat(testAccessControl.getPermissionType()).isEqualTo(UPDATED_PERMISSION_TYPE);
        assertThat(testAccessControl.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testAccessControl.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testAccessControl.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testAccessControl.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
        assertThat(testAccessControl.getUpdateDate()).isEqualTo(UPDATED_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingAccessControl() throws Exception {
        int databaseSizeBeforeUpdate = accessControlRepository.findAll().size();

        // Create the AccessControl
        AccessControlDTO accessControlDTO = accessControlMapper.toDto(accessControl);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAccessControlMockMvc.perform(put("/api/access-controls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(accessControlDTO)))
            .andExpect(status().isCreated());

        // Validate the AccessControl in the database
        List<AccessControl> accessControlList = accessControlRepository.findAll();
        assertThat(accessControlList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAccessControl() throws Exception {
        // Initialize the database
        accessControlRepository.saveAndFlush(accessControl);
        int databaseSizeBeforeDelete = accessControlRepository.findAll().size();

        // Get the accessControl
        restAccessControlMockMvc.perform(delete("/api/access-controls/{id}", accessControl.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<AccessControl> accessControlList = accessControlRepository.findAll();
        assertThat(accessControlList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AccessControl.class);
        AccessControl accessControl1 = new AccessControl();
        accessControl1.setId(1L);
        AccessControl accessControl2 = new AccessControl();
        accessControl2.setId(accessControl1.getId());
        assertThat(accessControl1).isEqualTo(accessControl2);
        accessControl2.setId(2L);
        assertThat(accessControl1).isNotEqualTo(accessControl2);
        accessControl1.setId(null);
        assertThat(accessControl1).isNotEqualTo(accessControl2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AccessControlDTO.class);
        AccessControlDTO accessControlDTO1 = new AccessControlDTO();
        accessControlDTO1.setId(1L);
        AccessControlDTO accessControlDTO2 = new AccessControlDTO();
        assertThat(accessControlDTO1).isNotEqualTo(accessControlDTO2);
        accessControlDTO2.setId(accessControlDTO1.getId());
        assertThat(accessControlDTO1).isEqualTo(accessControlDTO2);
        accessControlDTO2.setId(2L);
        assertThat(accessControlDTO1).isNotEqualTo(accessControlDTO2);
        accessControlDTO1.setId(null);
        assertThat(accessControlDTO1).isNotEqualTo(accessControlDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(accessControlMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(accessControlMapper.fromId(null)).isNull();
    }
}
