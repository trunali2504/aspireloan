package com.quix.aspireloan.web.rest;

import com.quix.aspireloan.AspireloanApp;

import com.quix.aspireloan.domain.RebateStatus;
import com.quix.aspireloan.repository.RebateStatusRepository;
import com.quix.aspireloan.service.dto.RebateStatusDTO;
import com.quix.aspireloan.service.mapper.RebateStatusMapper;
import com.quix.aspireloan.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.quix.aspireloan.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RebateStatusResource REST controller.
 *
 * @see RebateStatusResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AspireloanApp.class)
public class RebateStatusResourceIntTest {

    private static final Integer DEFAULT_APPLICATION_ID = 1;
    private static final Integer UPDATED_APPLICATION_ID = 2;

    private static final Integer DEFAULT_REBATEAMOUNT = 1;
    private static final Integer UPDATED_REBATEAMOUNT = 2;

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_LASTREMARK = "AAAAAAAAAA";
    private static final String UPDATED_LASTREMARK = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_APPLYDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_APPLYDATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_UPDATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private RebateStatusRepository rebateStatusRepository;

    @Autowired
    private RebateStatusMapper rebateStatusMapper;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restRebateStatusMockMvc;

    private RebateStatus rebateStatus;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RebateStatusResource rebateStatusResource = new RebateStatusResource(rebateStatusRepository, rebateStatusMapper);
        this.restRebateStatusMockMvc = MockMvcBuilders.standaloneSetup(rebateStatusResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RebateStatus createEntity(EntityManager em) {
        RebateStatus rebateStatus = new RebateStatus()
            .applicationId(DEFAULT_APPLICATION_ID)
            .rebateamount(DEFAULT_REBATEAMOUNT)
            .status(DEFAULT_STATUS)
            .lastremark(DEFAULT_LASTREMARK)
            .applydate(DEFAULT_APPLYDATE)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .updatedBy(DEFAULT_UPDATED_BY)
            .updatedDate(DEFAULT_UPDATED_DATE);
        return rebateStatus;
    }

    @Before
    public void initTest() {
        rebateStatus = createEntity(em);
    }

    @Test
    @Transactional
    public void createRebateStatus() throws Exception {
        int databaseSizeBeforeCreate = rebateStatusRepository.findAll().size();

        // Create the RebateStatus
        RebateStatusDTO rebateStatusDTO = rebateStatusMapper.toDto(rebateStatus);
        restRebateStatusMockMvc.perform(post("/api/rebate-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rebateStatusDTO)))
            .andExpect(status().isCreated());

        // Validate the RebateStatus in the database
        List<RebateStatus> rebateStatusList = rebateStatusRepository.findAll();
        assertThat(rebateStatusList).hasSize(databaseSizeBeforeCreate + 1);
        RebateStatus testRebateStatus = rebateStatusList.get(rebateStatusList.size() - 1);
        assertThat(testRebateStatus.getApplicationId()).isEqualTo(DEFAULT_APPLICATION_ID);
        assertThat(testRebateStatus.getRebateamount()).isEqualTo(DEFAULT_REBATEAMOUNT);
        assertThat(testRebateStatus.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testRebateStatus.getLastremark()).isEqualTo(DEFAULT_LASTREMARK);
        assertThat(testRebateStatus.getApplydate()).isEqualTo(DEFAULT_APPLYDATE);
        assertThat(testRebateStatus.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testRebateStatus.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testRebateStatus.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
        assertThat(testRebateStatus.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void createRebateStatusWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = rebateStatusRepository.findAll().size();

        // Create the RebateStatus with an existing ID
        rebateStatus.setId(1L);
        RebateStatusDTO rebateStatusDTO = rebateStatusMapper.toDto(rebateStatus);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRebateStatusMockMvc.perform(post("/api/rebate-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rebateStatusDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RebateStatus in the database
        List<RebateStatus> rebateStatusList = rebateStatusRepository.findAll();
        assertThat(rebateStatusList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllRebateStatuses() throws Exception {
        // Initialize the database
        rebateStatusRepository.saveAndFlush(rebateStatus);

        // Get all the rebateStatusList
        restRebateStatusMockMvc.perform(get("/api/rebate-statuses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(rebateStatus.getId().intValue())))
            .andExpect(jsonPath("$.[*].applicationId").value(hasItem(DEFAULT_APPLICATION_ID)))
            .andExpect(jsonPath("$.[*].rebateamount").value(hasItem(DEFAULT_REBATEAMOUNT)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].lastremark").value(hasItem(DEFAULT_LASTREMARK.toString())))
            .andExpect(jsonPath("$.[*].applydate").value(hasItem(DEFAULT_APPLYDATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    @Transactional
    public void getRebateStatus() throws Exception {
        // Initialize the database
        rebateStatusRepository.saveAndFlush(rebateStatus);

        // Get the rebateStatus
        restRebateStatusMockMvc.perform(get("/api/rebate-statuses/{id}", rebateStatus.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(rebateStatus.getId().intValue()))
            .andExpect(jsonPath("$.applicationId").value(DEFAULT_APPLICATION_ID))
            .andExpect(jsonPath("$.rebateamount").value(DEFAULT_REBATEAMOUNT))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.lastremark").value(DEFAULT_LASTREMARK.toString()))
            .andExpect(jsonPath("$.applydate").value(DEFAULT_APPLYDATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY.toString()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingRebateStatus() throws Exception {
        // Get the rebateStatus
        restRebateStatusMockMvc.perform(get("/api/rebate-statuses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRebateStatus() throws Exception {
        // Initialize the database
        rebateStatusRepository.saveAndFlush(rebateStatus);
        int databaseSizeBeforeUpdate = rebateStatusRepository.findAll().size();

        // Update the rebateStatus
        RebateStatus updatedRebateStatus = rebateStatusRepository.findOne(rebateStatus.getId());
        updatedRebateStatus
            .applicationId(UPDATED_APPLICATION_ID)
            .rebateamount(UPDATED_REBATEAMOUNT)
            .status(UPDATED_STATUS)
            .lastremark(UPDATED_LASTREMARK)
            .applydate(UPDATED_APPLYDATE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE);
        RebateStatusDTO rebateStatusDTO = rebateStatusMapper.toDto(updatedRebateStatus);

        restRebateStatusMockMvc.perform(put("/api/rebate-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rebateStatusDTO)))
            .andExpect(status().isOk());

        // Validate the RebateStatus in the database
        List<RebateStatus> rebateStatusList = rebateStatusRepository.findAll();
        assertThat(rebateStatusList).hasSize(databaseSizeBeforeUpdate);
        RebateStatus testRebateStatus = rebateStatusList.get(rebateStatusList.size() - 1);
        assertThat(testRebateStatus.getApplicationId()).isEqualTo(UPDATED_APPLICATION_ID);
        assertThat(testRebateStatus.getRebateamount()).isEqualTo(UPDATED_REBATEAMOUNT);
        assertThat(testRebateStatus.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testRebateStatus.getLastremark()).isEqualTo(UPDATED_LASTREMARK);
        assertThat(testRebateStatus.getApplydate()).isEqualTo(UPDATED_APPLYDATE);
        assertThat(testRebateStatus.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testRebateStatus.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testRebateStatus.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testRebateStatus.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingRebateStatus() throws Exception {
        int databaseSizeBeforeUpdate = rebateStatusRepository.findAll().size();

        // Create the RebateStatus
        RebateStatusDTO rebateStatusDTO = rebateStatusMapper.toDto(rebateStatus);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restRebateStatusMockMvc.perform(put("/api/rebate-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rebateStatusDTO)))
            .andExpect(status().isCreated());

        // Validate the RebateStatus in the database
        List<RebateStatus> rebateStatusList = rebateStatusRepository.findAll();
        assertThat(rebateStatusList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteRebateStatus() throws Exception {
        // Initialize the database
        rebateStatusRepository.saveAndFlush(rebateStatus);
        int databaseSizeBeforeDelete = rebateStatusRepository.findAll().size();

        // Get the rebateStatus
        restRebateStatusMockMvc.perform(delete("/api/rebate-statuses/{id}", rebateStatus.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<RebateStatus> rebateStatusList = rebateStatusRepository.findAll();
        assertThat(rebateStatusList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RebateStatus.class);
        RebateStatus rebateStatus1 = new RebateStatus();
        rebateStatus1.setId(1L);
        RebateStatus rebateStatus2 = new RebateStatus();
        rebateStatus2.setId(rebateStatus1.getId());
        assertThat(rebateStatus1).isEqualTo(rebateStatus2);
        rebateStatus2.setId(2L);
        assertThat(rebateStatus1).isNotEqualTo(rebateStatus2);
        rebateStatus1.setId(null);
        assertThat(rebateStatus1).isNotEqualTo(rebateStatus2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RebateStatusDTO.class);
        RebateStatusDTO rebateStatusDTO1 = new RebateStatusDTO();
        rebateStatusDTO1.setId(1L);
        RebateStatusDTO rebateStatusDTO2 = new RebateStatusDTO();
        assertThat(rebateStatusDTO1).isNotEqualTo(rebateStatusDTO2);
        rebateStatusDTO2.setId(rebateStatusDTO1.getId());
        assertThat(rebateStatusDTO1).isEqualTo(rebateStatusDTO2);
        rebateStatusDTO2.setId(2L);
        assertThat(rebateStatusDTO1).isNotEqualTo(rebateStatusDTO2);
        rebateStatusDTO1.setId(null);
        assertThat(rebateStatusDTO1).isNotEqualTo(rebateStatusDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(rebateStatusMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(rebateStatusMapper.fromId(null)).isNull();
    }
}
