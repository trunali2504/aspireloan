package com.quix.aspireloan.web.rest;

import com.quix.aspireloan.AspireloanApp;

import com.quix.aspireloan.domain.Packages;
import com.quix.aspireloan.repository.PackagesRepository;
import com.quix.aspireloan.service.dto.PackagesDTO;
import com.quix.aspireloan.service.mapper.PackagesMapper;
import com.quix.aspireloan.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.quix.aspireloan.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PackagesResource REST controller.
 *
 * @see PackagesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AspireloanApp.class)
public class PackagesResourceIntTest {

    private static final LocalDate DEFAULT_VALIDITYFROMDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VALIDITYFROMDATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_VALIDITYTODATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VALIDITYTODATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_TYPE_OF_PRODUCT = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_OF_PRODUCT = "BBBBBBBBBB";

    private static final Integer DEFAULT_INTERESTRATE = 1;
    private static final Integer UPDATED_INTERESTRATE = 2;

    private static final Long DEFAULT_MAXLOANAMOUNT = 1L;
    private static final Long UPDATED_MAXLOANAMOUNT = 2L;

    private static final Long DEFAULT_MINLOANAMOUNT = 1L;
    private static final Long UPDATED_MINLOANAMOUNT = 2L;

    private static final Integer DEFAULT_MAXTENURE = 1;
    private static final Integer UPDATED_MAXTENURE = 2;

    private static final Integer DEFAULT_MINTENURE = 1;
    private static final Integer UPDATED_MINTENURE = 2;

    private static final String DEFAULT_TOTALPAYOUT = "AAAAAAAAAA";
    private static final String UPDATED_TOTALPAYOUT = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_UPDATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private PackagesRepository packagesRepository;

    @Autowired
    private PackagesMapper packagesMapper;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPackagesMockMvc;

    private Packages packages;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PackagesResource packagesResource = new PackagesResource(packagesRepository, packagesMapper);
        this.restPackagesMockMvc = MockMvcBuilders.standaloneSetup(packagesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Packages createEntity(EntityManager em) {
        Packages packages = new Packages()
            .validityfromdate(DEFAULT_VALIDITYFROMDATE)
            .validitytodate(DEFAULT_VALIDITYTODATE)
            .typeOfProduct(DEFAULT_TYPE_OF_PRODUCT)
            .interestrate(DEFAULT_INTERESTRATE)
            .maxloanamount(DEFAULT_MAXLOANAMOUNT)
            .minloanamount(DEFAULT_MINLOANAMOUNT)
            .maxtenure(DEFAULT_MAXTENURE)
            .mintenure(DEFAULT_MINTENURE)
            .totalpayout(DEFAULT_TOTALPAYOUT)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .updatedBy(DEFAULT_UPDATED_BY)
            .updatedDate(DEFAULT_UPDATED_DATE);
        return packages;
    }

    @Before
    public void initTest() {
        packages = createEntity(em);
    }

    @Test
    @Transactional
    public void createPackages() throws Exception {
        int databaseSizeBeforeCreate = packagesRepository.findAll().size();

        // Create the Packages
        PackagesDTO packagesDTO = packagesMapper.toDto(packages);
        restPackagesMockMvc.perform(post("/api/packages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(packagesDTO)))
            .andExpect(status().isCreated());

        // Validate the Packages in the database
        List<Packages> packagesList = packagesRepository.findAll();
        assertThat(packagesList).hasSize(databaseSizeBeforeCreate + 1);
        Packages testPackages = packagesList.get(packagesList.size() - 1);
        assertThat(testPackages.getValidityfromdate()).isEqualTo(DEFAULT_VALIDITYFROMDATE);
        assertThat(testPackages.getValiditytodate()).isEqualTo(DEFAULT_VALIDITYTODATE);
        assertThat(testPackages.getTypeOfProduct()).isEqualTo(DEFAULT_TYPE_OF_PRODUCT);
        assertThat(testPackages.getInterestrate()).isEqualTo(DEFAULT_INTERESTRATE);
        assertThat(testPackages.getMaxloanamount()).isEqualTo(DEFAULT_MAXLOANAMOUNT);
        assertThat(testPackages.getMinloanamount()).isEqualTo(DEFAULT_MINLOANAMOUNT);
        assertThat(testPackages.getMaxtenure()).isEqualTo(DEFAULT_MAXTENURE);
        assertThat(testPackages.getMintenure()).isEqualTo(DEFAULT_MINTENURE);
        assertThat(testPackages.getTotalpayout()).isEqualTo(DEFAULT_TOTALPAYOUT);
        assertThat(testPackages.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testPackages.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testPackages.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
        assertThat(testPackages.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void createPackagesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = packagesRepository.findAll().size();

        // Create the Packages with an existing ID
        packages.setId(1L);
        PackagesDTO packagesDTO = packagesMapper.toDto(packages);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPackagesMockMvc.perform(post("/api/packages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(packagesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Packages in the database
        List<Packages> packagesList = packagesRepository.findAll();
        assertThat(packagesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPackages() throws Exception {
        // Initialize the database
        packagesRepository.saveAndFlush(packages);

        // Get all the packagesList
        restPackagesMockMvc.perform(get("/api/packages?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(packages.getId().intValue())))
            .andExpect(jsonPath("$.[*].validityfromdate").value(hasItem(DEFAULT_VALIDITYFROMDATE.toString())))
            .andExpect(jsonPath("$.[*].validitytodate").value(hasItem(DEFAULT_VALIDITYTODATE.toString())))
            .andExpect(jsonPath("$.[*].typeOfProduct").value(hasItem(DEFAULT_TYPE_OF_PRODUCT.toString())))
            .andExpect(jsonPath("$.[*].interestrate").value(hasItem(DEFAULT_INTERESTRATE)))
            .andExpect(jsonPath("$.[*].maxloanamount").value(hasItem(DEFAULT_MAXLOANAMOUNT.intValue())))
            .andExpect(jsonPath("$.[*].minloanamount").value(hasItem(DEFAULT_MINLOANAMOUNT.intValue())))
            .andExpect(jsonPath("$.[*].maxtenure").value(hasItem(DEFAULT_MAXTENURE)))
            .andExpect(jsonPath("$.[*].mintenure").value(hasItem(DEFAULT_MINTENURE)))
            .andExpect(jsonPath("$.[*].totalpayout").value(hasItem(DEFAULT_TOTALPAYOUT.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    @Transactional
    public void getPackages() throws Exception {
        // Initialize the database
        packagesRepository.saveAndFlush(packages);

        // Get the packages
        restPackagesMockMvc.perform(get("/api/packages/{id}", packages.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(packages.getId().intValue()))
            .andExpect(jsonPath("$.validityfromdate").value(DEFAULT_VALIDITYFROMDATE.toString()))
            .andExpect(jsonPath("$.validitytodate").value(DEFAULT_VALIDITYTODATE.toString()))
            .andExpect(jsonPath("$.typeOfProduct").value(DEFAULT_TYPE_OF_PRODUCT.toString()))
            .andExpect(jsonPath("$.interestrate").value(DEFAULT_INTERESTRATE))
            .andExpect(jsonPath("$.maxloanamount").value(DEFAULT_MAXLOANAMOUNT.intValue()))
            .andExpect(jsonPath("$.minloanamount").value(DEFAULT_MINLOANAMOUNT.intValue()))
            .andExpect(jsonPath("$.maxtenure").value(DEFAULT_MAXTENURE))
            .andExpect(jsonPath("$.mintenure").value(DEFAULT_MINTENURE))
            .andExpect(jsonPath("$.totalpayout").value(DEFAULT_TOTALPAYOUT.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY.toString()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPackages() throws Exception {
        // Get the packages
        restPackagesMockMvc.perform(get("/api/packages/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePackages() throws Exception {
        // Initialize the database
        packagesRepository.saveAndFlush(packages);
        int databaseSizeBeforeUpdate = packagesRepository.findAll().size();

        // Update the packages
        Packages updatedPackages = packagesRepository.findOne(packages.getId());
        updatedPackages
            .validityfromdate(UPDATED_VALIDITYFROMDATE)
            .validitytodate(UPDATED_VALIDITYTODATE)
            .typeOfProduct(UPDATED_TYPE_OF_PRODUCT)
            .interestrate(UPDATED_INTERESTRATE)
            .maxloanamount(UPDATED_MAXLOANAMOUNT)
            .minloanamount(UPDATED_MINLOANAMOUNT)
            .maxtenure(UPDATED_MAXTENURE)
            .mintenure(UPDATED_MINTENURE)
            .totalpayout(UPDATED_TOTALPAYOUT)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE);
        PackagesDTO packagesDTO = packagesMapper.toDto(updatedPackages);

        restPackagesMockMvc.perform(put("/api/packages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(packagesDTO)))
            .andExpect(status().isOk());

        // Validate the Packages in the database
        List<Packages> packagesList = packagesRepository.findAll();
        assertThat(packagesList).hasSize(databaseSizeBeforeUpdate);
        Packages testPackages = packagesList.get(packagesList.size() - 1);
        assertThat(testPackages.getValidityfromdate()).isEqualTo(UPDATED_VALIDITYFROMDATE);
        assertThat(testPackages.getValiditytodate()).isEqualTo(UPDATED_VALIDITYTODATE);
        assertThat(testPackages.getTypeOfProduct()).isEqualTo(UPDATED_TYPE_OF_PRODUCT);
        assertThat(testPackages.getInterestrate()).isEqualTo(UPDATED_INTERESTRATE);
        assertThat(testPackages.getMaxloanamount()).isEqualTo(UPDATED_MAXLOANAMOUNT);
        assertThat(testPackages.getMinloanamount()).isEqualTo(UPDATED_MINLOANAMOUNT);
        assertThat(testPackages.getMaxtenure()).isEqualTo(UPDATED_MAXTENURE);
        assertThat(testPackages.getMintenure()).isEqualTo(UPDATED_MINTENURE);
        assertThat(testPackages.getTotalpayout()).isEqualTo(UPDATED_TOTALPAYOUT);
        assertThat(testPackages.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testPackages.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testPackages.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testPackages.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingPackages() throws Exception {
        int databaseSizeBeforeUpdate = packagesRepository.findAll().size();

        // Create the Packages
        PackagesDTO packagesDTO = packagesMapper.toDto(packages);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPackagesMockMvc.perform(put("/api/packages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(packagesDTO)))
            .andExpect(status().isCreated());

        // Validate the Packages in the database
        List<Packages> packagesList = packagesRepository.findAll();
        assertThat(packagesList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePackages() throws Exception {
        // Initialize the database
        packagesRepository.saveAndFlush(packages);
        int databaseSizeBeforeDelete = packagesRepository.findAll().size();

        // Get the packages
        restPackagesMockMvc.perform(delete("/api/packages/{id}", packages.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Packages> packagesList = packagesRepository.findAll();
        assertThat(packagesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Packages.class);
        Packages packages1 = new Packages();
        packages1.setId(1L);
        Packages packages2 = new Packages();
        packages2.setId(packages1.getId());
        assertThat(packages1).isEqualTo(packages2);
        packages2.setId(2L);
        assertThat(packages1).isNotEqualTo(packages2);
        packages1.setId(null);
        assertThat(packages1).isNotEqualTo(packages2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PackagesDTO.class);
        PackagesDTO packagesDTO1 = new PackagesDTO();
        packagesDTO1.setId(1L);
        PackagesDTO packagesDTO2 = new PackagesDTO();
        assertThat(packagesDTO1).isNotEqualTo(packagesDTO2);
        packagesDTO2.setId(packagesDTO1.getId());
        assertThat(packagesDTO1).isEqualTo(packagesDTO2);
        packagesDTO2.setId(2L);
        assertThat(packagesDTO1).isNotEqualTo(packagesDTO2);
        packagesDTO1.setId(null);
        assertThat(packagesDTO1).isNotEqualTo(packagesDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(packagesMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(packagesMapper.fromId(null)).isNull();
    }
}
