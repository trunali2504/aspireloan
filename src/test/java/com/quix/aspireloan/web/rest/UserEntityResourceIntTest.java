package com.quix.aspireloan.web.rest;

import com.quix.aspireloan.AspireloanApp;

import com.quix.aspireloan.domain.UserEntity;
import com.quix.aspireloan.repository.UserEntityRepository;
import com.quix.aspireloan.service.dto.UserEntityDTO;
import com.quix.aspireloan.service.mapper.UserEntityMapper;
import com.quix.aspireloan.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.quix.aspireloan.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UserEntityResource REST controller.
 *
 * @see UserEntityResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AspireloanApp.class)
public class UserEntityResourceIntTest {

    private static final Integer DEFAULT_MOBILE_NO = 1;
    private static final Integer UPDATED_MOBILE_NO = 2;

    private static final String DEFAULT_EMAILID = "AAAAAAAAAA";
    private static final String UPDATED_EMAILID = "BBBBBBBBBB";

    private static final String DEFAULT_TOKENID = "AAAAAAAAAA";
    private static final String UPDATED_TOKENID = "BBBBBBBBBB";

    private static final Integer DEFAULT_STATUS = 1;
    private static final Integer UPDATED_STATUS = 2;

    private static final String DEFAULT_USERTYPE = "AAAAAAAAAA";
    private static final String UPDATED_USERTYPE = "BBBBBBBBBB";

    private static final String DEFAULT_COMPANY = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_UPDATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private UserEntityRepository userEntityRepository;

    @Autowired
    private UserEntityMapper userEntityMapper;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restUserEntityMockMvc;

    private UserEntity userEntity;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UserEntityResource userEntityResource = new UserEntityResource(userEntityRepository, userEntityMapper);
        this.restUserEntityMockMvc = MockMvcBuilders.standaloneSetup(userEntityResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserEntity createEntity(EntityManager em) {
        UserEntity userEntity = new UserEntity()
            .mobileNo(DEFAULT_MOBILE_NO)
            .emailid(DEFAULT_EMAILID)
            .tokenid(DEFAULT_TOKENID)
            .status(DEFAULT_STATUS)
            .usertype(DEFAULT_USERTYPE)
            .company(DEFAULT_COMPANY)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .updatedBy(DEFAULT_UPDATED_BY)
            .updatedDate(DEFAULT_UPDATED_DATE);
        return userEntity;
    }

    @Before
    public void initTest() {
        userEntity = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserEntity() throws Exception {
        int databaseSizeBeforeCreate = userEntityRepository.findAll().size();

        // Create the UserEntity
        UserEntityDTO userEntityDTO = userEntityMapper.toDto(userEntity);
        restUserEntityMockMvc.perform(post("/api/user-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userEntityDTO)))
            .andExpect(status().isCreated());

        // Validate the UserEntity in the database
        List<UserEntity> userEntityList = userEntityRepository.findAll();
        assertThat(userEntityList).hasSize(databaseSizeBeforeCreate + 1);
        UserEntity testUserEntity = userEntityList.get(userEntityList.size() - 1);
        assertThat(testUserEntity.getMobileNo()).isEqualTo(DEFAULT_MOBILE_NO);
        assertThat(testUserEntity.getEmailid()).isEqualTo(DEFAULT_EMAILID);
        assertThat(testUserEntity.getTokenid()).isEqualTo(DEFAULT_TOKENID);
        assertThat(testUserEntity.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testUserEntity.getUsertype()).isEqualTo(DEFAULT_USERTYPE);
        assertThat(testUserEntity.getCompany()).isEqualTo(DEFAULT_COMPANY);
        assertThat(testUserEntity.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testUserEntity.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testUserEntity.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
        assertThat(testUserEntity.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void createUserEntityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userEntityRepository.findAll().size();

        // Create the UserEntity with an existing ID
        userEntity.setId(1L);
        UserEntityDTO userEntityDTO = userEntityMapper.toDto(userEntity);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserEntityMockMvc.perform(post("/api/user-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userEntityDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserEntity in the database
        List<UserEntity> userEntityList = userEntityRepository.findAll();
        assertThat(userEntityList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllUserEntities() throws Exception {
        // Initialize the database
        userEntityRepository.saveAndFlush(userEntity);

        // Get all the userEntityList
        restUserEntityMockMvc.perform(get("/api/user-entities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userEntity.getId().intValue())))
            .andExpect(jsonPath("$.[*].mobileNo").value(hasItem(DEFAULT_MOBILE_NO)))
            .andExpect(jsonPath("$.[*].emailid").value(hasItem(DEFAULT_EMAILID.toString())))
            .andExpect(jsonPath("$.[*].tokenid").value(hasItem(DEFAULT_TOKENID.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].usertype").value(hasItem(DEFAULT_USERTYPE.toString())))
            .andExpect(jsonPath("$.[*].company").value(hasItem(DEFAULT_COMPANY.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    @Transactional
    public void getUserEntity() throws Exception {
        // Initialize the database
        userEntityRepository.saveAndFlush(userEntity);

        // Get the userEntity
        restUserEntityMockMvc.perform(get("/api/user-entities/{id}", userEntity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userEntity.getId().intValue()))
            .andExpect(jsonPath("$.mobileNo").value(DEFAULT_MOBILE_NO))
            .andExpect(jsonPath("$.emailid").value(DEFAULT_EMAILID.toString()))
            .andExpect(jsonPath("$.tokenid").value(DEFAULT_TOKENID.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.usertype").value(DEFAULT_USERTYPE.toString()))
            .andExpect(jsonPath("$.company").value(DEFAULT_COMPANY.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY.toString()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingUserEntity() throws Exception {
        // Get the userEntity
        restUserEntityMockMvc.perform(get("/api/user-entities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserEntity() throws Exception {
        // Initialize the database
        userEntityRepository.saveAndFlush(userEntity);
        int databaseSizeBeforeUpdate = userEntityRepository.findAll().size();

        // Update the userEntity
        UserEntity updatedUserEntity = userEntityRepository.findOne(userEntity.getId());
        updatedUserEntity
            .mobileNo(UPDATED_MOBILE_NO)
            .emailid(UPDATED_EMAILID)
            .tokenid(UPDATED_TOKENID)
            .status(UPDATED_STATUS)
            .usertype(UPDATED_USERTYPE)
            .company(UPDATED_COMPANY)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE);
        UserEntityDTO userEntityDTO = userEntityMapper.toDto(updatedUserEntity);

        restUserEntityMockMvc.perform(put("/api/user-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userEntityDTO)))
            .andExpect(status().isOk());

        // Validate the UserEntity in the database
        List<UserEntity> userEntityList = userEntityRepository.findAll();
        assertThat(userEntityList).hasSize(databaseSizeBeforeUpdate);
        UserEntity testUserEntity = userEntityList.get(userEntityList.size() - 1);
        assertThat(testUserEntity.getMobileNo()).isEqualTo(UPDATED_MOBILE_NO);
        assertThat(testUserEntity.getEmailid()).isEqualTo(UPDATED_EMAILID);
        assertThat(testUserEntity.getTokenid()).isEqualTo(UPDATED_TOKENID);
        assertThat(testUserEntity.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testUserEntity.getUsertype()).isEqualTo(UPDATED_USERTYPE);
        assertThat(testUserEntity.getCompany()).isEqualTo(UPDATED_COMPANY);
        assertThat(testUserEntity.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testUserEntity.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testUserEntity.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testUserEntity.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingUserEntity() throws Exception {
        int databaseSizeBeforeUpdate = userEntityRepository.findAll().size();

        // Create the UserEntity
        UserEntityDTO userEntityDTO = userEntityMapper.toDto(userEntity);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restUserEntityMockMvc.perform(put("/api/user-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userEntityDTO)))
            .andExpect(status().isCreated());

        // Validate the UserEntity in the database
        List<UserEntity> userEntityList = userEntityRepository.findAll();
        assertThat(userEntityList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteUserEntity() throws Exception {
        // Initialize the database
        userEntityRepository.saveAndFlush(userEntity);
        int databaseSizeBeforeDelete = userEntityRepository.findAll().size();

        // Get the userEntity
        restUserEntityMockMvc.perform(delete("/api/user-entities/{id}", userEntity.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<UserEntity> userEntityList = userEntityRepository.findAll();
        assertThat(userEntityList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserEntity.class);
        UserEntity userEntity1 = new UserEntity();
        userEntity1.setId(1L);
        UserEntity userEntity2 = new UserEntity();
        userEntity2.setId(userEntity1.getId());
        assertThat(userEntity1).isEqualTo(userEntity2);
        userEntity2.setId(2L);
        assertThat(userEntity1).isNotEqualTo(userEntity2);
        userEntity1.setId(null);
        assertThat(userEntity1).isNotEqualTo(userEntity2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserEntityDTO.class);
        UserEntityDTO userEntityDTO1 = new UserEntityDTO();
        userEntityDTO1.setId(1L);
        UserEntityDTO userEntityDTO2 = new UserEntityDTO();
        assertThat(userEntityDTO1).isNotEqualTo(userEntityDTO2);
        userEntityDTO2.setId(userEntityDTO1.getId());
        assertThat(userEntityDTO1).isEqualTo(userEntityDTO2);
        userEntityDTO2.setId(2L);
        assertThat(userEntityDTO1).isNotEqualTo(userEntityDTO2);
        userEntityDTO1.setId(null);
        assertThat(userEntityDTO1).isNotEqualTo(userEntityDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(userEntityMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(userEntityMapper.fromId(null)).isNull();
    }
}
