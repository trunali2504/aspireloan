package com.quix.aspireloan.web.rest;

import com.quix.aspireloan.AspireloanApp;

import com.quix.aspireloan.domain.Passwords;
import com.quix.aspireloan.repository.PasswordsRepository;
import com.quix.aspireloan.service.dto.PasswordsDTO;
import com.quix.aspireloan.service.mapper.PasswordsMapper;
import com.quix.aspireloan.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.quix.aspireloan.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PasswordsResource REST controller.
 *
 * @see PasswordsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AspireloanApp.class)
public class PasswordsResourceIntTest {

    private static final Integer DEFAULT_USER_ID = 1;
    private static final Integer UPDATED_USER_ID = 2;

    private static final String DEFAULT_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_PASSWORD = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_UPDATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private PasswordsRepository passwordsRepository;

    @Autowired
    private PasswordsMapper passwordsMapper;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPasswordsMockMvc;

    private Passwords passwords;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PasswordsResource passwordsResource = new PasswordsResource(passwordsRepository, passwordsMapper);
        this.restPasswordsMockMvc = MockMvcBuilders.standaloneSetup(passwordsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Passwords createEntity(EntityManager em) {
        Passwords passwords = new Passwords()
            .userId(DEFAULT_USER_ID)
            .password(DEFAULT_PASSWORD)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .updatedBy(DEFAULT_UPDATED_BY)
            .updatedDate(DEFAULT_UPDATED_DATE);
        return passwords;
    }

    @Before
    public void initTest() {
        passwords = createEntity(em);
    }

    @Test
    @Transactional
    public void createPasswords() throws Exception {
        int databaseSizeBeforeCreate = passwordsRepository.findAll().size();

        // Create the Passwords
        PasswordsDTO passwordsDTO = passwordsMapper.toDto(passwords);
        restPasswordsMockMvc.perform(post("/api/passwords")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(passwordsDTO)))
            .andExpect(status().isCreated());

        // Validate the Passwords in the database
        List<Passwords> passwordsList = passwordsRepository.findAll();
        assertThat(passwordsList).hasSize(databaseSizeBeforeCreate + 1);
        Passwords testPasswords = passwordsList.get(passwordsList.size() - 1);
        assertThat(testPasswords.getUserId()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testPasswords.getPassword()).isEqualTo(DEFAULT_PASSWORD);
        assertThat(testPasswords.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testPasswords.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testPasswords.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
        assertThat(testPasswords.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void createPasswordsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = passwordsRepository.findAll().size();

        // Create the Passwords with an existing ID
        passwords.setId(1L);
        PasswordsDTO passwordsDTO = passwordsMapper.toDto(passwords);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPasswordsMockMvc.perform(post("/api/passwords")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(passwordsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Passwords in the database
        List<Passwords> passwordsList = passwordsRepository.findAll();
        assertThat(passwordsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPasswords() throws Exception {
        // Initialize the database
        passwordsRepository.saveAndFlush(passwords);

        // Get all the passwordsList
        restPasswordsMockMvc.perform(get("/api/passwords?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(passwords.getId().intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID)))
            .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    @Transactional
    public void getPasswords() throws Exception {
        // Initialize the database
        passwordsRepository.saveAndFlush(passwords);

        // Get the passwords
        restPasswordsMockMvc.perform(get("/api/passwords/{id}", passwords.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(passwords.getId().intValue()))
            .andExpect(jsonPath("$.userId").value(DEFAULT_USER_ID))
            .andExpect(jsonPath("$.password").value(DEFAULT_PASSWORD.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY.toString()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPasswords() throws Exception {
        // Get the passwords
        restPasswordsMockMvc.perform(get("/api/passwords/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePasswords() throws Exception {
        // Initialize the database
        passwordsRepository.saveAndFlush(passwords);
        int databaseSizeBeforeUpdate = passwordsRepository.findAll().size();

        // Update the passwords
        Passwords updatedPasswords = passwordsRepository.findOne(passwords.getId());
        updatedPasswords
            .userId(UPDATED_USER_ID)
            .password(UPDATED_PASSWORD)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE);
        PasswordsDTO passwordsDTO = passwordsMapper.toDto(updatedPasswords);

        restPasswordsMockMvc.perform(put("/api/passwords")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(passwordsDTO)))
            .andExpect(status().isOk());

        // Validate the Passwords in the database
        List<Passwords> passwordsList = passwordsRepository.findAll();
        assertThat(passwordsList).hasSize(databaseSizeBeforeUpdate);
        Passwords testPasswords = passwordsList.get(passwordsList.size() - 1);
        assertThat(testPasswords.getUserId()).isEqualTo(UPDATED_USER_ID);
        assertThat(testPasswords.getPassword()).isEqualTo(UPDATED_PASSWORD);
        assertThat(testPasswords.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testPasswords.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testPasswords.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testPasswords.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingPasswords() throws Exception {
        int databaseSizeBeforeUpdate = passwordsRepository.findAll().size();

        // Create the Passwords
        PasswordsDTO passwordsDTO = passwordsMapper.toDto(passwords);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPasswordsMockMvc.perform(put("/api/passwords")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(passwordsDTO)))
            .andExpect(status().isCreated());

        // Validate the Passwords in the database
        List<Passwords> passwordsList = passwordsRepository.findAll();
        assertThat(passwordsList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePasswords() throws Exception {
        // Initialize the database
        passwordsRepository.saveAndFlush(passwords);
        int databaseSizeBeforeDelete = passwordsRepository.findAll().size();

        // Get the passwords
        restPasswordsMockMvc.perform(delete("/api/passwords/{id}", passwords.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Passwords> passwordsList = passwordsRepository.findAll();
        assertThat(passwordsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Passwords.class);
        Passwords passwords1 = new Passwords();
        passwords1.setId(1L);
        Passwords passwords2 = new Passwords();
        passwords2.setId(passwords1.getId());
        assertThat(passwords1).isEqualTo(passwords2);
        passwords2.setId(2L);
        assertThat(passwords1).isNotEqualTo(passwords2);
        passwords1.setId(null);
        assertThat(passwords1).isNotEqualTo(passwords2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PasswordsDTO.class);
        PasswordsDTO passwordsDTO1 = new PasswordsDTO();
        passwordsDTO1.setId(1L);
        PasswordsDTO passwordsDTO2 = new PasswordsDTO();
        assertThat(passwordsDTO1).isNotEqualTo(passwordsDTO2);
        passwordsDTO2.setId(passwordsDTO1.getId());
        assertThat(passwordsDTO1).isEqualTo(passwordsDTO2);
        passwordsDTO2.setId(2L);
        assertThat(passwordsDTO1).isNotEqualTo(passwordsDTO2);
        passwordsDTO1.setId(null);
        assertThat(passwordsDTO1).isNotEqualTo(passwordsDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(passwordsMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(passwordsMapper.fromId(null)).isNull();
    }
}
