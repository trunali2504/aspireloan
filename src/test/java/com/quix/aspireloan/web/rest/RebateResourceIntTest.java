package com.quix.aspireloan.web.rest;

import com.quix.aspireloan.AspireloanApp;

import com.quix.aspireloan.domain.Rebate;
import com.quix.aspireloan.repository.RebateRepository;
import com.quix.aspireloan.service.dto.RebateDTO;
import com.quix.aspireloan.service.mapper.RebateMapper;
import com.quix.aspireloan.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.quix.aspireloan.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RebateResource REST controller.
 *
 * @see RebateResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AspireloanApp.class)
public class RebateResourceIntTest {

    private static final Integer DEFAULT_LOAN_FROM = 1;
    private static final Integer UPDATED_LOAN_FROM = 2;

    private static final String DEFAULT_LOAN_TO = "AAAAAAAAAA";
    private static final String UPDATED_LOAN_TO = "BBBBBBBBBB";

    private static final Integer DEFAULT_LOAN_PERCENTAGE = 1;
    private static final Integer UPDATED_LOAN_PERCENTAGE = 2;

    private static final Integer DEFAULT_MAXAMOUNT = 1;
    private static final Integer UPDATED_MAXAMOUNT = 2;

    private static final LocalDate DEFAULT_VALIDITYFROMDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VALIDITYFROMDATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_VALIDITYTODATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VALIDITYTODATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_PRODUCT = "AAAAAAAAAA";
    private static final String UPDATED_PRODUCT = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_UPDATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private RebateRepository rebateRepository;

    @Autowired
    private RebateMapper rebateMapper;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restRebateMockMvc;

    private Rebate rebate;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RebateResource rebateResource = new RebateResource(rebateRepository, rebateMapper);
        this.restRebateMockMvc = MockMvcBuilders.standaloneSetup(rebateResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Rebate createEntity(EntityManager em) {
        Rebate rebate = new Rebate()
            .loanFrom(DEFAULT_LOAN_FROM)
            .loanTo(DEFAULT_LOAN_TO)
            .loanPercentage(DEFAULT_LOAN_PERCENTAGE)
            .maxamount(DEFAULT_MAXAMOUNT)
            .validityfromdate(DEFAULT_VALIDITYFROMDATE)
            .validitytodate(DEFAULT_VALIDITYTODATE)
            .product(DEFAULT_PRODUCT)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .updatedBy(DEFAULT_UPDATED_BY)
            .updatedDate(DEFAULT_UPDATED_DATE);
        return rebate;
    }

    @Before
    public void initTest() {
        rebate = createEntity(em);
    }

    @Test
    @Transactional
    public void createRebate() throws Exception {
        int databaseSizeBeforeCreate = rebateRepository.findAll().size();

        // Create the Rebate
        RebateDTO rebateDTO = rebateMapper.toDto(rebate);
        restRebateMockMvc.perform(post("/api/rebates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rebateDTO)))
            .andExpect(status().isCreated());

        // Validate the Rebate in the database
        List<Rebate> rebateList = rebateRepository.findAll();
        assertThat(rebateList).hasSize(databaseSizeBeforeCreate + 1);
        Rebate testRebate = rebateList.get(rebateList.size() - 1);
        assertThat(testRebate.getLoanFrom()).isEqualTo(DEFAULT_LOAN_FROM);
        assertThat(testRebate.getLoanTo()).isEqualTo(DEFAULT_LOAN_TO);
        assertThat(testRebate.getLoanPercentage()).isEqualTo(DEFAULT_LOAN_PERCENTAGE);
        assertThat(testRebate.getMaxamount()).isEqualTo(DEFAULT_MAXAMOUNT);
        assertThat(testRebate.getValidityfromdate()).isEqualTo(DEFAULT_VALIDITYFROMDATE);
        assertThat(testRebate.getValiditytodate()).isEqualTo(DEFAULT_VALIDITYTODATE);
        assertThat(testRebate.getProduct()).isEqualTo(DEFAULT_PRODUCT);
        assertThat(testRebate.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testRebate.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testRebate.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
        assertThat(testRebate.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void createRebateWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = rebateRepository.findAll().size();

        // Create the Rebate with an existing ID
        rebate.setId(1L);
        RebateDTO rebateDTO = rebateMapper.toDto(rebate);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRebateMockMvc.perform(post("/api/rebates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rebateDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Rebate in the database
        List<Rebate> rebateList = rebateRepository.findAll();
        assertThat(rebateList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllRebates() throws Exception {
        // Initialize the database
        rebateRepository.saveAndFlush(rebate);

        // Get all the rebateList
        restRebateMockMvc.perform(get("/api/rebates?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(rebate.getId().intValue())))
            .andExpect(jsonPath("$.[*].loanFrom").value(hasItem(DEFAULT_LOAN_FROM)))
            .andExpect(jsonPath("$.[*].loanTo").value(hasItem(DEFAULT_LOAN_TO.toString())))
            .andExpect(jsonPath("$.[*].loanPercentage").value(hasItem(DEFAULT_LOAN_PERCENTAGE)))
            .andExpect(jsonPath("$.[*].maxamount").value(hasItem(DEFAULT_MAXAMOUNT)))
            .andExpect(jsonPath("$.[*].validityfromdate").value(hasItem(DEFAULT_VALIDITYFROMDATE.toString())))
            .andExpect(jsonPath("$.[*].validitytodate").value(hasItem(DEFAULT_VALIDITYTODATE.toString())))
            .andExpect(jsonPath("$.[*].product").value(hasItem(DEFAULT_PRODUCT.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    @Transactional
    public void getRebate() throws Exception {
        // Initialize the database
        rebateRepository.saveAndFlush(rebate);

        // Get the rebate
        restRebateMockMvc.perform(get("/api/rebates/{id}", rebate.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(rebate.getId().intValue()))
            .andExpect(jsonPath("$.loanFrom").value(DEFAULT_LOAN_FROM))
            .andExpect(jsonPath("$.loanTo").value(DEFAULT_LOAN_TO.toString()))
            .andExpect(jsonPath("$.loanPercentage").value(DEFAULT_LOAN_PERCENTAGE))
            .andExpect(jsonPath("$.maxamount").value(DEFAULT_MAXAMOUNT))
            .andExpect(jsonPath("$.validityfromdate").value(DEFAULT_VALIDITYFROMDATE.toString()))
            .andExpect(jsonPath("$.validitytodate").value(DEFAULT_VALIDITYTODATE.toString()))
            .andExpect(jsonPath("$.product").value(DEFAULT_PRODUCT.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY.toString()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingRebate() throws Exception {
        // Get the rebate
        restRebateMockMvc.perform(get("/api/rebates/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRebate() throws Exception {
        // Initialize the database
        rebateRepository.saveAndFlush(rebate);
        int databaseSizeBeforeUpdate = rebateRepository.findAll().size();

        // Update the rebate
        Rebate updatedRebate = rebateRepository.findOne(rebate.getId());
        updatedRebate
            .loanFrom(UPDATED_LOAN_FROM)
            .loanTo(UPDATED_LOAN_TO)
            .loanPercentage(UPDATED_LOAN_PERCENTAGE)
            .maxamount(UPDATED_MAXAMOUNT)
            .validityfromdate(UPDATED_VALIDITYFROMDATE)
            .validitytodate(UPDATED_VALIDITYTODATE)
            .product(UPDATED_PRODUCT)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE);
        RebateDTO rebateDTO = rebateMapper.toDto(updatedRebate);

        restRebateMockMvc.perform(put("/api/rebates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rebateDTO)))
            .andExpect(status().isOk());

        // Validate the Rebate in the database
        List<Rebate> rebateList = rebateRepository.findAll();
        assertThat(rebateList).hasSize(databaseSizeBeforeUpdate);
        Rebate testRebate = rebateList.get(rebateList.size() - 1);
        assertThat(testRebate.getLoanFrom()).isEqualTo(UPDATED_LOAN_FROM);
        assertThat(testRebate.getLoanTo()).isEqualTo(UPDATED_LOAN_TO);
        assertThat(testRebate.getLoanPercentage()).isEqualTo(UPDATED_LOAN_PERCENTAGE);
        assertThat(testRebate.getMaxamount()).isEqualTo(UPDATED_MAXAMOUNT);
        assertThat(testRebate.getValidityfromdate()).isEqualTo(UPDATED_VALIDITYFROMDATE);
        assertThat(testRebate.getValiditytodate()).isEqualTo(UPDATED_VALIDITYTODATE);
        assertThat(testRebate.getProduct()).isEqualTo(UPDATED_PRODUCT);
        assertThat(testRebate.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testRebate.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testRebate.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testRebate.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingRebate() throws Exception {
        int databaseSizeBeforeUpdate = rebateRepository.findAll().size();

        // Create the Rebate
        RebateDTO rebateDTO = rebateMapper.toDto(rebate);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restRebateMockMvc.perform(put("/api/rebates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rebateDTO)))
            .andExpect(status().isCreated());

        // Validate the Rebate in the database
        List<Rebate> rebateList = rebateRepository.findAll();
        assertThat(rebateList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteRebate() throws Exception {
        // Initialize the database
        rebateRepository.saveAndFlush(rebate);
        int databaseSizeBeforeDelete = rebateRepository.findAll().size();

        // Get the rebate
        restRebateMockMvc.perform(delete("/api/rebates/{id}", rebate.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Rebate> rebateList = rebateRepository.findAll();
        assertThat(rebateList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Rebate.class);
        Rebate rebate1 = new Rebate();
        rebate1.setId(1L);
        Rebate rebate2 = new Rebate();
        rebate2.setId(rebate1.getId());
        assertThat(rebate1).isEqualTo(rebate2);
        rebate2.setId(2L);
        assertThat(rebate1).isNotEqualTo(rebate2);
        rebate1.setId(null);
        assertThat(rebate1).isNotEqualTo(rebate2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RebateDTO.class);
        RebateDTO rebateDTO1 = new RebateDTO();
        rebateDTO1.setId(1L);
        RebateDTO rebateDTO2 = new RebateDTO();
        assertThat(rebateDTO1).isNotEqualTo(rebateDTO2);
        rebateDTO2.setId(rebateDTO1.getId());
        assertThat(rebateDTO1).isEqualTo(rebateDTO2);
        rebateDTO2.setId(2L);
        assertThat(rebateDTO1).isNotEqualTo(rebateDTO2);
        rebateDTO1.setId(null);
        assertThat(rebateDTO1).isNotEqualTo(rebateDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(rebateMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(rebateMapper.fromId(null)).isNull();
    }
}
