(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('AuditTrailMySuffixDialogController', AuditTrailMySuffixDialogController);

    AuditTrailMySuffixDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'AuditTrail'];

    function AuditTrailMySuffixDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, AuditTrail) {
        var vm = this;

        vm.auditTrail = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.auditTrail.id !== null) {
                AuditTrail.update(vm.auditTrail, onSaveSuccess, onSaveError);
            } else {
                AuditTrail.save(vm.auditTrail, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('aspireLoanMgtApp:auditTrailUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.logDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
