(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('AuditTrailMySuffixDetailController', AuditTrailMySuffixDetailController);

    AuditTrailMySuffixDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'AuditTrail'];

    function AuditTrailMySuffixDetailController($scope, $rootScope, $stateParams, previousState, entity, AuditTrail) {
        var vm = this;

        vm.auditTrail = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('aspireLoanMgtApp:auditTrailUpdate', function(event, result) {
            vm.auditTrail = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
