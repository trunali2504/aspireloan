(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('AuditTrailMySuffixDeleteController',AuditTrailMySuffixDeleteController);

    AuditTrailMySuffixDeleteController.$inject = ['$uibModalInstance', 'entity', 'AuditTrail'];

    function AuditTrailMySuffixDeleteController($uibModalInstance, entity, AuditTrail) {
        var vm = this;

        vm.auditTrail = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            AuditTrail.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
