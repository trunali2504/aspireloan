(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('audit-trail-my-suffix', {
            parent: 'entity',
            url: '/audit-trail-my-suffix',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'aspireLoanMgtApp.auditTrail.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/audit-trail/audit-trailsmySuffix.html',
                    controller: 'AuditTrailMySuffixController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('auditTrail');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('audit-trail-my-suffix-detail', {
            parent: 'audit-trail-my-suffix',
            url: '/audit-trail-my-suffix/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'aspireLoanMgtApp.auditTrail.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/audit-trail/audit-trail-my-suffix-detail.html',
                    controller: 'AuditTrailMySuffixDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('auditTrail');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'AuditTrail', function($stateParams, AuditTrail) {
                    return AuditTrail.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'audit-trail-my-suffix',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('audit-trail-my-suffix-detail.edit', {
            parent: 'audit-trail-my-suffix-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/audit-trail/audit-trail-my-suffix-dialog.html',
                    controller: 'AuditTrailMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['AuditTrail', function(AuditTrail) {
                            return AuditTrail.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('audit-trail-my-suffix.new', {
            parent: 'audit-trail-my-suffix',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/audit-trail/audit-trail-my-suffix-dialog.html',
                    controller: 'AuditTrailMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                userCode: null,
                                logDate: null,
                                logTime: null,
                                moduleName: null,
                                action: null,
                                actionstatus: null,
                                itemcode: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('audit-trail-my-suffix', null, { reload: 'audit-trail-my-suffix' });
                }, function() {
                    $state.go('audit-trail-my-suffix');
                });
            }]
        })
        .state('audit-trail-my-suffix.edit', {
            parent: 'audit-trail-my-suffix',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/audit-trail/audit-trail-my-suffix-dialog.html',
                    controller: 'AuditTrailMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['AuditTrail', function(AuditTrail) {
                            return AuditTrail.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('audit-trail-my-suffix', null, { reload: 'audit-trail-my-suffix' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('audit-trail-my-suffix.delete', {
            parent: 'audit-trail-my-suffix',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/audit-trail/audit-trail-my-suffix-delete-dialog.html',
                    controller: 'AuditTrailMySuffixDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['AuditTrail', function(AuditTrail) {
                            return AuditTrail.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('audit-trail-my-suffix', null, { reload: 'audit-trail-my-suffix' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
