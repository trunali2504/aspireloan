(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('AuditTrailMySuffixController', AuditTrailMySuffixController);

    AuditTrailMySuffixController.$inject = ['AuditTrail'];

    function AuditTrailMySuffixController(AuditTrail) {

        var vm = this;

        vm.auditTrails = [];

        loadAll();

        function loadAll() {
            AuditTrail.query(function(result) {
                vm.auditTrails = result;
                vm.searchQuery = null;
            });
        }
    }
})();
