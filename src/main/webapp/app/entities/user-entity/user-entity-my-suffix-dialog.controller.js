(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('UserEntityMySuffixDialogController', UserEntityMySuffixDialogController);

    UserEntityMySuffixDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'UserEntity'];

    function UserEntityMySuffixDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, UserEntity) {
        var vm = this;

        vm.userEntity = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.userEntity.id !== null) {
                UserEntity.update(vm.userEntity, onSaveSuccess, onSaveError);
            } else {
                UserEntity.save(vm.userEntity, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('aspireLoanMgtApp:userEntityUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.createdDate = false;
        vm.datePickerOpenStatus.updatedDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
