(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('UserEntityMySuffixDeleteController',UserEntityMySuffixDeleteController);

    UserEntityMySuffixDeleteController.$inject = ['$uibModalInstance', 'entity', 'UserEntity'];

    function UserEntityMySuffixDeleteController($uibModalInstance, entity, UserEntity) {
        var vm = this;

        vm.userEntity = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            UserEntity.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
