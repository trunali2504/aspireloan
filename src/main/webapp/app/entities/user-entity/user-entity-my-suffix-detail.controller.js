(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('UserEntityMySuffixDetailController', UserEntityMySuffixDetailController);

    UserEntityMySuffixDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'UserEntity'];

    function UserEntityMySuffixDetailController($scope, $rootScope, $stateParams, previousState, entity, UserEntity) {
        var vm = this;

        vm.userEntity = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('aspireLoanMgtApp:userEntityUpdate', function(event, result) {
            vm.userEntity = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
