(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('UserEntityMySuffixController', UserEntityMySuffixController);

    UserEntityMySuffixController.$inject = ['UserEntity'];

    function UserEntityMySuffixController(UserEntity) {

        var vm = this;

        vm.userEntities = [];

        loadAll();

        function loadAll() {
            UserEntity.query(function(result) {
                vm.userEntities = result;
                vm.searchQuery = null;
            });
        }
    }
})();
