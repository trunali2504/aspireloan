(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('user-entity-my-suffix', {
            parent: 'entity',
            url: '/user-entity-my-suffix',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'aspireLoanMgtApp.userEntity.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-entity/user-entitiesmySuffix.html',
                    controller: 'UserEntityMySuffixController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('userEntity');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('user-entity-my-suffix-detail', {
            parent: 'user-entity-my-suffix',
            url: '/user-entity-my-suffix/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'aspireLoanMgtApp.userEntity.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-entity/user-entity-my-suffix-detail.html',
                    controller: 'UserEntityMySuffixDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('userEntity');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'UserEntity', function($stateParams, UserEntity) {
                    return UserEntity.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'user-entity-my-suffix',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('user-entity-my-suffix-detail.edit', {
            parent: 'user-entity-my-suffix-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-entity/user-entity-my-suffix-dialog.html',
                    controller: 'UserEntityMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserEntity', function(UserEntity) {
                            return UserEntity.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-entity-my-suffix.new', {
            parent: 'user-entity-my-suffix',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-entity/user-entity-my-suffix-dialog.html',
                    controller: 'UserEntityMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                mobileNo: null,
                                emailid: null,
                                tokenid: null,
                                status: null,
                                usertype: null,
                                company: null,
                                createdBy: null,
                                createdDate: null,
                                updatedBy: null,
                                updatedDate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('user-entity-my-suffix', null, { reload: 'user-entity-my-suffix' });
                }, function() {
                    $state.go('user-entity-my-suffix');
                });
            }]
        })
        .state('user-entity-my-suffix.edit', {
            parent: 'user-entity-my-suffix',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-entity/user-entity-my-suffix-dialog.html',
                    controller: 'UserEntityMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserEntity', function(UserEntity) {
                            return UserEntity.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-entity-my-suffix', null, { reload: 'user-entity-my-suffix' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-entity-my-suffix.delete', {
            parent: 'user-entity-my-suffix',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-entity/user-entity-my-suffix-delete-dialog.html',
                    controller: 'UserEntityMySuffixDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['UserEntity', function(UserEntity) {
                            return UserEntity.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-entity-my-suffix', null, { reload: 'user-entity-my-suffix' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
