(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('ProductMySuffixController', ProductMySuffixController);

    ProductMySuffixController.$inject = ['Product'];

    function ProductMySuffixController(Product) {

        var vm = this;

        vm.products = [];

        loadAll();

        function loadAll() {
            Product.query(function(result) {
                vm.products = result;
                vm.searchQuery = null;
            });
        }
    }
})();
