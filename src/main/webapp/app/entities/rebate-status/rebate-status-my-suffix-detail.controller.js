(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('RebateStatusMySuffixDetailController', RebateStatusMySuffixDetailController);

    RebateStatusMySuffixDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'RebateStatus'];

    function RebateStatusMySuffixDetailController($scope, $rootScope, $stateParams, previousState, entity, RebateStatus) {
        var vm = this;

        vm.rebateStatus = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('aspireLoanMgtApp:rebateStatusUpdate', function(event, result) {
            vm.rebateStatus = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
