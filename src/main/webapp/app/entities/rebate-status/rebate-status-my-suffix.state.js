(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('rebate-status-my-suffix', {
            parent: 'entity',
            url: '/rebate-status-my-suffix',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'aspireLoanMgtApp.rebateStatus.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/rebate-status/rebate-statusesmySuffix.html',
                    controller: 'RebateStatusMySuffixController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('rebateStatus');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('rebate-status-my-suffix-detail', {
            parent: 'rebate-status-my-suffix',
            url: '/rebate-status-my-suffix/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'aspireLoanMgtApp.rebateStatus.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/rebate-status/rebate-status-my-suffix-detail.html',
                    controller: 'RebateStatusMySuffixDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('rebateStatus');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'RebateStatus', function($stateParams, RebateStatus) {
                    return RebateStatus.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'rebate-status-my-suffix',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('rebate-status-my-suffix-detail.edit', {
            parent: 'rebate-status-my-suffix-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/rebate-status/rebate-status-my-suffix-dialog.html',
                    controller: 'RebateStatusMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['RebateStatus', function(RebateStatus) {
                            return RebateStatus.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('rebate-status-my-suffix.new', {
            parent: 'rebate-status-my-suffix',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/rebate-status/rebate-status-my-suffix-dialog.html',
                    controller: 'RebateStatusMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                applicationId: null,
                                rebateamount: null,
                                status: null,
                                lastremark: null,
                                applydate: null,
                                createdBy: null,
                                createdDate: null,
                                updatedBy: null,
                                updatedDate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('rebate-status-my-suffix', null, { reload: 'rebate-status-my-suffix' });
                }, function() {
                    $state.go('rebate-status-my-suffix');
                });
            }]
        })
        .state('rebate-status-my-suffix.edit', {
            parent: 'rebate-status-my-suffix',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/rebate-status/rebate-status-my-suffix-dialog.html',
                    controller: 'RebateStatusMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['RebateStatus', function(RebateStatus) {
                            return RebateStatus.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('rebate-status-my-suffix', null, { reload: 'rebate-status-my-suffix' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('rebate-status-my-suffix.delete', {
            parent: 'rebate-status-my-suffix',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/rebate-status/rebate-status-my-suffix-delete-dialog.html',
                    controller: 'RebateStatusMySuffixDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['RebateStatus', function(RebateStatus) {
                            return RebateStatus.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('rebate-status-my-suffix', null, { reload: 'rebate-status-my-suffix' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
