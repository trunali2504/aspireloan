(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('RebateStatusMySuffixDeleteController',RebateStatusMySuffixDeleteController);

    RebateStatusMySuffixDeleteController.$inject = ['$uibModalInstance', 'entity', 'RebateStatus'];

    function RebateStatusMySuffixDeleteController($uibModalInstance, entity, RebateStatus) {
        var vm = this;

        vm.rebateStatus = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            RebateStatus.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
