(function() {
    'use strict';
    angular
        .module('aspireLoanMgtApp')
        .factory('RebateStatus', RebateStatus);

    RebateStatus.$inject = ['$resource', 'DateUtils'];

    function RebateStatus ($resource, DateUtils) {
        var resourceUrl =  'api/rebate-statuses/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.applydate = DateUtils.convertLocalDateFromServer(data.applydate);
                        data.createdDate = DateUtils.convertLocalDateFromServer(data.createdDate);
                        data.updatedDate = DateUtils.convertLocalDateFromServer(data.updatedDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.applydate = DateUtils.convertLocalDateToServer(copy.applydate);
                    copy.createdDate = DateUtils.convertLocalDateToServer(copy.createdDate);
                    copy.updatedDate = DateUtils.convertLocalDateToServer(copy.updatedDate);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.applydate = DateUtils.convertLocalDateToServer(copy.applydate);
                    copy.createdDate = DateUtils.convertLocalDateToServer(copy.createdDate);
                    copy.updatedDate = DateUtils.convertLocalDateToServer(copy.updatedDate);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
