(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('RebateStatusMySuffixDialogController', RebateStatusMySuffixDialogController);

    RebateStatusMySuffixDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'RebateStatus'];

    function RebateStatusMySuffixDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, RebateStatus) {
        var vm = this;

        vm.rebateStatus = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.rebateStatus.id !== null) {
                RebateStatus.update(vm.rebateStatus, onSaveSuccess, onSaveError);
            } else {
                RebateStatus.save(vm.rebateStatus, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('aspireLoanMgtApp:rebateStatusUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.applydate = false;
        vm.datePickerOpenStatus.createdDate = false;
        vm.datePickerOpenStatus.updatedDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
