(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('RebateStatusMySuffixController', RebateStatusMySuffixController);

    RebateStatusMySuffixController.$inject = ['RebateStatus'];

    function RebateStatusMySuffixController(RebateStatus) {

        var vm = this;

        vm.rebateStatuses = [];

        loadAll();

        function loadAll() {
            RebateStatus.query(function(result) {
                vm.rebateStatuses = result;
                vm.searchQuery = null;
            });
        }
    }
})();
