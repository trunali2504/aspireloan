(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('TermsConditionMySuffixDetailController', TermsConditionMySuffixDetailController);

    TermsConditionMySuffixDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'TermsCondition'];

    function TermsConditionMySuffixDetailController($scope, $rootScope, $stateParams, previousState, entity, TermsCondition) {
        var vm = this;

        vm.termsCondition = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('aspireLoanMgtApp:termsConditionUpdate', function(event, result) {
            vm.termsCondition = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
