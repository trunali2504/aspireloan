(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('TermsConditionMySuffixController', TermsConditionMySuffixController);

    TermsConditionMySuffixController.$inject = ['TermsCondition'];

    function TermsConditionMySuffixController(TermsCondition) {

        var vm = this;

        vm.termsConditions = [];

        loadAll();

        function loadAll() {
            TermsCondition.query(function(result) {
                vm.termsConditions = result;
                vm.searchQuery = null;
            });
        }
    }
})();
