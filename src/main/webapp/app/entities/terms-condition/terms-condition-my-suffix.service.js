(function() {
    'use strict';
    angular
        .module('aspireLoanMgtApp')
        .factory('TermsCondition', TermsCondition);

    TermsCondition.$inject = ['$resource', 'DateUtils'];

    function TermsCondition ($resource, DateUtils) {
        var resourceUrl =  'api/terms-conditions/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.submissiondate = DateUtils.convertLocalDateFromServer(data.submissiondate);
                        data.fromDate = DateUtils.convertLocalDateFromServer(data.fromDate);
                        data.toDate = DateUtils.convertLocalDateFromServer(data.toDate);
                        data.createdDate = DateUtils.convertLocalDateFromServer(data.createdDate);
                        data.updatedDate = DateUtils.convertLocalDateFromServer(data.updatedDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.submissiondate = DateUtils.convertLocalDateToServer(copy.submissiondate);
                    copy.fromDate = DateUtils.convertLocalDateToServer(copy.fromDate);
                    copy.toDate = DateUtils.convertLocalDateToServer(copy.toDate);
                    copy.createdDate = DateUtils.convertLocalDateToServer(copy.createdDate);
                    copy.updatedDate = DateUtils.convertLocalDateToServer(copy.updatedDate);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.submissiondate = DateUtils.convertLocalDateToServer(copy.submissiondate);
                    copy.fromDate = DateUtils.convertLocalDateToServer(copy.fromDate);
                    copy.toDate = DateUtils.convertLocalDateToServer(copy.toDate);
                    copy.createdDate = DateUtils.convertLocalDateToServer(copy.createdDate);
                    copy.updatedDate = DateUtils.convertLocalDateToServer(copy.updatedDate);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
