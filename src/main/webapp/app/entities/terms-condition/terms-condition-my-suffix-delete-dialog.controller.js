(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('TermsConditionMySuffixDeleteController',TermsConditionMySuffixDeleteController);

    TermsConditionMySuffixDeleteController.$inject = ['$uibModalInstance', 'entity', 'TermsCondition'];

    function TermsConditionMySuffixDeleteController($uibModalInstance, entity, TermsCondition) {
        var vm = this;

        vm.termsCondition = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            TermsCondition.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
