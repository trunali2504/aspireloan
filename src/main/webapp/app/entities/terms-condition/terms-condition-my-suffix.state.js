(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('terms-condition-my-suffix', {
            parent: 'entity',
            url: '/terms-condition-my-suffix',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'aspireLoanMgtApp.termsCondition.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/terms-condition/terms-conditionsmySuffix.html',
                    controller: 'TermsConditionMySuffixController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('termsCondition');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('terms-condition-my-suffix-detail', {
            parent: 'terms-condition-my-suffix',
            url: '/terms-condition-my-suffix/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'aspireLoanMgtApp.termsCondition.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/terms-condition/terms-condition-my-suffix-detail.html',
                    controller: 'TermsConditionMySuffixDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('termsCondition');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'TermsCondition', function($stateParams, TermsCondition) {
                    return TermsCondition.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'terms-condition-my-suffix',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('terms-condition-my-suffix-detail.edit', {
            parent: 'terms-condition-my-suffix-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/terms-condition/terms-condition-my-suffix-dialog.html',
                    controller: 'TermsConditionMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TermsCondition', function(TermsCondition) {
                            return TermsCondition.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('terms-condition-my-suffix.new', {
            parent: 'terms-condition-my-suffix',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/terms-condition/terms-condition-my-suffix-dialog.html',
                    controller: 'TermsConditionMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                submissiondate: null,
                                termsAndConditions: null,
                                versionnumber: null,
                                fromDate: null,
                                toDate: null,
                                createdBy: null,
                                createdDate: null,
                                updatedBy: null,
                                updatedDate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('terms-condition-my-suffix', null, { reload: 'terms-condition-my-suffix' });
                }, function() {
                    $state.go('terms-condition-my-suffix');
                });
            }]
        })
        .state('terms-condition-my-suffix.edit', {
            parent: 'terms-condition-my-suffix',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/terms-condition/terms-condition-my-suffix-dialog.html',
                    controller: 'TermsConditionMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TermsCondition', function(TermsCondition) {
                            return TermsCondition.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('terms-condition-my-suffix', null, { reload: 'terms-condition-my-suffix' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('terms-condition-my-suffix.delete', {
            parent: 'terms-condition-my-suffix',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/terms-condition/terms-condition-my-suffix-delete-dialog.html',
                    controller: 'TermsConditionMySuffixDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['TermsCondition', function(TermsCondition) {
                            return TermsCondition.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('terms-condition-my-suffix', null, { reload: 'terms-condition-my-suffix' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
