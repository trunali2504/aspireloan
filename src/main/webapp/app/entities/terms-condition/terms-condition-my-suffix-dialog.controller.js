(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('TermsConditionMySuffixDialogController', TermsConditionMySuffixDialogController);

    TermsConditionMySuffixDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'TermsCondition'];

    function TermsConditionMySuffixDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, TermsCondition) {
        var vm = this;

        vm.termsCondition = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.termsCondition.id !== null) {
                TermsCondition.update(vm.termsCondition, onSaveSuccess, onSaveError);
            } else {
                TermsCondition.save(vm.termsCondition, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('aspireLoanMgtApp:termsConditionUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.submissiondate = false;
        vm.datePickerOpenStatus.fromDate = false;
        vm.datePickerOpenStatus.toDate = false;
        vm.datePickerOpenStatus.createdDate = false;
        vm.datePickerOpenStatus.updatedDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
