(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('ApplicationMySuffixDetailController', ApplicationMySuffixDetailController);

    ApplicationMySuffixDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Application'];

    function ApplicationMySuffixDetailController($scope, $rootScope, $stateParams, previousState, entity, Application) {
        var vm = this;

        vm.application = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('aspireLoanMgtApp:applicationUpdate', function(event, result) {
            vm.application = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
