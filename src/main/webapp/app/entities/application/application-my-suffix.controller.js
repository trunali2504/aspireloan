(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('ApplicationMySuffixController', ApplicationMySuffixController);

    ApplicationMySuffixController.$inject = ['Application'];

    function ApplicationMySuffixController(Application) {

        var vm = this;

        vm.applications = [];

        loadAll();

        function loadAll() {
            Application.query(function(result) {
                vm.applications = result;
                vm.searchQuery = null;
            });
        }
    }
})();
