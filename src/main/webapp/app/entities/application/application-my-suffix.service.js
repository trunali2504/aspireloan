(function() {
    'use strict';
    angular
        .module('aspireLoanMgtApp')
        .factory('Application', Application);

    Application.$inject = ['$resource', 'DateUtils'];

    function Application ($resource, DateUtils) {
        var resourceUrl =  'api/applications/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.applyDate = DateUtils.convertLocalDateFromServer(data.applyDate);
                        data.remarkDate = DateUtils.convertLocalDateFromServer(data.remarkDate);
                        data.createDate = DateUtils.convertLocalDateFromServer(data.createDate);
                        data.updateDate = DateUtils.convertLocalDateFromServer(data.updateDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.applyDate = DateUtils.convertLocalDateToServer(copy.applyDate);
                    copy.remarkDate = DateUtils.convertLocalDateToServer(copy.remarkDate);
                    copy.createDate = DateUtils.convertLocalDateToServer(copy.createDate);
                    copy.updateDate = DateUtils.convertLocalDateToServer(copy.updateDate);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.applyDate = DateUtils.convertLocalDateToServer(copy.applyDate);
                    copy.remarkDate = DateUtils.convertLocalDateToServer(copy.remarkDate);
                    copy.createDate = DateUtils.convertLocalDateToServer(copy.createDate);
                    copy.updateDate = DateUtils.convertLocalDateToServer(copy.updateDate);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
