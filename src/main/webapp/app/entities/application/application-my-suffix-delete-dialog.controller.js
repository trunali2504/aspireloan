(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('ApplicationMySuffixDeleteController',ApplicationMySuffixDeleteController);

    ApplicationMySuffixDeleteController.$inject = ['$uibModalInstance', 'entity', 'Application'];

    function ApplicationMySuffixDeleteController($uibModalInstance, entity, Application) {
        var vm = this;

        vm.application = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Application.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
