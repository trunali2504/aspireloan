(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('application-my-suffix', {
            parent: 'entity',
            url: '/application-my-suffix',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'aspireLoanMgtApp.application.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/application/applicationsmySuffix.html',
                    controller: 'ApplicationMySuffixController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('application');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('application-my-suffix-detail', {
            parent: 'application-my-suffix',
            url: '/application-my-suffix/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'aspireLoanMgtApp.application.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/application/application-my-suffix-detail.html',
                    controller: 'ApplicationMySuffixDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('application');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Application', function($stateParams, Application) {
                    return Application.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'application-my-suffix',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('application-my-suffix-detail.edit', {
            parent: 'application-my-suffix-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/application/application-my-suffix-dialog.html',
                    controller: 'ApplicationMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Application', function(Application) {
                            return Application.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('application-my-suffix.new', {
            parent: 'application-my-suffix',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/application/application-my-suffix-dialog.html',
                    controller: 'ApplicationMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                applyDate: null,
                                customerName: null,
                                userCode: null,
                                packageCode: null,
                                loanAmount: null,
                                employerName: null,
                                status: null,
                                remark: null,
                                remarkDate: null,
                                createDate: null,
                                updateDate: null,
                                createdBy: null,
                                updatedBy: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('application-my-suffix', null, { reload: 'application-my-suffix' });
                }, function() {
                    $state.go('application-my-suffix');
                });
            }]
        })
        .state('application-my-suffix.edit', {
            parent: 'application-my-suffix',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/application/application-my-suffix-dialog.html',
                    controller: 'ApplicationMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Application', function(Application) {
                            return Application.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('application-my-suffix', null, { reload: 'application-my-suffix' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('application-my-suffix.delete', {
            parent: 'application-my-suffix',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/application/application-my-suffix-delete-dialog.html',
                    controller: 'ApplicationMySuffixDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Application', function(Application) {
                            return Application.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('application-my-suffix', null, { reload: 'application-my-suffix' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
