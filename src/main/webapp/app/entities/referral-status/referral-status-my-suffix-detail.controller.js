(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('ReferralStatusMySuffixDetailController', ReferralStatusMySuffixDetailController);

    ReferralStatusMySuffixDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ReferralStatus'];

    function ReferralStatusMySuffixDetailController($scope, $rootScope, $stateParams, previousState, entity, ReferralStatus) {
        var vm = this;

        vm.referralStatus = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('aspireLoanMgtApp:referralStatusUpdate', function(event, result) {
            vm.referralStatus = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
