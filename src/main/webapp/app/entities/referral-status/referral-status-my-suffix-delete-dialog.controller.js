(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('ReferralStatusMySuffixDeleteController',ReferralStatusMySuffixDeleteController);

    ReferralStatusMySuffixDeleteController.$inject = ['$uibModalInstance', 'entity', 'ReferralStatus'];

    function ReferralStatusMySuffixDeleteController($uibModalInstance, entity, ReferralStatus) {
        var vm = this;

        vm.referralStatus = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ReferralStatus.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
