(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('ReferralStatusMySuffixController', ReferralStatusMySuffixController);

    ReferralStatusMySuffixController.$inject = ['ReferralStatus'];

    function ReferralStatusMySuffixController(ReferralStatus) {

        var vm = this;

        vm.referralStatuses = [];

        loadAll();

        function loadAll() {
            ReferralStatus.query(function(result) {
                vm.referralStatuses = result;
                vm.searchQuery = null;
            });
        }
    }
})();
