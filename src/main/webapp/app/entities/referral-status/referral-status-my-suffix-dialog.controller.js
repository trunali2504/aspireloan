(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('ReferralStatusMySuffixDialogController', ReferralStatusMySuffixDialogController);

    ReferralStatusMySuffixDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ReferralStatus'];

    function ReferralStatusMySuffixDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, ReferralStatus) {
        var vm = this;

        vm.referralStatus = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.referralStatus.id !== null) {
                ReferralStatus.update(vm.referralStatus, onSaveSuccess, onSaveError);
            } else {
                ReferralStatus.save(vm.referralStatus, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('aspireLoanMgtApp:referralStatusUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.validityfromdate = false;
        vm.datePickerOpenStatus.validitytodate = false;
        vm.datePickerOpenStatus.applydate = false;
        vm.datePickerOpenStatus.createdDate = false;
        vm.datePickerOpenStatus.updatedDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
