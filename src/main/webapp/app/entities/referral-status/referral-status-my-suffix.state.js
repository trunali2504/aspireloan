(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('referral-status-my-suffix', {
            parent: 'entity',
            url: '/referral-status-my-suffix',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'aspireLoanMgtApp.referralStatus.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/referral-status/referral-statusesmySuffix.html',
                    controller: 'ReferralStatusMySuffixController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('referralStatus');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('referral-status-my-suffix-detail', {
            parent: 'referral-status-my-suffix',
            url: '/referral-status-my-suffix/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'aspireLoanMgtApp.referralStatus.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/referral-status/referral-status-my-suffix-detail.html',
                    controller: 'ReferralStatusMySuffixDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('referralStatus');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ReferralStatus', function($stateParams, ReferralStatus) {
                    return ReferralStatus.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'referral-status-my-suffix',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('referral-status-my-suffix-detail.edit', {
            parent: 'referral-status-my-suffix-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/referral-status/referral-status-my-suffix-dialog.html',
                    controller: 'ReferralStatusMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ReferralStatus', function(ReferralStatus) {
                            return ReferralStatus.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('referral-status-my-suffix.new', {
            parent: 'referral-status-my-suffix',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/referral-status/referral-status-my-suffix-dialog.html',
                    controller: 'ReferralStatusMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                applicationid: null,
                                referralamount: null,
                                validityfromdate: null,
                                validitytodate: null,
                                status: null,
                                lastremark: null,
                                applydate: null,
                                createdBy: null,
                                createdDate: null,
                                updatedBy: null,
                                updatedDate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('referral-status-my-suffix', null, { reload: 'referral-status-my-suffix' });
                }, function() {
                    $state.go('referral-status-my-suffix');
                });
            }]
        })
        .state('referral-status-my-suffix.edit', {
            parent: 'referral-status-my-suffix',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/referral-status/referral-status-my-suffix-dialog.html',
                    controller: 'ReferralStatusMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ReferralStatus', function(ReferralStatus) {
                            return ReferralStatus.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('referral-status-my-suffix', null, { reload: 'referral-status-my-suffix' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('referral-status-my-suffix.delete', {
            parent: 'referral-status-my-suffix',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/referral-status/referral-status-my-suffix-delete-dialog.html',
                    controller: 'ReferralStatusMySuffixDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ReferralStatus', function(ReferralStatus) {
                            return ReferralStatus.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('referral-status-my-suffix', null, { reload: 'referral-status-my-suffix' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
