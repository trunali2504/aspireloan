(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('InvitationMySuffixDetailController', InvitationMySuffixDetailController);

    InvitationMySuffixDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Invitation'];

    function InvitationMySuffixDetailController($scope, $rootScope, $stateParams, previousState, entity, Invitation) {
        var vm = this;

        vm.invitation = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('aspireLoanMgtApp:invitationUpdate', function(event, result) {
            vm.invitation = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
