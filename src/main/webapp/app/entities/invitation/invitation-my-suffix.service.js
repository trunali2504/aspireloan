(function() {
    'use strict';
    angular
        .module('aspireLoanMgtApp')
        .factory('Invitation', Invitation);

    Invitation.$inject = ['$resource', 'DateUtils'];

    function Invitation ($resource, DateUtils) {
        var resourceUrl =  'api/invitations/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.date = DateUtils.convertLocalDateFromServer(data.date);
                        data.createdDate = DateUtils.convertLocalDateFromServer(data.createdDate);
                        data.updatedDate = DateUtils.convertLocalDateFromServer(data.updatedDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.date = DateUtils.convertLocalDateToServer(copy.date);
                    copy.createdDate = DateUtils.convertLocalDateToServer(copy.createdDate);
                    copy.updatedDate = DateUtils.convertLocalDateToServer(copy.updatedDate);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.date = DateUtils.convertLocalDateToServer(copy.date);
                    copy.createdDate = DateUtils.convertLocalDateToServer(copy.createdDate);
                    copy.updatedDate = DateUtils.convertLocalDateToServer(copy.updatedDate);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
