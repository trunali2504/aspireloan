(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('InvitationMySuffixController', InvitationMySuffixController);

    InvitationMySuffixController.$inject = ['Invitation'];

    function InvitationMySuffixController(Invitation) {

        var vm = this;

        vm.invitations = [];

        loadAll();

        function loadAll() {
            Invitation.query(function(result) {
                vm.invitations = result;
                vm.searchQuery = null;
            });
        }
    }
})();
