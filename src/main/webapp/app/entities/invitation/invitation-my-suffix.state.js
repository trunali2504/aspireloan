(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('invitation-my-suffix', {
            parent: 'entity',
            url: '/invitation-my-suffix',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'aspireLoanMgtApp.invitation.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/invitation/invitationsmySuffix.html',
                    controller: 'InvitationMySuffixController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('invitation');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('invitation-my-suffix-detail', {
            parent: 'invitation-my-suffix',
            url: '/invitation-my-suffix/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'aspireLoanMgtApp.invitation.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/invitation/invitation-my-suffix-detail.html',
                    controller: 'InvitationMySuffixDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('invitation');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Invitation', function($stateParams, Invitation) {
                    return Invitation.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'invitation-my-suffix',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('invitation-my-suffix-detail.edit', {
            parent: 'invitation-my-suffix-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/invitation/invitation-my-suffix-dialog.html',
                    controller: 'InvitationMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Invitation', function(Invitation) {
                            return Invitation.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('invitation-my-suffix.new', {
            parent: 'invitation-my-suffix',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/invitation/invitation-my-suffix-dialog.html',
                    controller: 'InvitationMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                emailID: null,
                                mobileNo: null,
                                date: null,
                                status: null,
                                userCode: null,
                                createdBy: null,
                                createdDate: null,
                                updatedBy: null,
                                updatedDate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('invitation-my-suffix', null, { reload: 'invitation-my-suffix' });
                }, function() {
                    $state.go('invitation-my-suffix');
                });
            }]
        })
        .state('invitation-my-suffix.edit', {
            parent: 'invitation-my-suffix',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/invitation/invitation-my-suffix-dialog.html',
                    controller: 'InvitationMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Invitation', function(Invitation) {
                            return Invitation.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('invitation-my-suffix', null, { reload: 'invitation-my-suffix' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('invitation-my-suffix.delete', {
            parent: 'invitation-my-suffix',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/invitation/invitation-my-suffix-delete-dialog.html',
                    controller: 'InvitationMySuffixDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Invitation', function(Invitation) {
                            return Invitation.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('invitation-my-suffix', null, { reload: 'invitation-my-suffix' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
