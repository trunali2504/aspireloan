(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('InvitationMySuffixDialogController', InvitationMySuffixDialogController);

    InvitationMySuffixDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Invitation'];

    function InvitationMySuffixDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Invitation) {
        var vm = this;

        vm.invitation = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.invitation.id !== null) {
                Invitation.update(vm.invitation, onSaveSuccess, onSaveError);
            } else {
                Invitation.save(vm.invitation, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('aspireLoanMgtApp:invitationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.date = false;
        vm.datePickerOpenStatus.createdDate = false;
        vm.datePickerOpenStatus.updatedDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
