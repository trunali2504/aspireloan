(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('InvitationMySuffixDeleteController',InvitationMySuffixDeleteController);

    InvitationMySuffixDeleteController.$inject = ['$uibModalInstance', 'entity', 'Invitation'];

    function InvitationMySuffixDeleteController($uibModalInstance, entity, Invitation) {
        var vm = this;

        vm.invitation = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Invitation.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
