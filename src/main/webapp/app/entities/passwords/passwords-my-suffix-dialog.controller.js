(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('PasswordsMySuffixDialogController', PasswordsMySuffixDialogController);

    PasswordsMySuffixDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Passwords'];

    function PasswordsMySuffixDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Passwords) {
        var vm = this;

        vm.passwords = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.passwords.id !== null) {
                Passwords.update(vm.passwords, onSaveSuccess, onSaveError);
            } else {
                Passwords.save(vm.passwords, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('aspireLoanMgtApp:passwordsUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.createdDate = false;
        vm.datePickerOpenStatus.updatedDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
