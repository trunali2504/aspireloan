(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('PasswordsMySuffixDeleteController',PasswordsMySuffixDeleteController);

    PasswordsMySuffixDeleteController.$inject = ['$uibModalInstance', 'entity', 'Passwords'];

    function PasswordsMySuffixDeleteController($uibModalInstance, entity, Passwords) {
        var vm = this;

        vm.passwords = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Passwords.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
