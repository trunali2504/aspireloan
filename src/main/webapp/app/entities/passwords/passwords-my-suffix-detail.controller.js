(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('PasswordsMySuffixDetailController', PasswordsMySuffixDetailController);

    PasswordsMySuffixDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Passwords'];

    function PasswordsMySuffixDetailController($scope, $rootScope, $stateParams, previousState, entity, Passwords) {
        var vm = this;

        vm.passwords = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('aspireLoanMgtApp:passwordsUpdate', function(event, result) {
            vm.passwords = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
