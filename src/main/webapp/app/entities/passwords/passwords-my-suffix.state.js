(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('passwords-my-suffix', {
            parent: 'entity',
            url: '/passwords-my-suffix',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'aspireLoanMgtApp.passwords.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/passwords/passwordsmySuffix.html',
                    controller: 'PasswordsMySuffixController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('passwords');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('passwords-my-suffix-detail', {
            parent: 'passwords-my-suffix',
            url: '/passwords-my-suffix/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'aspireLoanMgtApp.passwords.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/passwords/passwords-my-suffix-detail.html',
                    controller: 'PasswordsMySuffixDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('passwords');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Passwords', function($stateParams, Passwords) {
                    return Passwords.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'passwords-my-suffix',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('passwords-my-suffix-detail.edit', {
            parent: 'passwords-my-suffix-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/passwords/passwords-my-suffix-dialog.html',
                    controller: 'PasswordsMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Passwords', function(Passwords) {
                            return Passwords.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('passwords-my-suffix.new', {
            parent: 'passwords-my-suffix',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/passwords/passwords-my-suffix-dialog.html',
                    controller: 'PasswordsMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                userId: null,
                                password: null,
                                createdBy: null,
                                createdDate: null,
                                updatedBy: null,
                                updatedDate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('passwords-my-suffix', null, { reload: 'passwords-my-suffix' });
                }, function() {
                    $state.go('passwords-my-suffix');
                });
            }]
        })
        .state('passwords-my-suffix.edit', {
            parent: 'passwords-my-suffix',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/passwords/passwords-my-suffix-dialog.html',
                    controller: 'PasswordsMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Passwords', function(Passwords) {
                            return Passwords.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('passwords-my-suffix', null, { reload: 'passwords-my-suffix' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('passwords-my-suffix.delete', {
            parent: 'passwords-my-suffix',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/passwords/passwords-my-suffix-delete-dialog.html',
                    controller: 'PasswordsMySuffixDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Passwords', function(Passwords) {
                            return Passwords.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('passwords-my-suffix', null, { reload: 'passwords-my-suffix' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
