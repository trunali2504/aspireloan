(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('PasswordsMySuffixController', PasswordsMySuffixController);

    PasswordsMySuffixController.$inject = ['Passwords'];

    function PasswordsMySuffixController(Passwords) {

        var vm = this;

        vm.passwords = [];

        loadAll();

        function loadAll() {
            Passwords.query(function(result) {
                vm.passwords = result;
                vm.searchQuery = null;
            });
        }
    }
})();
