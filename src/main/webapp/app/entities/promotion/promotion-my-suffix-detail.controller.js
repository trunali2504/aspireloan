(function() {
    'use strict';

    angular
        .module('aspireloanApp')
        .controller('PromotionMySuffixDetailController', PromotionMySuffixDetailController);

    PromotionMySuffixDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Promotion'];

    function PromotionMySuffixDetailController($scope, $rootScope, $stateParams, previousState, entity, Promotion) {
        var vm = this;

        vm.promotion = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('aspireloanApp:promotionUpdate', function(event, result) {
            vm.promotion = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
