(function() {
    'use strict';

    angular
        .module('aspireloanApp')
        .controller('PromotionMySuffixController', PromotionMySuffixController);

    PromotionMySuffixController.$inject = ['Promotion'];

    function PromotionMySuffixController(Promotion) {

        var vm = this;

        vm.promotions = [];

        loadAll();

        function loadAll() {
            Promotion.query(function(result) {
                vm.promotions = result;
                vm.searchQuery = null;
            });
        }
    }
})();
