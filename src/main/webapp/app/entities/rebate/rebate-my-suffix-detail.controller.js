(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('RebateMySuffixDetailController', RebateMySuffixDetailController);

    RebateMySuffixDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Rebate'];

    function RebateMySuffixDetailController($scope, $rootScope, $stateParams, previousState, entity, Rebate) {
        var vm = this;

        vm.rebate = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('aspireLoanMgtApp:rebateUpdate', function(event, result) {
            vm.rebate = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
