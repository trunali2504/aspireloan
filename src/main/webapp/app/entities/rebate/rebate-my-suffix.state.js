(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('rebate-my-suffix', {
            parent: 'entity',
            url: '/rebate-my-suffix',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'aspireLoanMgtApp.rebate.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/rebate/rebatesmySuffix.html',
                    controller: 'RebateMySuffixController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('rebate');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('rebate-my-suffix-detail', {
            parent: 'rebate-my-suffix',
            url: '/rebate-my-suffix/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'aspireLoanMgtApp.rebate.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/rebate/rebate-my-suffix-detail.html',
                    controller: 'RebateMySuffixDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('rebate');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Rebate', function($stateParams, Rebate) {
                    return Rebate.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'rebate-my-suffix',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('rebate-my-suffix-detail.edit', {
            parent: 'rebate-my-suffix-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/rebate/rebate-my-suffix-dialog.html',
                    controller: 'RebateMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Rebate', function(Rebate) {
                            return Rebate.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('rebate-my-suffix.new', {
            parent: 'rebate-my-suffix',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/rebate/rebate-my-suffix-dialog.html',
                    controller: 'RebateMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                loanFrom: null,
                                loanTo: null,
                                loanPercentage: null,
                                maxamount: null,
                                validityfromdate: null,
                                validitytodate: null,
                                product: null,
                                createdBy: null,
                                createdDate: null,
                                updatedBy: null,
                                updatedDate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('rebate-my-suffix', null, { reload: 'rebate-my-suffix' });
                }, function() {
                    $state.go('rebate-my-suffix');
                });
            }]
        })
        .state('rebate-my-suffix.edit', {
            parent: 'rebate-my-suffix',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/rebate/rebate-my-suffix-dialog.html',
                    controller: 'RebateMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Rebate', function(Rebate) {
                            return Rebate.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('rebate-my-suffix', null, { reload: 'rebate-my-suffix' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('rebate-my-suffix.delete', {
            parent: 'rebate-my-suffix',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/rebate/rebate-my-suffix-delete-dialog.html',
                    controller: 'RebateMySuffixDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Rebate', function(Rebate) {
                            return Rebate.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('rebate-my-suffix', null, { reload: 'rebate-my-suffix' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
