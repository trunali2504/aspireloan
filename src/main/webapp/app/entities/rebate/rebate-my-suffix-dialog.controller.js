(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('RebateMySuffixDialogController', RebateMySuffixDialogController);

    RebateMySuffixDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Rebate'];

    function RebateMySuffixDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Rebate) {
        var vm = this;

        vm.rebate = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.rebate.id !== null) {
                Rebate.update(vm.rebate, onSaveSuccess, onSaveError);
            } else {
                Rebate.save(vm.rebate, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('aspireLoanMgtApp:rebateUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.validityfromdate = false;
        vm.datePickerOpenStatus.validitytodate = false;
        vm.datePickerOpenStatus.createdDate = false;
        vm.datePickerOpenStatus.updatedDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
