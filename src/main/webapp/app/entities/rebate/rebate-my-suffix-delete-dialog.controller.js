(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('RebateMySuffixDeleteController',RebateMySuffixDeleteController);

    RebateMySuffixDeleteController.$inject = ['$uibModalInstance', 'entity', 'Rebate'];

    function RebateMySuffixDeleteController($uibModalInstance, entity, Rebate) {
        var vm = this;

        vm.rebate = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Rebate.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
