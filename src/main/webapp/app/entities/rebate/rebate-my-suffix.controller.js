(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('RebateMySuffixController', RebateMySuffixController);

    RebateMySuffixController.$inject = ['Rebate'];

    function RebateMySuffixController(Rebate) {

        var vm = this;

        vm.rebates = [];

        loadAll();

        function loadAll() {
            Rebate.query(function(result) {
                vm.rebates = result;
                vm.searchQuery = null;
            });
        }
    }
})();
