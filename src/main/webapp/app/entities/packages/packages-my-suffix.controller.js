(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('PackagesMySuffixController', PackagesMySuffixController);

    PackagesMySuffixController.$inject = ['Packages'];

    function PackagesMySuffixController(Packages) {

        var vm = this;

        vm.packages = [];

        loadAll();

        function loadAll() {
            Packages.query(function(result) {
                vm.packages = result;
                vm.searchQuery = null;
            });
        }
    }
})();
