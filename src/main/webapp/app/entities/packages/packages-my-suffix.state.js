(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('packages-my-suffix', {
            parent: 'entity',
            url: '/packages-my-suffix',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'aspireLoanMgtApp.packages.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/packages/packagesmySuffix.html',
                    controller: 'PackagesMySuffixController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('packages');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('packages-my-suffix-detail', {
            parent: 'packages-my-suffix',
            url: '/packages-my-suffix/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'aspireLoanMgtApp.packages.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/packages/packages-my-suffix-detail.html',
                    controller: 'PackagesMySuffixDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('packages');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Packages', function($stateParams, Packages) {
                    return Packages.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'packages-my-suffix',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('packages-my-suffix-detail.edit', {
            parent: 'packages-my-suffix-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/packages/packages-my-suffix-dialog.html',
                    controller: 'PackagesMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Packages', function(Packages) {
                            return Packages.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('packages-my-suffix.new', {
            parent: 'packages-my-suffix',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/packages/packages-my-suffix-dialog.html',
                    controller: 'PackagesMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                validityfromdate: null,
                                validitytodate: null,
                                typeOfProduct: null,
                                interestrate: null,
                                maxloanamount: null,
                                minloanamount: null,
                                maxtenure: null,
                                mintenure: null,
                                totalpayout: null,
                                createdBy: null,
                                createdDate: null,
                                updatedBy: null,
                                updatedDate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('packages-my-suffix', null, { reload: 'packages-my-suffix' });
                }, function() {
                    $state.go('packages-my-suffix');
                });
            }]
        })
        .state('packages-my-suffix.edit', {
            parent: 'packages-my-suffix',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/packages/packages-my-suffix-dialog.html',
                    controller: 'PackagesMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Packages', function(Packages) {
                            return Packages.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('packages-my-suffix', null, { reload: 'packages-my-suffix' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('packages-my-suffix.delete', {
            parent: 'packages-my-suffix',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/packages/packages-my-suffix-delete-dialog.html',
                    controller: 'PackagesMySuffixDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Packages', function(Packages) {
                            return Packages.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('packages-my-suffix', null, { reload: 'packages-my-suffix' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
