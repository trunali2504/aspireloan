(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('PackagesMySuffixDialogController', PackagesMySuffixDialogController);

    PackagesMySuffixDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Packages'];

    function PackagesMySuffixDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Packages) {
        var vm = this;

        vm.packages = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.packages.id !== null) {
                Packages.update(vm.packages, onSaveSuccess, onSaveError);
            } else {
                Packages.save(vm.packages, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('aspireLoanMgtApp:packagesUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.validityfromdate = false;
        vm.datePickerOpenStatus.validitytodate = false;
        vm.datePickerOpenStatus.createdDate = false;
        vm.datePickerOpenStatus.updatedDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
