(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('PackagesMySuffixDetailController', PackagesMySuffixDetailController);

    PackagesMySuffixDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Packages'];

    function PackagesMySuffixDetailController($scope, $rootScope, $stateParams, previousState, entity, Packages) {
        var vm = this;

        vm.packages = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('aspireLoanMgtApp:packagesUpdate', function(event, result) {
            vm.packages = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
