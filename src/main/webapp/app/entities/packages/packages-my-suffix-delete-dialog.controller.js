(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('PackagesMySuffixDeleteController',PackagesMySuffixDeleteController);

    PackagesMySuffixDeleteController.$inject = ['$uibModalInstance', 'entity', 'Packages'];

    function PackagesMySuffixDeleteController($uibModalInstance, entity, Packages) {
        var vm = this;

        vm.packages = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Packages.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
