(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('CompanyMySuffixController', CompanyMySuffixController);

    CompanyMySuffixController.$inject = ['Company'];

    function CompanyMySuffixController(Company) {

        var vm = this;

        vm.companies = [];

        loadAll();

        function loadAll() {
            Company.query(function(result) {
                vm.companies = result;
                vm.searchQuery = null;
            });
        }
    }
})();
