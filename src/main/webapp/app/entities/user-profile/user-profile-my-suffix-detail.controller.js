(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('UserProfileMySuffixDetailController', UserProfileMySuffixDetailController);

    UserProfileMySuffixDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'UserProfile'];

    function UserProfileMySuffixDetailController($scope, $rootScope, $stateParams, previousState, entity, UserProfile) {
        var vm = this;

        vm.userProfile = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('aspireLoanMgtApp:userProfileUpdate', function(event, result) {
            vm.userProfile = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
