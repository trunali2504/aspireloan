(function() {
    'use strict';
    angular
        .module('aspireLoanMgtApp')
        .factory('UserProfile', UserProfile);

    UserProfile.$inject = ['$resource', 'DateUtils'];

    function UserProfile ($resource, DateUtils) {
        var resourceUrl =  'api/user-profiles/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.createdDate = DateUtils.convertLocalDateFromServer(data.createdDate);
                        data.updatedDate = DateUtils.convertLocalDateFromServer(data.updatedDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.createdDate = DateUtils.convertLocalDateToServer(copy.createdDate);
                    copy.updatedDate = DateUtils.convertLocalDateToServer(copy.updatedDate);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.createdDate = DateUtils.convertLocalDateToServer(copy.createdDate);
                    copy.updatedDate = DateUtils.convertLocalDateToServer(copy.updatedDate);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
