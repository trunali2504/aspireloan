(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('user-profile-my-suffix', {
            parent: 'entity',
            url: '/user-profile-my-suffix',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'aspireLoanMgtApp.userProfile.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-profile/user-profilesmySuffix.html',
                    controller: 'UserProfileMySuffixController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('userProfile');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('user-profile-my-suffix-detail', {
            parent: 'user-profile-my-suffix',
            url: '/user-profile-my-suffix/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'aspireLoanMgtApp.userProfile.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-profile/user-profile-my-suffix-detail.html',
                    controller: 'UserProfileMySuffixDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('userProfile');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'UserProfile', function($stateParams, UserProfile) {
                    return UserProfile.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'user-profile-my-suffix',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('user-profile-my-suffix-detail.edit', {
            parent: 'user-profile-my-suffix-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-profile/user-profile-my-suffix-dialog.html',
                    controller: 'UserProfileMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserProfile', function(UserProfile) {
                            return UserProfile.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-profile-my-suffix.new', {
            parent: 'user-profile-my-suffix',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-profile/user-profile-my-suffix-dialog.html',
                    controller: 'UserProfileMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                nric: null,
                                jobtitle: null,
                                employername: null,
                                retirementage: null,
                                age: null,
                                state: null,
                                city: null,
                                postcode: null,
                                street: null,
                                bankname: null,
                                bankaccountnumber: null,
                                userId: null,
                                createdBy: null,
                                createdDate: null,
                                updatedBy: null,
                                updatedDate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('user-profile-my-suffix', null, { reload: 'user-profile-my-suffix' });
                }, function() {
                    $state.go('user-profile-my-suffix');
                });
            }]
        })
        .state('user-profile-my-suffix.edit', {
            parent: 'user-profile-my-suffix',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-profile/user-profile-my-suffix-dialog.html',
                    controller: 'UserProfileMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserProfile', function(UserProfile) {
                            return UserProfile.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-profile-my-suffix', null, { reload: 'user-profile-my-suffix' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-profile-my-suffix.delete', {
            parent: 'user-profile-my-suffix',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-profile/user-profile-my-suffix-delete-dialog.html',
                    controller: 'UserProfileMySuffixDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['UserProfile', function(UserProfile) {
                            return UserProfile.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-profile-my-suffix', null, { reload: 'user-profile-my-suffix' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
