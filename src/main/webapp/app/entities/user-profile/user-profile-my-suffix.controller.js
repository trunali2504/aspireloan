(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('UserProfileMySuffixController', UserProfileMySuffixController);

    UserProfileMySuffixController.$inject = ['UserProfile'];

    function UserProfileMySuffixController(UserProfile) {

        var vm = this;

        vm.userProfiles = [];

        loadAll();

        function loadAll() {
            UserProfile.query(function(result) {
                vm.userProfiles = result;
                vm.searchQuery = null;
            });
        }
    }
})();
