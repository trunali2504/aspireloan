(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('ReferralMySuffixDialogController', ReferralMySuffixDialogController);

    ReferralMySuffixDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Referral'];

    function ReferralMySuffixDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Referral) {
        var vm = this;

        vm.referral = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.referral.id !== null) {
                Referral.update(vm.referral, onSaveSuccess, onSaveError);
            } else {
                Referral.save(vm.referral, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('aspireLoanMgtApp:referralUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.validityfromdate = false;
        vm.datePickerOpenStatus.validitytodate = false;
        vm.datePickerOpenStatus.createdDate = false;
        vm.datePickerOpenStatus.updatedDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
