(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('ReferralMySuffixDeleteController',ReferralMySuffixDeleteController);

    ReferralMySuffixDeleteController.$inject = ['$uibModalInstance', 'entity', 'Referral'];

    function ReferralMySuffixDeleteController($uibModalInstance, entity, Referral) {
        var vm = this;

        vm.referral = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Referral.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
