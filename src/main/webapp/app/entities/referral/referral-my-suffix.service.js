(function() {
    'use strict';
    angular
        .module('aspireLoanMgtApp')
        .factory('Referral', Referral);

    Referral.$inject = ['$resource', 'DateUtils'];

    function Referral ($resource, DateUtils) {
        var resourceUrl =  'api/referrals/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.validityfromdate = DateUtils.convertLocalDateFromServer(data.validityfromdate);
                        data.validitytodate = DateUtils.convertLocalDateFromServer(data.validitytodate);
                        data.createdDate = DateUtils.convertLocalDateFromServer(data.createdDate);
                        data.updatedDate = DateUtils.convertLocalDateFromServer(data.updatedDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.validityfromdate = DateUtils.convertLocalDateToServer(copy.validityfromdate);
                    copy.validitytodate = DateUtils.convertLocalDateToServer(copy.validitytodate);
                    copy.createdDate = DateUtils.convertLocalDateToServer(copy.createdDate);
                    copy.updatedDate = DateUtils.convertLocalDateToServer(copy.updatedDate);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.validityfromdate = DateUtils.convertLocalDateToServer(copy.validityfromdate);
                    copy.validitytodate = DateUtils.convertLocalDateToServer(copy.validitytodate);
                    copy.createdDate = DateUtils.convertLocalDateToServer(copy.createdDate);
                    copy.updatedDate = DateUtils.convertLocalDateToServer(copy.updatedDate);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
