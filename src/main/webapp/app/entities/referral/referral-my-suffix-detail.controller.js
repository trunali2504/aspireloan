(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('ReferralMySuffixDetailController', ReferralMySuffixDetailController);

    ReferralMySuffixDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Referral'];

    function ReferralMySuffixDetailController($scope, $rootScope, $stateParams, previousState, entity, Referral) {
        var vm = this;

        vm.referral = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('aspireLoanMgtApp:referralUpdate', function(event, result) {
            vm.referral = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
