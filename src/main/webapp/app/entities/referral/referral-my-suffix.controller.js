(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('ReferralMySuffixController', ReferralMySuffixController);

    ReferralMySuffixController.$inject = ['Referral'];

    function ReferralMySuffixController(Referral) {

        var vm = this;

        vm.referrals = [];

        loadAll();

        function loadAll() {
            Referral.query(function(result) {
                vm.referrals = result;
                vm.searchQuery = null;
            });
        }
    }
})();
