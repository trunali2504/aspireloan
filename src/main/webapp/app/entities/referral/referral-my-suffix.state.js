(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('referral-my-suffix', {
            parent: 'entity',
            url: '/referral-my-suffix',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'aspireLoanMgtApp.referral.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/referral/referralsmySuffix.html',
                    controller: 'ReferralMySuffixController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('referral');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('referral-my-suffix-detail', {
            parent: 'referral-my-suffix',
            url: '/referral-my-suffix/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'aspireLoanMgtApp.referral.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/referral/referral-my-suffix-detail.html',
                    controller: 'ReferralMySuffixDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('referral');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Referral', function($stateParams, Referral) {
                    return Referral.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'referral-my-suffix',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('referral-my-suffix-detail.edit', {
            parent: 'referral-my-suffix-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/referral/referral-my-suffix-dialog.html',
                    controller: 'ReferralMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Referral', function(Referral) {
                            return Referral.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('referral-my-suffix.new', {
            parent: 'referral-my-suffix',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/referral/referral-my-suffix-dialog.html',
                    controller: 'ReferralMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                numberofsuccessfullreferral: null,
                                referralbonus: null,
                                validityfromdate: null,
                                validitytodate: null,
                                createdBy: null,
                                createdDate: null,
                                updatedBy: null,
                                updatedDate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('referral-my-suffix', null, { reload: 'referral-my-suffix' });
                }, function() {
                    $state.go('referral-my-suffix');
                });
            }]
        })
        .state('referral-my-suffix.edit', {
            parent: 'referral-my-suffix',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/referral/referral-my-suffix-dialog.html',
                    controller: 'ReferralMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Referral', function(Referral) {
                            return Referral.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('referral-my-suffix', null, { reload: 'referral-my-suffix' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('referral-my-suffix.delete', {
            parent: 'referral-my-suffix',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/referral/referral-my-suffix-delete-dialog.html',
                    controller: 'ReferralMySuffixDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Referral', function(Referral) {
                            return Referral.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('referral-my-suffix', null, { reload: 'referral-my-suffix' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
