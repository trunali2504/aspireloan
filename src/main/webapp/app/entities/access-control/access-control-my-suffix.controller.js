(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('AccessControlMySuffixController', AccessControlMySuffixController);

    AccessControlMySuffixController.$inject = ['AccessControl'];

    function AccessControlMySuffixController(AccessControl) {

        var vm = this;

        vm.accessControls = [];

        loadAll();

        function loadAll() {
            AccessControl.query(function(result) {
                vm.accessControls = result;
                vm.searchQuery = null;
            });
        }
    }
})();
