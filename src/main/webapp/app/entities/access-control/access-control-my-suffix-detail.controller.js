(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('AccessControlMySuffixDetailController', AccessControlMySuffixDetailController);

    AccessControlMySuffixDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'AccessControl'];

    function AccessControlMySuffixDetailController($scope, $rootScope, $stateParams, previousState, entity, AccessControl) {
        var vm = this;

        vm.accessControl = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('aspireLoanMgtApp:accessControlUpdate', function(event, result) {
            vm.accessControl = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
