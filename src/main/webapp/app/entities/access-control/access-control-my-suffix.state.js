(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('access-control-my-suffix', {
            parent: 'entity',
            url: '/access-control-my-suffix',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'aspireLoanMgtApp.accessControl.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/access-control/access-controlsmySuffix.html',
                    controller: 'AccessControlMySuffixController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('accessControl');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('access-control-my-suffix-detail', {
            parent: 'access-control-my-suffix',
            url: '/access-control-my-suffix/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'aspireLoanMgtApp.accessControl.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/access-control/access-control-my-suffix-detail.html',
                    controller: 'AccessControlMySuffixDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('accessControl');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'AccessControl', function($stateParams, AccessControl) {
                    return AccessControl.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'access-control-my-suffix',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('access-control-my-suffix-detail.edit', {
            parent: 'access-control-my-suffix-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/access-control/access-control-my-suffix-dialog.html',
                    controller: 'AccessControlMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['AccessControl', function(AccessControl) {
                            return AccessControl.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('access-control-my-suffix.new', {
            parent: 'access-control-my-suffix',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/access-control/access-control-my-suffix-dialog.html',
                    controller: 'AccessControlMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                userType: null,
                                permissionType: null,
                                status: null,
                                createdBy: null,
                                updatedBy: null,
                                createDate: null,
                                updateDate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('access-control-my-suffix', null, { reload: 'access-control-my-suffix' });
                }, function() {
                    $state.go('access-control-my-suffix');
                });
            }]
        })
        .state('access-control-my-suffix.edit', {
            parent: 'access-control-my-suffix',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/access-control/access-control-my-suffix-dialog.html',
                    controller: 'AccessControlMySuffixDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['AccessControl', function(AccessControl) {
                            return AccessControl.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('access-control-my-suffix', null, { reload: 'access-control-my-suffix' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('access-control-my-suffix.delete', {
            parent: 'access-control-my-suffix',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/access-control/access-control-my-suffix-delete-dialog.html',
                    controller: 'AccessControlMySuffixDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['AccessControl', function(AccessControl) {
                            return AccessControl.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('access-control-my-suffix', null, { reload: 'access-control-my-suffix' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
