(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('AccessControlMySuffixDeleteController',AccessControlMySuffixDeleteController);

    AccessControlMySuffixDeleteController.$inject = ['$uibModalInstance', 'entity', 'AccessControl'];

    function AccessControlMySuffixDeleteController($uibModalInstance, entity, AccessControl) {
        var vm = this;

        vm.accessControl = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            AccessControl.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
