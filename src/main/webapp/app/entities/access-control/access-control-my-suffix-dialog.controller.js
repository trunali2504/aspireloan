(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .controller('AccessControlMySuffixDialogController', AccessControlMySuffixDialogController);

    AccessControlMySuffixDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'AccessControl'];

    function AccessControlMySuffixDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, AccessControl) {
        var vm = this;

        vm.accessControl = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.accessControl.id !== null) {
                AccessControl.update(vm.accessControl, onSaveSuccess, onSaveError);
            } else {
                AccessControl.save(vm.accessControl, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('aspireLoanMgtApp:accessControlUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.createDate = false;
        vm.datePickerOpenStatus.updateDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
