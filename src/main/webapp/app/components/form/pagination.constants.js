(function() {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
