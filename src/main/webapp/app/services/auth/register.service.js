(function () {
    'use strict';

    angular
        .module('aspireLoanMgtApp')
        .factory('Register', Register);

    Register.$inject = ['$resource'];

    function Register ($resource) {
        return $resource('api/register', {}, {});
    }
})();
