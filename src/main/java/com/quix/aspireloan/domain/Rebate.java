package com.quix.aspireloan.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Rebate.
 */
@Entity
@Table(name = "rebate")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Rebate implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "loan_from")
    private Integer loanFrom;

    @Column(name = "loan_to")
    private String loanTo;

    @Column(name = "loan_percentage")
    private Integer loanPercentage;

    @Column(name = "maxamount")
    private Integer maxamount;

    @Column(name = "validityfromdate")
    private LocalDate validityfromdate;

    @Column(name = "validitytodate")
    private LocalDate validitytodate;

    @Column(name = "product")
    private String product;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_date")
    private LocalDate createdDate;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_date")
    private LocalDate updatedDate;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getLoanFrom() {
        return loanFrom;
    }

    public Rebate loanFrom(Integer loanFrom) {
        this.loanFrom = loanFrom;
        return this;
    }

    public void setLoanFrom(Integer loanFrom) {
        this.loanFrom = loanFrom;
    }

    public String getLoanTo() {
        return loanTo;
    }

    public Rebate loanTo(String loanTo) {
        this.loanTo = loanTo;
        return this;
    }

    public void setLoanTo(String loanTo) {
        this.loanTo = loanTo;
    }

    public Integer getLoanPercentage() {
        return loanPercentage;
    }

    public Rebate loanPercentage(Integer loanPercentage) {
        this.loanPercentage = loanPercentage;
        return this;
    }

    public void setLoanPercentage(Integer loanPercentage) {
        this.loanPercentage = loanPercentage;
    }

    public Integer getMaxamount() {
        return maxamount;
    }

    public Rebate maxamount(Integer maxamount) {
        this.maxamount = maxamount;
        return this;
    }

    public void setMaxamount(Integer maxamount) {
        this.maxamount = maxamount;
    }

    public LocalDate getValidityfromdate() {
        return validityfromdate;
    }

    public Rebate validityfromdate(LocalDate validityfromdate) {
        this.validityfromdate = validityfromdate;
        return this;
    }

    public void setValidityfromdate(LocalDate validityfromdate) {
        this.validityfromdate = validityfromdate;
    }

    public LocalDate getValiditytodate() {
        return validitytodate;
    }

    public Rebate validitytodate(LocalDate validitytodate) {
        this.validitytodate = validitytodate;
        return this;
    }

    public void setValiditytodate(LocalDate validitytodate) {
        this.validitytodate = validitytodate;
    }

    public String getProduct() {
        return product;
    }

    public Rebate product(String product) {
        this.product = product;
        return this;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Rebate createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public Rebate createdDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public Rebate updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public LocalDate getUpdatedDate() {
        return updatedDate;
    }

    public Rebate updatedDate(LocalDate updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public void setUpdatedDate(LocalDate updatedDate) {
        this.updatedDate = updatedDate;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Rebate rebate = (Rebate) o;
        if (rebate.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), rebate.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Rebate{" +
            "id=" + getId() +
            ", loanFrom='" + getLoanFrom() + "'" +
            ", loanTo='" + getLoanTo() + "'" +
            ", loanPercentage='" + getLoanPercentage() + "'" +
            ", maxamount='" + getMaxamount() + "'" +
            ", validityfromdate='" + getValidityfromdate() + "'" +
            ", validitytodate='" + getValiditytodate() + "'" +
            ", product='" + getProduct() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            "}";
    }
}
