package com.quix.aspireloan.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A UserProfile.
 */
@Entity
@Table(name = "user_profile")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class UserProfile implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "nric")
    private Integer nric;

    @Column(name = "jobtitle")
    private String jobtitle;

    @Column(name = "employername")
    private String employername;

    @Column(name = "retirementage")
    private Integer retirementage;

    @Column(name = "age")
    private Integer age;

    @Column(name = "state")
    private String state;

    @Column(name = "city")
    private String city;

    @Column(name = "postcode")
    private Integer postcode;

    @Column(name = "street")
    private String street;

    @Column(name = "bankname")
    private String bankname;

    @Column(name = "bankaccountnumber")
    private Integer bankaccountnumber;

    @Column(name = "user_id")
    private Integer userId;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_date")
    private LocalDate createdDate;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_date")
    private LocalDate updatedDate;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public UserProfile name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNric() {
        return nric;
    }

    public UserProfile nric(Integer nric) {
        this.nric = nric;
        return this;
    }

    public void setNric(Integer nric) {
        this.nric = nric;
    }

    public String getJobtitle() {
        return jobtitle;
    }

    public UserProfile jobtitle(String jobtitle) {
        this.jobtitle = jobtitle;
        return this;
    }

    public void setJobtitle(String jobtitle) {
        this.jobtitle = jobtitle;
    }

    public String getEmployername() {
        return employername;
    }

    public UserProfile employername(String employername) {
        this.employername = employername;
        return this;
    }

    public void setEmployername(String employername) {
        this.employername = employername;
    }

    public Integer getRetirementage() {
        return retirementage;
    }

    public UserProfile retirementage(Integer retirementage) {
        this.retirementage = retirementage;
        return this;
    }

    public void setRetirementage(Integer retirementage) {
        this.retirementage = retirementage;
    }

    public Integer getAge() {
        return age;
    }

    public UserProfile age(Integer age) {
        this.age = age;
        return this;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getState() {
        return state;
    }

    public UserProfile state(String state) {
        this.state = state;
        return this;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public UserProfile city(String city) {
        this.city = city;
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getPostcode() {
        return postcode;
    }

    public UserProfile postcode(Integer postcode) {
        this.postcode = postcode;
        return this;
    }

    public void setPostcode(Integer postcode) {
        this.postcode = postcode;
    }

    public String getStreet() {
        return street;
    }

    public UserProfile street(String street) {
        this.street = street;
        return this;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBankname() {
        return bankname;
    }

    public UserProfile bankname(String bankname) {
        this.bankname = bankname;
        return this;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public Integer getBankaccountnumber() {
        return bankaccountnumber;
    }

    public UserProfile bankaccountnumber(Integer bankaccountnumber) {
        this.bankaccountnumber = bankaccountnumber;
        return this;
    }

    public void setBankaccountnumber(Integer bankaccountnumber) {
        this.bankaccountnumber = bankaccountnumber;
    }

    public Integer getUserId() {
        return userId;
    }

    public UserProfile userId(Integer userId) {
        this.userId = userId;
        return this;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public UserProfile createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public UserProfile createdDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public UserProfile updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public LocalDate getUpdatedDate() {
        return updatedDate;
    }

    public UserProfile updatedDate(LocalDate updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public void setUpdatedDate(LocalDate updatedDate) {
        this.updatedDate = updatedDate;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserProfile userProfile = (UserProfile) o;
        if (userProfile.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userProfile.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserProfile{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", nric='" + getNric() + "'" +
            ", jobtitle='" + getJobtitle() + "'" +
            ", employername='" + getEmployername() + "'" +
            ", retirementage='" + getRetirementage() + "'" +
            ", age='" + getAge() + "'" +
            ", state='" + getState() + "'" +
            ", city='" + getCity() + "'" +
            ", postcode='" + getPostcode() + "'" +
            ", street='" + getStreet() + "'" +
            ", bankname='" + getBankname() + "'" +
            ", bankaccountnumber='" + getBankaccountnumber() + "'" +
            ", userId='" + getUserId() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            "}";
    }
}
