package com.quix.aspireloan.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Application.
 */
@Entity
@Table(name = "application")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Application implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "apply_date")
    private LocalDate applyDate;

    @Column(name = "customer_name")
    private String customerName;

    @Column(name = "user_code")
    private Integer userCode;

    @Column(name = "package_code")
    private Integer packageCode;

    @Column(name = "loan_amount")
    private Integer loanAmount;

    @Column(name = "employer_name")
    private String employerName;

    @Column(name = "status")
    private String status;

    @Column(name = "remark")
    private String remark;

    @Column(name = "remark_date")
    private LocalDate remarkDate;

    @Column(name = "create_date")
    private LocalDate createDate;

    @Column(name = "update_date")
    private LocalDate updateDate;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_by")
    private String updatedBy;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getApplyDate() {
        return applyDate;
    }

    public Application applyDate(LocalDate applyDate) {
        this.applyDate = applyDate;
        return this;
    }

    public void setApplyDate(LocalDate applyDate) {
        this.applyDate = applyDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public Application customerName(String customerName) {
        this.customerName = customerName;
        return this;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Integer getUserCode() {
        return userCode;
    }

    public Application userCode(Integer userCode) {
        this.userCode = userCode;
        return this;
    }

    public void setUserCode(Integer userCode) {
        this.userCode = userCode;
    }

    public Integer getPackageCode() {
        return packageCode;
    }

    public Application packageCode(Integer packageCode) {
        this.packageCode = packageCode;
        return this;
    }

    public void setPackageCode(Integer packageCode) {
        this.packageCode = packageCode;
    }

    public Integer getLoanAmount() {
        return loanAmount;
    }

    public Application loanAmount(Integer loanAmount) {
        this.loanAmount = loanAmount;
        return this;
    }

    public void setLoanAmount(Integer loanAmount) {
        this.loanAmount = loanAmount;
    }

    public String getEmployerName() {
        return employerName;
    }

    public Application employerName(String employerName) {
        this.employerName = employerName;
        return this;
    }

    public void setEmployerName(String employerName) {
        this.employerName = employerName;
    }

    public String getStatus() {
        return status;
    }

    public Application status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public Application remark(String remark) {
        this.remark = remark;
        return this;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public LocalDate getRemarkDate() {
        return remarkDate;
    }

    public Application remarkDate(LocalDate remarkDate) {
        this.remarkDate = remarkDate;
        return this;
    }

    public void setRemarkDate(LocalDate remarkDate) {
        this.remarkDate = remarkDate;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public Application createDate(LocalDate createDate) {
        this.createDate = createDate;
        return this;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public LocalDate getUpdateDate() {
        return updateDate;
    }

    public Application updateDate(LocalDate updateDate) {
        this.updateDate = updateDate;
        return this;
    }

    public void setUpdateDate(LocalDate updateDate) {
        this.updateDate = updateDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Application createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public Application updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Application application = (Application) o;
        if (application.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), application.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Application{" +
            "id=" + getId() +
            ", applyDate='" + getApplyDate() + "'" +
            ", customerName='" + getCustomerName() + "'" +
            ", userCode='" + getUserCode() + "'" +
            ", packageCode='" + getPackageCode() + "'" +
            ", loanAmount='" + getLoanAmount() + "'" +
            ", employerName='" + getEmployerName() + "'" +
            ", status='" + getStatus() + "'" +
            ", remark='" + getRemark() + "'" +
            ", remarkDate='" + getRemarkDate() + "'" +
            ", createDate='" + getCreateDate() + "'" +
            ", updateDate='" + getUpdateDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            "}";
    }
}
