package com.quix.aspireloan.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A ReferralStatus.
 */
@Entity
@Table(name = "referral_status")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ReferralStatus implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "applicationid")
    private Integer applicationid;

    @Column(name = "referralamount")
    private Integer referralamount;

    @Column(name = "validityfromdate")
    private LocalDate validityfromdate;

    @Column(name = "validitytodate")
    private LocalDate validitytodate;

    @Column(name = "status")
    private String status;

    @Column(name = "lastremark")
    private String lastremark;

    @Column(name = "applydate")
    private LocalDate applydate;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_date")
    private LocalDate createdDate;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_date")
    private LocalDate updatedDate;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getApplicationid() {
        return applicationid;
    }

    public ReferralStatus applicationid(Integer applicationid) {
        this.applicationid = applicationid;
        return this;
    }

    public void setApplicationid(Integer applicationid) {
        this.applicationid = applicationid;
    }

    public Integer getReferralamount() {
        return referralamount;
    }

    public ReferralStatus referralamount(Integer referralamount) {
        this.referralamount = referralamount;
        return this;
    }

    public void setReferralamount(Integer referralamount) {
        this.referralamount = referralamount;
    }

    public LocalDate getValidityfromdate() {
        return validityfromdate;
    }

    public ReferralStatus validityfromdate(LocalDate validityfromdate) {
        this.validityfromdate = validityfromdate;
        return this;
    }

    public void setValidityfromdate(LocalDate validityfromdate) {
        this.validityfromdate = validityfromdate;
    }

    public LocalDate getValiditytodate() {
        return validitytodate;
    }

    public ReferralStatus validitytodate(LocalDate validitytodate) {
        this.validitytodate = validitytodate;
        return this;
    }

    public void setValiditytodate(LocalDate validitytodate) {
        this.validitytodate = validitytodate;
    }

    public String getStatus() {
        return status;
    }

    public ReferralStatus status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLastremark() {
        return lastremark;
    }

    public ReferralStatus lastremark(String lastremark) {
        this.lastremark = lastremark;
        return this;
    }

    public void setLastremark(String lastremark) {
        this.lastremark = lastremark;
    }

    public LocalDate getApplydate() {
        return applydate;
    }

    public ReferralStatus applydate(LocalDate applydate) {
        this.applydate = applydate;
        return this;
    }

    public void setApplydate(LocalDate applydate) {
        this.applydate = applydate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public ReferralStatus createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public ReferralStatus createdDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public ReferralStatus updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public LocalDate getUpdatedDate() {
        return updatedDate;
    }

    public ReferralStatus updatedDate(LocalDate updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public void setUpdatedDate(LocalDate updatedDate) {
        this.updatedDate = updatedDate;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ReferralStatus referralStatus = (ReferralStatus) o;
        if (referralStatus.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), referralStatus.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ReferralStatus{" +
            "id=" + getId() +
            ", applicationid='" + getApplicationid() + "'" +
            ", referralamount='" + getReferralamount() + "'" +
            ", validityfromdate='" + getValidityfromdate() + "'" +
            ", validitytodate='" + getValiditytodate() + "'" +
            ", status='" + getStatus() + "'" +
            ", lastremark='" + getLastremark() + "'" +
            ", applydate='" + getApplydate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            "}";
    }
}
