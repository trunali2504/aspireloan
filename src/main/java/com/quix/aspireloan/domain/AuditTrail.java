package com.quix.aspireloan.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A AuditTrail.
 */
@Entity
@Table(name = "audit_trail")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AuditTrail implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_code")
    private String userCode;

    @Column(name = "log_date")
    private LocalDate logDate;

    @Column(name = "log_time")
    private String logTime;

    @Column(name = "module_name")
    private String moduleName;

    @Column(name = "action")
    private String action;

    @Column(name = "actionstatus")
    private String actionstatus;

    @Column(name = "itemcode")
    private String itemcode;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserCode() {
        return userCode;
    }

    public AuditTrail userCode(String userCode) {
        this.userCode = userCode;
        return this;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public LocalDate getLogDate() {
        return logDate;
    }

    public AuditTrail logDate(LocalDate logDate) {
        this.logDate = logDate;
        return this;
    }

    public void setLogDate(LocalDate logDate) {
        this.logDate = logDate;
    }

    public String getLogTime() {
        return logTime;
    }

    public AuditTrail logTime(String logTime) {
        this.logTime = logTime;
        return this;
    }

    public void setLogTime(String logTime) {
        this.logTime = logTime;
    }

    public String getModuleName() {
        return moduleName;
    }

    public AuditTrail moduleName(String moduleName) {
        this.moduleName = moduleName;
        return this;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getAction() {
        return action;
    }

    public AuditTrail action(String action) {
        this.action = action;
        return this;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getActionstatus() {
        return actionstatus;
    }

    public AuditTrail actionstatus(String actionstatus) {
        this.actionstatus = actionstatus;
        return this;
    }

    public void setActionstatus(String actionstatus) {
        this.actionstatus = actionstatus;
    }

    public String getItemcode() {
        return itemcode;
    }

    public AuditTrail itemcode(String itemcode) {
        this.itemcode = itemcode;
        return this;
    }

    public void setItemcode(String itemcode) {
        this.itemcode = itemcode;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AuditTrail auditTrail = (AuditTrail) o;
        if (auditTrail.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), auditTrail.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AuditTrail{" +
            "id=" + getId() +
            ", userCode='" + getUserCode() + "'" +
            ", logDate='" + getLogDate() + "'" +
            ", logTime='" + getLogTime() + "'" +
            ", moduleName='" + getModuleName() + "'" +
            ", action='" + getAction() + "'" +
            ", actionstatus='" + getActionstatus() + "'" +
            ", itemcode='" + getItemcode() + "'" +
            "}";
    }
}
