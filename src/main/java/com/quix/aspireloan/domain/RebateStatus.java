package com.quix.aspireloan.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A RebateStatus.
 */
@Entity
@Table(name = "rebate_status")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class RebateStatus implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "application_id")
    private Integer applicationId;

    @Column(name = "rebateamount")
    private Integer rebateamount;

    @Column(name = "status")
    private String status;

    @Column(name = "lastremark")
    private String lastremark;

    @Column(name = "applydate")
    private LocalDate applydate;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_date")
    private LocalDate createdDate;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_date")
    private LocalDate updatedDate;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getApplicationId() {
        return applicationId;
    }

    public RebateStatus applicationId(Integer applicationId) {
        this.applicationId = applicationId;
        return this;
    }

    public void setApplicationId(Integer applicationId) {
        this.applicationId = applicationId;
    }

    public Integer getRebateamount() {
        return rebateamount;
    }

    public RebateStatus rebateamount(Integer rebateamount) {
        this.rebateamount = rebateamount;
        return this;
    }

    public void setRebateamount(Integer rebateamount) {
        this.rebateamount = rebateamount;
    }

    public String getStatus() {
        return status;
    }

    public RebateStatus status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLastremark() {
        return lastremark;
    }

    public RebateStatus lastremark(String lastremark) {
        this.lastremark = lastremark;
        return this;
    }

    public void setLastremark(String lastremark) {
        this.lastremark = lastremark;
    }

    public LocalDate getApplydate() {
        return applydate;
    }

    public RebateStatus applydate(LocalDate applydate) {
        this.applydate = applydate;
        return this;
    }

    public void setApplydate(LocalDate applydate) {
        this.applydate = applydate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public RebateStatus createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public RebateStatus createdDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public RebateStatus updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public LocalDate getUpdatedDate() {
        return updatedDate;
    }

    public RebateStatus updatedDate(LocalDate updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public void setUpdatedDate(LocalDate updatedDate) {
        this.updatedDate = updatedDate;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RebateStatus rebateStatus = (RebateStatus) o;
        if (rebateStatus.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), rebateStatus.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RebateStatus{" +
            "id=" + getId() +
            ", applicationId='" + getApplicationId() + "'" +
            ", rebateamount='" + getRebateamount() + "'" +
            ", status='" + getStatus() + "'" +
            ", lastremark='" + getLastremark() + "'" +
            ", applydate='" + getApplydate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            "}";
    }
}
