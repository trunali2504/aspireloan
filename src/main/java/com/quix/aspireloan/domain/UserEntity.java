package com.quix.aspireloan.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A UserEntity.
 */
@Entity
@Table(name = "user_entity")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class UserEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "mobile_no")
    private Integer mobileNo;

    @Column(name = "emailid")
    private String emailid;

    @Column(name = "tokenid")
    private String tokenid;

    @Column(name = "status")
    private Integer status;

    @Column(name = "usertype")
    private String usertype;

    @Column(name = "company")
    private String company;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_date")
    private LocalDate createdDate;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_date")
    private LocalDate updatedDate;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMobileNo() {
        return mobileNo;
    }

    public UserEntity mobileNo(Integer mobileNo) {
        this.mobileNo = mobileNo;
        return this;
    }

    public void setMobileNo(Integer mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getEmailid() {
        return emailid;
    }

    public UserEntity emailid(String emailid) {
        this.emailid = emailid;
        return this;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    public String getTokenid() {
        return tokenid;
    }

    public UserEntity tokenid(String tokenid) {
        this.tokenid = tokenid;
        return this;
    }

    public void setTokenid(String tokenid) {
        this.tokenid = tokenid;
    }

    public Integer getStatus() {
        return status;
    }

    public UserEntity status(Integer status) {
        this.status = status;
        return this;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getUsertype() {
        return usertype;
    }

    public UserEntity usertype(String usertype) {
        this.usertype = usertype;
        return this;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    public String getCompany() {
        return company;
    }

    public UserEntity company(String company) {
        this.company = company;
        return this;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public UserEntity createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public UserEntity createdDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public UserEntity updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public LocalDate getUpdatedDate() {
        return updatedDate;
    }

    public UserEntity updatedDate(LocalDate updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public void setUpdatedDate(LocalDate updatedDate) {
        this.updatedDate = updatedDate;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserEntity userEntity = (UserEntity) o;
        if (userEntity.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userEntity.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserEntity{" +
            "id=" + getId() +
            ", mobileNo='" + getMobileNo() + "'" +
            ", emailid='" + getEmailid() + "'" +
            ", tokenid='" + getTokenid() + "'" +
            ", status='" + getStatus() + "'" +
            ", usertype='" + getUsertype() + "'" +
            ", company='" + getCompany() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            "}";
    }
}
