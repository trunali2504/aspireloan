package com.quix.aspireloan.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * The Employee entity.
 */
@ApiModel(description = "The Employee entity.")
@Entity
@Table(name = "packages")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Packages implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * The firstname attribute.
     */
    @ApiModelProperty(value = "The firstname attribute.")
    @Column(name = "validityfromdate")
    private LocalDate validityfromdate;

    @Column(name = "validitytodate")
    private LocalDate validitytodate;

    @Column(name = "type_of_product")
    private String typeOfProduct;

    @Column(name = "interestrate")
    private Integer interestrate;

    @Column(name = "maxloanamount")
    private Long maxloanamount;

    @Column(name = "minloanamount")
    private Long minloanamount;

    @Column(name = "maxtenure")
    private Integer maxtenure;

    @Column(name = "mintenure")
    private Integer mintenure;

    @Column(name = "totalpayout")
    private String totalpayout;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_date")
    private LocalDate createdDate;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_date")
    private LocalDate updatedDate;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getValidityfromdate() {
        return validityfromdate;
    }

    public Packages validityfromdate(LocalDate validityfromdate) {
        this.validityfromdate = validityfromdate;
        return this;
    }

    public void setValidityfromdate(LocalDate validityfromdate) {
        this.validityfromdate = validityfromdate;
    }

    public LocalDate getValiditytodate() {
        return validitytodate;
    }

    public Packages validitytodate(LocalDate validitytodate) {
        this.validitytodate = validitytodate;
        return this;
    }

    public void setValiditytodate(LocalDate validitytodate) {
        this.validitytodate = validitytodate;
    }

    public String getTypeOfProduct() {
        return typeOfProduct;
    }

    public Packages typeOfProduct(String typeOfProduct) {
        this.typeOfProduct = typeOfProduct;
        return this;
    }

    public void setTypeOfProduct(String typeOfProduct) {
        this.typeOfProduct = typeOfProduct;
    }

    public Integer getInterestrate() {
        return interestrate;
    }

    public Packages interestrate(Integer interestrate) {
        this.interestrate = interestrate;
        return this;
    }

    public void setInterestrate(Integer interestrate) {
        this.interestrate = interestrate;
    }

    public Long getMaxloanamount() {
        return maxloanamount;
    }

    public Packages maxloanamount(Long maxloanamount) {
        this.maxloanamount = maxloanamount;
        return this;
    }

    public void setMaxloanamount(Long maxloanamount) {
        this.maxloanamount = maxloanamount;
    }

    public Long getMinloanamount() {
        return minloanamount;
    }

    public Packages minloanamount(Long minloanamount) {
        this.minloanamount = minloanamount;
        return this;
    }

    public void setMinloanamount(Long minloanamount) {
        this.minloanamount = minloanamount;
    }

    public Integer getMaxtenure() {
        return maxtenure;
    }

    public Packages maxtenure(Integer maxtenure) {
        this.maxtenure = maxtenure;
        return this;
    }

    public void setMaxtenure(Integer maxtenure) {
        this.maxtenure = maxtenure;
    }

    public Integer getMintenure() {
        return mintenure;
    }

    public Packages mintenure(Integer mintenure) {
        this.mintenure = mintenure;
        return this;
    }

    public void setMintenure(Integer mintenure) {
        this.mintenure = mintenure;
    }

    public String getTotalpayout() {
        return totalpayout;
    }

    public Packages totalpayout(String totalpayout) {
        this.totalpayout = totalpayout;
        return this;
    }

    public void setTotalpayout(String totalpayout) {
        this.totalpayout = totalpayout;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Packages createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public Packages createdDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public Packages updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public LocalDate getUpdatedDate() {
        return updatedDate;
    }

    public Packages updatedDate(LocalDate updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public void setUpdatedDate(LocalDate updatedDate) {
        this.updatedDate = updatedDate;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Packages packages = (Packages) o;
        if (packages.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), packages.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Packages{" +
            "id=" + getId() +
            ", validityfromdate='" + getValidityfromdate() + "'" +
            ", validitytodate='" + getValiditytodate() + "'" +
            ", typeOfProduct='" + getTypeOfProduct() + "'" +
            ", interestrate='" + getInterestrate() + "'" +
            ", maxloanamount='" + getMaxloanamount() + "'" +
            ", minloanamount='" + getMinloanamount() + "'" +
            ", maxtenure='" + getMaxtenure() + "'" +
            ", mintenure='" + getMintenure() + "'" +
            ", totalpayout='" + getTotalpayout() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            "}";
    }
}
