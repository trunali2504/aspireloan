package com.quix.aspireloan.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.quix.aspireloan.domain.Rebate;

import com.quix.aspireloan.repository.RebateRepository;
import com.quix.aspireloan.web.rest.errors.BadRequestAlertException;
import com.quix.aspireloan.web.rest.util.HeaderUtil;
import com.quix.aspireloan.service.dto.RebateDTO;
import com.quix.aspireloan.service.mapper.RebateMapper;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Rebate.
 */
@RestController
@RequestMapping("/api")
public class RebateResource {

    private final Logger log = LoggerFactory.getLogger(RebateResource.class);

    private static final String ENTITY_NAME = "rebate";

    private final RebateRepository rebateRepository;

    private final RebateMapper rebateMapper;

    public RebateResource(RebateRepository rebateRepository, RebateMapper rebateMapper) {
        this.rebateRepository = rebateRepository;
        this.rebateMapper = rebateMapper;
    }

    /**
     * POST  /rebates : Create a new rebate.
     *
     * @param rebateDTO the rebateDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new rebateDTO, or with status 400 (Bad Request) if the rebate has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/rebates")
    @Timed
    public ResponseEntity<RebateDTO> createRebate(@RequestBody RebateDTO rebateDTO) throws URISyntaxException {
        log.debug("REST request to save Rebate : {}", rebateDTO);
        if (rebateDTO.getId() != null) {
            throw new BadRequestAlertException("A new rebate cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Rebate rebate = rebateMapper.toEntity(rebateDTO);
        rebate = rebateRepository.save(rebate);
        RebateDTO result = rebateMapper.toDto(rebate);
        return ResponseEntity.created(new URI("/api/rebates/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /rebates : Updates an existing rebate.
     *
     * @param rebateDTO the rebateDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated rebateDTO,
     * or with status 400 (Bad Request) if the rebateDTO is not valid,
     * or with status 500 (Internal Server Error) if the rebateDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/rebates")
    @Timed
    public ResponseEntity<RebateDTO> updateRebate(@RequestBody RebateDTO rebateDTO) throws URISyntaxException {
        log.debug("REST request to update Rebate : {}", rebateDTO);
        if (rebateDTO.getId() == null) {
            return createRebate(rebateDTO);
        }
        Rebate rebate = rebateMapper.toEntity(rebateDTO);
        rebate = rebateRepository.save(rebate);
        RebateDTO result = rebateMapper.toDto(rebate);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, rebateDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /rebates : get all the rebates.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of rebates in body
     */
    @GetMapping("/rebates")
    @Timed
    public List<RebateDTO> getAllRebates() {
        log.debug("REST request to get all Rebates");
        List<Rebate> rebates = rebateRepository.findAll();
        return rebateMapper.toDto(rebates);
        }

    /**
     * GET  /rebates/:id : get the "id" rebate.
     *
     * @param id the id of the rebateDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the rebateDTO, or with status 404 (Not Found)
     */
    @GetMapping("/rebates/{id}")
    @Timed
    public ResponseEntity<RebateDTO> getRebate(@PathVariable Long id) {
        log.debug("REST request to get Rebate : {}", id);
        Rebate rebate = rebateRepository.findOne(id);
        RebateDTO rebateDTO = rebateMapper.toDto(rebate);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(rebateDTO));
    }

    /**
     * DELETE  /rebates/:id : delete the "id" rebate.
     *
     * @param id the id of the rebateDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/rebates/{id}")
    @Timed
    public ResponseEntity<Void> deleteRebate(@PathVariable Long id) {
        log.debug("REST request to delete Rebate : {}", id);
        rebateRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
