/**
 * View Models used by Spring MVC REST controllers.
 */
package com.quix.aspireloan.web.rest.vm;
