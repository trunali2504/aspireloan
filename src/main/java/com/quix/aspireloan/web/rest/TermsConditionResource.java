package com.quix.aspireloan.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.quix.aspireloan.domain.TermsCondition;

import com.quix.aspireloan.repository.TermsConditionRepository;
import com.quix.aspireloan.web.rest.errors.BadRequestAlertException;
import com.quix.aspireloan.web.rest.util.HeaderUtil;
import com.quix.aspireloan.service.dto.TermsConditionDTO;
import com.quix.aspireloan.service.mapper.TermsConditionMapper;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TermsCondition.
 */
@RestController
@RequestMapping("/api")
public class TermsConditionResource {

    private final Logger log = LoggerFactory.getLogger(TermsConditionResource.class);

    private static final String ENTITY_NAME = "termsCondition";

    private final TermsConditionRepository termsConditionRepository;

    private final TermsConditionMapper termsConditionMapper;

    public TermsConditionResource(TermsConditionRepository termsConditionRepository, TermsConditionMapper termsConditionMapper) {
        this.termsConditionRepository = termsConditionRepository;
        this.termsConditionMapper = termsConditionMapper;
    }

    /**
     * POST  /terms-conditions : Create a new termsCondition.
     *
     * @param termsConditionDTO the termsConditionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new termsConditionDTO, or with status 400 (Bad Request) if the termsCondition has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/terms-conditions")
    @Timed
    public ResponseEntity<TermsConditionDTO> createTermsCondition(@RequestBody TermsConditionDTO termsConditionDTO) throws URISyntaxException {
        log.debug("REST request to save TermsCondition : {}", termsConditionDTO);
        if (termsConditionDTO.getId() != null) {
            throw new BadRequestAlertException("A new termsCondition cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TermsCondition termsCondition = termsConditionMapper.toEntity(termsConditionDTO);
        termsCondition = termsConditionRepository.save(termsCondition);
        TermsConditionDTO result = termsConditionMapper.toDto(termsCondition);
        return ResponseEntity.created(new URI("/api/terms-conditions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /terms-conditions : Updates an existing termsCondition.
     *
     * @param termsConditionDTO the termsConditionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated termsConditionDTO,
     * or with status 400 (Bad Request) if the termsConditionDTO is not valid,
     * or with status 500 (Internal Server Error) if the termsConditionDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/terms-conditions")
    @Timed
    public ResponseEntity<TermsConditionDTO> updateTermsCondition(@RequestBody TermsConditionDTO termsConditionDTO) throws URISyntaxException {
        log.debug("REST request to update TermsCondition : {}", termsConditionDTO);
        if (termsConditionDTO.getId() == null) {
            return createTermsCondition(termsConditionDTO);
        }
        TermsCondition termsCondition = termsConditionMapper.toEntity(termsConditionDTO);
        termsCondition = termsConditionRepository.save(termsCondition);
        TermsConditionDTO result = termsConditionMapper.toDto(termsCondition);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, termsConditionDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /terms-conditions : get all the termsConditions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of termsConditions in body
     */
    @GetMapping("/terms-conditions")
    @Timed
    public List<TermsConditionDTO> getAllTermsConditions() {
        log.debug("REST request to get all TermsConditions");
        List<TermsCondition> termsConditions = termsConditionRepository.findAll();
        return termsConditionMapper.toDto(termsConditions);
        }

    /**
     * GET  /terms-conditions/:id : get the "id" termsCondition.
     *
     * @param id the id of the termsConditionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the termsConditionDTO, or with status 404 (Not Found)
     */
    @GetMapping("/terms-conditions/{id}")
    @Timed
    public ResponseEntity<TermsConditionDTO> getTermsCondition(@PathVariable Long id) {
        log.debug("REST request to get TermsCondition : {}", id);
        TermsCondition termsCondition = termsConditionRepository.findOne(id);
        TermsConditionDTO termsConditionDTO = termsConditionMapper.toDto(termsCondition);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(termsConditionDTO));
    }

    /**
     * DELETE  /terms-conditions/:id : delete the "id" termsCondition.
     *
     * @param id the id of the termsConditionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/terms-conditions/{id}")
    @Timed
    public ResponseEntity<Void> deleteTermsCondition(@PathVariable Long id) {
        log.debug("REST request to delete TermsCondition : {}", id);
        termsConditionRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
