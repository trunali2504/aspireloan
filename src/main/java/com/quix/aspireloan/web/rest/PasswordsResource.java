package com.quix.aspireloan.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.quix.aspireloan.domain.Passwords;

import com.quix.aspireloan.repository.PasswordsRepository;
import com.quix.aspireloan.web.rest.errors.BadRequestAlertException;
import com.quix.aspireloan.web.rest.util.HeaderUtil;
import com.quix.aspireloan.service.dto.PasswordsDTO;
import com.quix.aspireloan.service.mapper.PasswordsMapper;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Passwords.
 */
@RestController
@RequestMapping("/api")
public class PasswordsResource {

    private final Logger log = LoggerFactory.getLogger(PasswordsResource.class);

    private static final String ENTITY_NAME = "passwords";

    private final PasswordsRepository passwordsRepository;

    private final PasswordsMapper passwordsMapper;

    public PasswordsResource(PasswordsRepository passwordsRepository, PasswordsMapper passwordsMapper) {
        this.passwordsRepository = passwordsRepository;
        this.passwordsMapper = passwordsMapper;
    }

    /**
     * POST  /passwords : Create a new passwords.
     *
     * @param passwordsDTO the passwordsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new passwordsDTO, or with status 400 (Bad Request) if the passwords has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/passwords")
    @Timed
    public ResponseEntity<PasswordsDTO> createPasswords(@RequestBody PasswordsDTO passwordsDTO) throws URISyntaxException {
        log.debug("REST request to save Passwords : {}", passwordsDTO);
        if (passwordsDTO.getId() != null) {
            throw new BadRequestAlertException("A new passwords cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Passwords passwords = passwordsMapper.toEntity(passwordsDTO);
        passwords = passwordsRepository.save(passwords);
        PasswordsDTO result = passwordsMapper.toDto(passwords);
        return ResponseEntity.created(new URI("/api/passwords/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /passwords : Updates an existing passwords.
     *
     * @param passwordsDTO the passwordsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated passwordsDTO,
     * or with status 400 (Bad Request) if the passwordsDTO is not valid,
     * or with status 500 (Internal Server Error) if the passwordsDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/passwords")
    @Timed
    public ResponseEntity<PasswordsDTO> updatePasswords(@RequestBody PasswordsDTO passwordsDTO) throws URISyntaxException {
        log.debug("REST request to update Passwords : {}", passwordsDTO);
        if (passwordsDTO.getId() == null) {
            return createPasswords(passwordsDTO);
        }
        Passwords passwords = passwordsMapper.toEntity(passwordsDTO);
        passwords = passwordsRepository.save(passwords);
        PasswordsDTO result = passwordsMapper.toDto(passwords);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, passwordsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /passwords : get all the passwords.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of passwords in body
     */
    @GetMapping("/passwords")
    @Timed
    public List<PasswordsDTO> getAllPasswords() {
        log.debug("REST request to get all Passwords");
        List<Passwords> passwords = passwordsRepository.findAll();
        return passwordsMapper.toDto(passwords);
        }

    /**
     * GET  /passwords/:id : get the "id" passwords.
     *
     * @param id the id of the passwordsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the passwordsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/passwords/{id}")
    @Timed
    public ResponseEntity<PasswordsDTO> getPasswords(@PathVariable Long id) {
        log.debug("REST request to get Passwords : {}", id);
        Passwords passwords = passwordsRepository.findOne(id);
        PasswordsDTO passwordsDTO = passwordsMapper.toDto(passwords);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(passwordsDTO));
    }

    /**
     * DELETE  /passwords/:id : delete the "id" passwords.
     *
     * @param id the id of the passwordsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/passwords/{id}")
    @Timed
    public ResponseEntity<Void> deletePasswords(@PathVariable Long id) {
        log.debug("REST request to delete Passwords : {}", id);
        passwordsRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
