package com.quix.aspireloan.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.quix.aspireloan.domain.UserEntity;

import com.quix.aspireloan.repository.UserEntityRepository;
import com.quix.aspireloan.web.rest.errors.BadRequestAlertException;
import com.quix.aspireloan.web.rest.util.HeaderUtil;
import com.quix.aspireloan.service.dto.UserEntityDTO;
import com.quix.aspireloan.service.mapper.UserEntityMapper;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing UserEntity.
 */
@RestController
@RequestMapping("/api")
public class UserEntityResource {

    private final Logger log = LoggerFactory.getLogger(UserEntityResource.class);

    private static final String ENTITY_NAME = "userEntity";

    private final UserEntityRepository userEntityRepository;

    private final UserEntityMapper userEntityMapper;

    public UserEntityResource(UserEntityRepository userEntityRepository, UserEntityMapper userEntityMapper) {
        this.userEntityRepository = userEntityRepository;
        this.userEntityMapper = userEntityMapper;
    }

    /**
     * POST  /user-entities : Create a new userEntity.
     *
     * @param userEntityDTO the userEntityDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new userEntityDTO, or with status 400 (Bad Request) if the userEntity has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/user-entities")
    @Timed
    public ResponseEntity<UserEntityDTO> createUserEntity(@RequestBody UserEntityDTO userEntityDTO) throws URISyntaxException {
        log.debug("REST request to save UserEntity : {}", userEntityDTO);
        if (userEntityDTO.getId() != null) {
            throw new BadRequestAlertException("A new userEntity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserEntity userEntity = userEntityMapper.toEntity(userEntityDTO);
        userEntity = userEntityRepository.save(userEntity);
        UserEntityDTO result = userEntityMapper.toDto(userEntity);
        return ResponseEntity.created(new URI("/api/user-entities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /user-entities : Updates an existing userEntity.
     *
     * @param userEntityDTO the userEntityDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated userEntityDTO,
     * or with status 400 (Bad Request) if the userEntityDTO is not valid,
     * or with status 500 (Internal Server Error) if the userEntityDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/user-entities")
    @Timed
    public ResponseEntity<UserEntityDTO> updateUserEntity(@RequestBody UserEntityDTO userEntityDTO) throws URISyntaxException {
        log.debug("REST request to update UserEntity : {}", userEntityDTO);
        if (userEntityDTO.getId() == null) {
            return createUserEntity(userEntityDTO);
        }
        UserEntity userEntity = userEntityMapper.toEntity(userEntityDTO);
        userEntity = userEntityRepository.save(userEntity);
        UserEntityDTO result = userEntityMapper.toDto(userEntity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, userEntityDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /user-entities : get all the userEntities.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of userEntities in body
     */
    @GetMapping("/user-entities")
    @Timed
    public List<UserEntityDTO> getAllUserEntities() {
        log.debug("REST request to get all UserEntities");
        List<UserEntity> userEntities = userEntityRepository.findAll();
        return userEntityMapper.toDto(userEntities);
        }

    /**
     * GET  /user-entities/:id : get the "id" userEntity.
     *
     * @param id the id of the userEntityDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the userEntityDTO, or with status 404 (Not Found)
     */
    @GetMapping("/user-entities/{id}")
    @Timed
    public ResponseEntity<UserEntityDTO> getUserEntity(@PathVariable Long id) {
        log.debug("REST request to get UserEntity : {}", id);
        UserEntity userEntity = userEntityRepository.findOne(id);
        UserEntityDTO userEntityDTO = userEntityMapper.toDto(userEntity);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(userEntityDTO));
    }

    /**
     * DELETE  /user-entities/:id : delete the "id" userEntity.
     *
     * @param id the id of the userEntityDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/user-entities/{id}")
    @Timed
    public ResponseEntity<Void> deleteUserEntity(@PathVariable Long id) {
        log.debug("REST request to delete UserEntity : {}", id);
        userEntityRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
