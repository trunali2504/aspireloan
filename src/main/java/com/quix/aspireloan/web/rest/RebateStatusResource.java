package com.quix.aspireloan.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.quix.aspireloan.domain.RebateStatus;

import com.quix.aspireloan.repository.RebateStatusRepository;
import com.quix.aspireloan.web.rest.errors.BadRequestAlertException;
import com.quix.aspireloan.web.rest.util.HeaderUtil;
import com.quix.aspireloan.service.dto.RebateStatusDTO;
import com.quix.aspireloan.service.mapper.RebateStatusMapper;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing RebateStatus.
 */
@RestController
@RequestMapping("/api")
public class RebateStatusResource {

    private final Logger log = LoggerFactory.getLogger(RebateStatusResource.class);

    private static final String ENTITY_NAME = "rebateStatus";

    private final RebateStatusRepository rebateStatusRepository;

    private final RebateStatusMapper rebateStatusMapper;

    public RebateStatusResource(RebateStatusRepository rebateStatusRepository, RebateStatusMapper rebateStatusMapper) {
        this.rebateStatusRepository = rebateStatusRepository;
        this.rebateStatusMapper = rebateStatusMapper;
    }

    /**
     * POST  /rebate-statuses : Create a new rebateStatus.
     *
     * @param rebateStatusDTO the rebateStatusDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new rebateStatusDTO, or with status 400 (Bad Request) if the rebateStatus has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/rebate-statuses")
    @Timed
    public ResponseEntity<RebateStatusDTO> createRebateStatus(@RequestBody RebateStatusDTO rebateStatusDTO) throws URISyntaxException {
        log.debug("REST request to save RebateStatus : {}", rebateStatusDTO);
        if (rebateStatusDTO.getId() != null) {
            throw new BadRequestAlertException("A new rebateStatus cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RebateStatus rebateStatus = rebateStatusMapper.toEntity(rebateStatusDTO);
        rebateStatus = rebateStatusRepository.save(rebateStatus);
        RebateStatusDTO result = rebateStatusMapper.toDto(rebateStatus);
        return ResponseEntity.created(new URI("/api/rebate-statuses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /rebate-statuses : Updates an existing rebateStatus.
     *
     * @param rebateStatusDTO the rebateStatusDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated rebateStatusDTO,
     * or with status 400 (Bad Request) if the rebateStatusDTO is not valid,
     * or with status 500 (Internal Server Error) if the rebateStatusDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/rebate-statuses")
    @Timed
    public ResponseEntity<RebateStatusDTO> updateRebateStatus(@RequestBody RebateStatusDTO rebateStatusDTO) throws URISyntaxException {
        log.debug("REST request to update RebateStatus : {}", rebateStatusDTO);
        if (rebateStatusDTO.getId() == null) {
            return createRebateStatus(rebateStatusDTO);
        }
        RebateStatus rebateStatus = rebateStatusMapper.toEntity(rebateStatusDTO);
        rebateStatus = rebateStatusRepository.save(rebateStatus);
        RebateStatusDTO result = rebateStatusMapper.toDto(rebateStatus);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, rebateStatusDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /rebate-statuses : get all the rebateStatuses.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of rebateStatuses in body
     */
    @GetMapping("/rebate-statuses")
    @Timed
    public List<RebateStatusDTO> getAllRebateStatuses() {
        log.debug("REST request to get all RebateStatuses");
        List<RebateStatus> rebateStatuses = rebateStatusRepository.findAll();
        return rebateStatusMapper.toDto(rebateStatuses);
        }

    /**
     * GET  /rebate-statuses/:id : get the "id" rebateStatus.
     *
     * @param id the id of the rebateStatusDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the rebateStatusDTO, or with status 404 (Not Found)
     */
    @GetMapping("/rebate-statuses/{id}")
    @Timed
    public ResponseEntity<RebateStatusDTO> getRebateStatus(@PathVariable Long id) {
        log.debug("REST request to get RebateStatus : {}", id);
        RebateStatus rebateStatus = rebateStatusRepository.findOne(id);
        RebateStatusDTO rebateStatusDTO = rebateStatusMapper.toDto(rebateStatus);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(rebateStatusDTO));
    }

    /**
     * DELETE  /rebate-statuses/:id : delete the "id" rebateStatus.
     *
     * @param id the id of the rebateStatusDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/rebate-statuses/{id}")
    @Timed
    public ResponseEntity<Void> deleteRebateStatus(@PathVariable Long id) {
        log.debug("REST request to delete RebateStatus : {}", id);
        rebateStatusRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
