package com.quix.aspireloan.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.quix.aspireloan.domain.AuditTrail;

import com.quix.aspireloan.repository.AuditTrailRepository;
import com.quix.aspireloan.web.rest.errors.BadRequestAlertException;
import com.quix.aspireloan.web.rest.util.HeaderUtil;
import com.quix.aspireloan.service.dto.AuditTrailDTO;
import com.quix.aspireloan.service.mapper.AuditTrailMapper;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing AuditTrail.
 */
@RestController
@RequestMapping("/api")
public class AuditTrailResource {

    private final Logger log = LoggerFactory.getLogger(AuditTrailResource.class);

    private static final String ENTITY_NAME = "auditTrail";

    private final AuditTrailRepository auditTrailRepository;

    private final AuditTrailMapper auditTrailMapper;

    public AuditTrailResource(AuditTrailRepository auditTrailRepository, AuditTrailMapper auditTrailMapper) {
        this.auditTrailRepository = auditTrailRepository;
        this.auditTrailMapper = auditTrailMapper;
    }

    /**
     * POST  /audit-trails : Create a new auditTrail.
     *
     * @param auditTrailDTO the auditTrailDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new auditTrailDTO, or with status 400 (Bad Request) if the auditTrail has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/audit-trails")
    @Timed
    public ResponseEntity<AuditTrailDTO> createAuditTrail(@RequestBody AuditTrailDTO auditTrailDTO) throws URISyntaxException {
        log.debug("REST request to save AuditTrail : {}", auditTrailDTO);
        if (auditTrailDTO.getId() != null) {
            throw new BadRequestAlertException("A new auditTrail cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AuditTrail auditTrail = auditTrailMapper.toEntity(auditTrailDTO);
        auditTrail = auditTrailRepository.save(auditTrail);
        AuditTrailDTO result = auditTrailMapper.toDto(auditTrail);
        return ResponseEntity.created(new URI("/api/audit-trails/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /audit-trails : Updates an existing auditTrail.
     *
     * @param auditTrailDTO the auditTrailDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated auditTrailDTO,
     * or with status 400 (Bad Request) if the auditTrailDTO is not valid,
     * or with status 500 (Internal Server Error) if the auditTrailDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/audit-trails")
    @Timed
    public ResponseEntity<AuditTrailDTO> updateAuditTrail(@RequestBody AuditTrailDTO auditTrailDTO) throws URISyntaxException {
        log.debug("REST request to update AuditTrail : {}", auditTrailDTO);
        if (auditTrailDTO.getId() == null) {
            return createAuditTrail(auditTrailDTO);
        }
        AuditTrail auditTrail = auditTrailMapper.toEntity(auditTrailDTO);
        auditTrail = auditTrailRepository.save(auditTrail);
        AuditTrailDTO result = auditTrailMapper.toDto(auditTrail);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, auditTrailDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /audit-trails : get all the auditTrails.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of auditTrails in body
     */
    @GetMapping("/audit-trails")
    @Timed
    public List<AuditTrailDTO> getAllAuditTrails() {
        log.debug("REST request to get all AuditTrails");
        List<AuditTrail> auditTrails = auditTrailRepository.findAll();
        return auditTrailMapper.toDto(auditTrails);
        }

    /**
     * GET  /audit-trails/:id : get the "id" auditTrail.
     *
     * @param id the id of the auditTrailDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the auditTrailDTO, or with status 404 (Not Found)
     */
    @GetMapping("/audit-trails/{id}")
    @Timed
    public ResponseEntity<AuditTrailDTO> getAuditTrail(@PathVariable Long id) {
        log.debug("REST request to get AuditTrail : {}", id);
        AuditTrail auditTrail = auditTrailRepository.findOne(id);
        AuditTrailDTO auditTrailDTO = auditTrailMapper.toDto(auditTrail);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(auditTrailDTO));
    }

    /**
     * DELETE  /audit-trails/:id : delete the "id" auditTrail.
     *
     * @param id the id of the auditTrailDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/audit-trails/{id}")
    @Timed
    public ResponseEntity<Void> deleteAuditTrail(@PathVariable Long id) {
        log.debug("REST request to delete AuditTrail : {}", id);
        auditTrailRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
