package com.quix.aspireloan.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.quix.aspireloan.domain.Referral;

import com.quix.aspireloan.repository.ReferralRepository;
import com.quix.aspireloan.web.rest.errors.BadRequestAlertException;
import com.quix.aspireloan.web.rest.util.HeaderUtil;
import com.quix.aspireloan.service.dto.ReferralDTO;
import com.quix.aspireloan.service.mapper.ReferralMapper;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Referral.
 */
@RestController
@RequestMapping("/api")
public class ReferralResource {

    private final Logger log = LoggerFactory.getLogger(ReferralResource.class);

    private static final String ENTITY_NAME = "referral";

    private final ReferralRepository referralRepository;

    private final ReferralMapper referralMapper;

    public ReferralResource(ReferralRepository referralRepository, ReferralMapper referralMapper) {
        this.referralRepository = referralRepository;
        this.referralMapper = referralMapper;
    }

    /**
     * POST  /referrals : Create a new referral.
     *
     * @param referralDTO the referralDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new referralDTO, or with status 400 (Bad Request) if the referral has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/referrals")
    @Timed
    public ResponseEntity<ReferralDTO> createReferral(@RequestBody ReferralDTO referralDTO) throws URISyntaxException {
        log.debug("REST request to save Referral : {}", referralDTO);
        if (referralDTO.getId() != null) {
            throw new BadRequestAlertException("A new referral cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Referral referral = referralMapper.toEntity(referralDTO);
        referral = referralRepository.save(referral);
        ReferralDTO result = referralMapper.toDto(referral);
        return ResponseEntity.created(new URI("/api/referrals/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /referrals : Updates an existing referral.
     *
     * @param referralDTO the referralDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated referralDTO,
     * or with status 400 (Bad Request) if the referralDTO is not valid,
     * or with status 500 (Internal Server Error) if the referralDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/referrals")
    @Timed
    public ResponseEntity<ReferralDTO> updateReferral(@RequestBody ReferralDTO referralDTO) throws URISyntaxException {
        log.debug("REST request to update Referral : {}", referralDTO);
        if (referralDTO.getId() == null) {
            return createReferral(referralDTO);
        }
        Referral referral = referralMapper.toEntity(referralDTO);
        referral = referralRepository.save(referral);
        ReferralDTO result = referralMapper.toDto(referral);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, referralDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /referrals : get all the referrals.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of referrals in body
     */
    @GetMapping("/referrals")
    @Timed
    public List<ReferralDTO> getAllReferrals() {
        log.debug("REST request to get all Referrals");
        List<Referral> referrals = referralRepository.findAll();
        return referralMapper.toDto(referrals);
        }

    /**
     * GET  /referrals/:id : get the "id" referral.
     *
     * @param id the id of the referralDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the referralDTO, or with status 404 (Not Found)
     */
    @GetMapping("/referrals/{id}")
    @Timed
    public ResponseEntity<ReferralDTO> getReferral(@PathVariable Long id) {
        log.debug("REST request to get Referral : {}", id);
        Referral referral = referralRepository.findOne(id);
        ReferralDTO referralDTO = referralMapper.toDto(referral);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(referralDTO));
    }

    /**
     * DELETE  /referrals/:id : delete the "id" referral.
     *
     * @param id the id of the referralDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/referrals/{id}")
    @Timed
    public ResponseEntity<Void> deleteReferral(@PathVariable Long id) {
        log.debug("REST request to delete Referral : {}", id);
        referralRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
