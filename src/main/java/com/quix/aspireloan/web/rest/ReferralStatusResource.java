package com.quix.aspireloan.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.quix.aspireloan.domain.ReferralStatus;

import com.quix.aspireloan.repository.ReferralStatusRepository;
import com.quix.aspireloan.web.rest.errors.BadRequestAlertException;
import com.quix.aspireloan.web.rest.util.HeaderUtil;
import com.quix.aspireloan.service.dto.ReferralStatusDTO;
import com.quix.aspireloan.service.mapper.ReferralStatusMapper;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ReferralStatus.
 */
@RestController
@RequestMapping("/api")
public class ReferralStatusResource {

    private final Logger log = LoggerFactory.getLogger(ReferralStatusResource.class);

    private static final String ENTITY_NAME = "referralStatus";

    private final ReferralStatusRepository referralStatusRepository;

    private final ReferralStatusMapper referralStatusMapper;

    public ReferralStatusResource(ReferralStatusRepository referralStatusRepository, ReferralStatusMapper referralStatusMapper) {
        this.referralStatusRepository = referralStatusRepository;
        this.referralStatusMapper = referralStatusMapper;
    }

    /**
     * POST  /referral-statuses : Create a new referralStatus.
     *
     * @param referralStatusDTO the referralStatusDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new referralStatusDTO, or with status 400 (Bad Request) if the referralStatus has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/referral-statuses")
    @Timed
    public ResponseEntity<ReferralStatusDTO> createReferralStatus(@RequestBody ReferralStatusDTO referralStatusDTO) throws URISyntaxException {
        log.debug("REST request to save ReferralStatus : {}", referralStatusDTO);
        if (referralStatusDTO.getId() != null) {
            throw new BadRequestAlertException("A new referralStatus cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ReferralStatus referralStatus = referralStatusMapper.toEntity(referralStatusDTO);
        referralStatus = referralStatusRepository.save(referralStatus);
        ReferralStatusDTO result = referralStatusMapper.toDto(referralStatus);
        return ResponseEntity.created(new URI("/api/referral-statuses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /referral-statuses : Updates an existing referralStatus.
     *
     * @param referralStatusDTO the referralStatusDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated referralStatusDTO,
     * or with status 400 (Bad Request) if the referralStatusDTO is not valid,
     * or with status 500 (Internal Server Error) if the referralStatusDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/referral-statuses")
    @Timed
    public ResponseEntity<ReferralStatusDTO> updateReferralStatus(@RequestBody ReferralStatusDTO referralStatusDTO) throws URISyntaxException {
        log.debug("REST request to update ReferralStatus : {}", referralStatusDTO);
        if (referralStatusDTO.getId() == null) {
            return createReferralStatus(referralStatusDTO);
        }
        ReferralStatus referralStatus = referralStatusMapper.toEntity(referralStatusDTO);
        referralStatus = referralStatusRepository.save(referralStatus);
        ReferralStatusDTO result = referralStatusMapper.toDto(referralStatus);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, referralStatusDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /referral-statuses : get all the referralStatuses.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of referralStatuses in body
     */
    @GetMapping("/referral-statuses")
    @Timed
    public List<ReferralStatusDTO> getAllReferralStatuses() {
        log.debug("REST request to get all ReferralStatuses");
        List<ReferralStatus> referralStatuses = referralStatusRepository.findAll();
        return referralStatusMapper.toDto(referralStatuses);
        }

    /**
     * GET  /referral-statuses/:id : get the "id" referralStatus.
     *
     * @param id the id of the referralStatusDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the referralStatusDTO, or with status 404 (Not Found)
     */
    @GetMapping("/referral-statuses/{id}")
    @Timed
    public ResponseEntity<ReferralStatusDTO> getReferralStatus(@PathVariable Long id) {
        log.debug("REST request to get ReferralStatus : {}", id);
        ReferralStatus referralStatus = referralStatusRepository.findOne(id);
        ReferralStatusDTO referralStatusDTO = referralStatusMapper.toDto(referralStatus);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(referralStatusDTO));
    }

    /**
     * DELETE  /referral-statuses/:id : delete the "id" referralStatus.
     *
     * @param id the id of the referralStatusDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/referral-statuses/{id}")
    @Timed
    public ResponseEntity<Void> deleteReferralStatus(@PathVariable Long id) {
        log.debug("REST request to delete ReferralStatus : {}", id);
        referralStatusRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
