package com.quix.aspireloan.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.quix.aspireloan.domain.Packages;

import com.quix.aspireloan.repository.PackagesRepository;
import com.quix.aspireloan.web.rest.errors.BadRequestAlertException;
import com.quix.aspireloan.web.rest.util.HeaderUtil;
import com.quix.aspireloan.service.dto.PackagesDTO;
import com.quix.aspireloan.service.mapper.PackagesMapper;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Packages.
 */
@RestController
@RequestMapping("/api")
public class PackagesResource {

    private final Logger log = LoggerFactory.getLogger(PackagesResource.class);

    private static final String ENTITY_NAME = "packages";

    private final PackagesRepository packagesRepository;

    private final PackagesMapper packagesMapper;

    public PackagesResource(PackagesRepository packagesRepository, PackagesMapper packagesMapper) {
        this.packagesRepository = packagesRepository;
        this.packagesMapper = packagesMapper;
    }

    /**
     * POST  /packages : Create a new packages.
     *
     * @param packagesDTO the packagesDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new packagesDTO, or with status 400 (Bad Request) if the packages has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/packages")
    @Timed
    public ResponseEntity<PackagesDTO> createPackages(@RequestBody PackagesDTO packagesDTO) throws URISyntaxException {
        log.debug("REST request to save Packages : {}", packagesDTO);
        if (packagesDTO.getId() != null) {
            throw new BadRequestAlertException("A new packages cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Packages packages = packagesMapper.toEntity(packagesDTO);
        packages = packagesRepository.save(packages);
        PackagesDTO result = packagesMapper.toDto(packages);
        return ResponseEntity.created(new URI("/api/packages/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /packages : Updates an existing packages.
     *
     * @param packagesDTO the packagesDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated packagesDTO,
     * or with status 400 (Bad Request) if the packagesDTO is not valid,
     * or with status 500 (Internal Server Error) if the packagesDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/packages")
    @Timed
    public ResponseEntity<PackagesDTO> updatePackages(@RequestBody PackagesDTO packagesDTO) throws URISyntaxException {
        log.debug("REST request to update Packages : {}", packagesDTO);
        if (packagesDTO.getId() == null) {
            return createPackages(packagesDTO);
        }
        Packages packages = packagesMapper.toEntity(packagesDTO);
        packages = packagesRepository.save(packages);
        PackagesDTO result = packagesMapper.toDto(packages);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, packagesDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /packages : get all the packages.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of packages in body
     */
    @GetMapping("/packages")
    @Timed
    public List<PackagesDTO> getAllPackages() {
        log.debug("REST request to get all Packages");
        List<Packages> packages = packagesRepository.findAll();
        return packagesMapper.toDto(packages);
        }

    /**
     * GET  /packages/:id : get the "id" packages.
     *
     * @param id the id of the packagesDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the packagesDTO, or with status 404 (Not Found)
     */
    @GetMapping("/packages/{id}")
    @Timed
    public ResponseEntity<PackagesDTO> getPackages(@PathVariable Long id) {
        log.debug("REST request to get Packages : {}", id);
        Packages packages = packagesRepository.findOne(id);
        PackagesDTO packagesDTO = packagesMapper.toDto(packages);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(packagesDTO));
    }

    /**
     * DELETE  /packages/:id : delete the "id" packages.
     *
     * @param id the id of the packagesDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/packages/{id}")
    @Timed
    public ResponseEntity<Void> deletePackages(@PathVariable Long id) {
        log.debug("REST request to delete Packages : {}", id);
        packagesRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
