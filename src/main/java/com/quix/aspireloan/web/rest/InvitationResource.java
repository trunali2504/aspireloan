package com.quix.aspireloan.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.quix.aspireloan.domain.Invitation;

import com.quix.aspireloan.repository.InvitationRepository;
import com.quix.aspireloan.web.rest.errors.BadRequestAlertException;
import com.quix.aspireloan.web.rest.util.HeaderUtil;
import com.quix.aspireloan.service.dto.InvitationDTO;
import com.quix.aspireloan.service.mapper.InvitationMapper;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Invitation.
 */
@RestController
@RequestMapping("/api")
public class InvitationResource {

    private final Logger log = LoggerFactory.getLogger(InvitationResource.class);

    private static final String ENTITY_NAME = "invitation";

    private final InvitationRepository invitationRepository;

    private final InvitationMapper invitationMapper;

    public InvitationResource(InvitationRepository invitationRepository, InvitationMapper invitationMapper) {
        this.invitationRepository = invitationRepository;
        this.invitationMapper = invitationMapper;
    }

    /**
     * POST  /invitations : Create a new invitation.
     *
     * @param invitationDTO the invitationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new invitationDTO, or with status 400 (Bad Request) if the invitation has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/invitations")
    @Timed
    public ResponseEntity<InvitationDTO> createInvitation(@RequestBody InvitationDTO invitationDTO) throws URISyntaxException {
        log.debug("REST request to save Invitation : {}", invitationDTO);
        if (invitationDTO.getId() != null) {
            throw new BadRequestAlertException("A new invitation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Invitation invitation = invitationMapper.toEntity(invitationDTO);
        invitation = invitationRepository.save(invitation);
        InvitationDTO result = invitationMapper.toDto(invitation);
        return ResponseEntity.created(new URI("/api/invitations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /invitations : Updates an existing invitation.
     *
     * @param invitationDTO the invitationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated invitationDTO,
     * or with status 400 (Bad Request) if the invitationDTO is not valid,
     * or with status 500 (Internal Server Error) if the invitationDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/invitations")
    @Timed
    public ResponseEntity<InvitationDTO> updateInvitation(@RequestBody InvitationDTO invitationDTO) throws URISyntaxException {
        log.debug("REST request to update Invitation : {}", invitationDTO);
        if (invitationDTO.getId() == null) {
            return createInvitation(invitationDTO);
        }
        Invitation invitation = invitationMapper.toEntity(invitationDTO);
        invitation = invitationRepository.save(invitation);
        InvitationDTO result = invitationMapper.toDto(invitation);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, invitationDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /invitations : get all the invitations.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of invitations in body
     */
    @GetMapping("/invitations")
    @Timed
    public List<InvitationDTO> getAllInvitations() {
        log.debug("REST request to get all Invitations");
        List<Invitation> invitations = invitationRepository.findAll();
        return invitationMapper.toDto(invitations);
        }

    /**
     * GET  /invitations/:id : get the "id" invitation.
     *
     * @param id the id of the invitationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the invitationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/invitations/{id}")
    @Timed
    public ResponseEntity<InvitationDTO> getInvitation(@PathVariable Long id) {
        log.debug("REST request to get Invitation : {}", id);
        Invitation invitation = invitationRepository.findOne(id);
        InvitationDTO invitationDTO = invitationMapper.toDto(invitation);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(invitationDTO));
    }

    /**
     * DELETE  /invitations/:id : delete the "id" invitation.
     *
     * @param id the id of the invitationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/invitations/{id}")
    @Timed
    public ResponseEntity<Void> deleteInvitation(@PathVariable Long id) {
        log.debug("REST request to delete Invitation : {}", id);
        invitationRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
