package com.quix.aspireloan.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.quix.aspireloan.domain.AccessControl;

import com.quix.aspireloan.repository.AccessControlRepository;
import com.quix.aspireloan.web.rest.errors.BadRequestAlertException;
import com.quix.aspireloan.web.rest.util.HeaderUtil;
import com.quix.aspireloan.service.dto.AccessControlDTO;
import com.quix.aspireloan.service.mapper.AccessControlMapper;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing AccessControl.
 */
@RestController
@RequestMapping("/api")
public class AccessControlResource {

    private final Logger log = LoggerFactory.getLogger(AccessControlResource.class);

    private static final String ENTITY_NAME = "accessControl";

    private final AccessControlRepository accessControlRepository;

    private final AccessControlMapper accessControlMapper;

    public AccessControlResource(AccessControlRepository accessControlRepository, AccessControlMapper accessControlMapper) {
        this.accessControlRepository = accessControlRepository;
        this.accessControlMapper = accessControlMapper;
    }

    /**
     * POST  /access-controls : Create a new accessControl.
     *
     * @param accessControlDTO the accessControlDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new accessControlDTO, or with status 400 (Bad Request) if the accessControl has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/access-controls")
    @Timed
    public ResponseEntity<AccessControlDTO> createAccessControl(@RequestBody AccessControlDTO accessControlDTO) throws URISyntaxException {
        log.debug("REST request to save AccessControl : {}", accessControlDTO);
        if (accessControlDTO.getId() != null) {
            throw new BadRequestAlertException("A new accessControl cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AccessControl accessControl = accessControlMapper.toEntity(accessControlDTO);
        accessControl = accessControlRepository.save(accessControl);
        AccessControlDTO result = accessControlMapper.toDto(accessControl);
        return ResponseEntity.created(new URI("/api/access-controls/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /access-controls : Updates an existing accessControl.
     *
     * @param accessControlDTO the accessControlDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated accessControlDTO,
     * or with status 400 (Bad Request) if the accessControlDTO is not valid,
     * or with status 500 (Internal Server Error) if the accessControlDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/access-controls")
    @Timed
    public ResponseEntity<AccessControlDTO> updateAccessControl(@RequestBody AccessControlDTO accessControlDTO) throws URISyntaxException {
        log.debug("REST request to update AccessControl : {}", accessControlDTO);
        if (accessControlDTO.getId() == null) {
            return createAccessControl(accessControlDTO);
        }
        AccessControl accessControl = accessControlMapper.toEntity(accessControlDTO);
        accessControl = accessControlRepository.save(accessControl);
        AccessControlDTO result = accessControlMapper.toDto(accessControl);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, accessControlDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /access-controls : get all the accessControls.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of accessControls in body
     */
    @GetMapping("/access-controls")
    @Timed
    public List<AccessControlDTO> getAllAccessControls() {
        log.debug("REST request to get all AccessControls");
        List<AccessControl> accessControls = accessControlRepository.findAll();
        return accessControlMapper.toDto(accessControls);
        }

    /**
     * GET  /access-controls/:id : get the "id" accessControl.
     *
     * @param id the id of the accessControlDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the accessControlDTO, or with status 404 (Not Found)
     */
    @GetMapping("/access-controls/{id}")
    @Timed
    public ResponseEntity<AccessControlDTO> getAccessControl(@PathVariable Long id) {
        log.debug("REST request to get AccessControl : {}", id);
        AccessControl accessControl = accessControlRepository.findOne(id);
        AccessControlDTO accessControlDTO = accessControlMapper.toDto(accessControl);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(accessControlDTO));
    }

    /**
     * DELETE  /access-controls/:id : delete the "id" accessControl.
     *
     * @param id the id of the accessControlDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/access-controls/{id}")
    @Timed
    public ResponseEntity<Void> deleteAccessControl(@PathVariable Long id) {
        log.debug("REST request to delete AccessControl : {}", id);
        accessControlRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
