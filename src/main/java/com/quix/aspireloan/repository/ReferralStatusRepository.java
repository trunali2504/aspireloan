package com.quix.aspireloan.repository;

import com.quix.aspireloan.domain.ReferralStatus;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ReferralStatus entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReferralStatusRepository extends JpaRepository<ReferralStatus, Long> {

}
