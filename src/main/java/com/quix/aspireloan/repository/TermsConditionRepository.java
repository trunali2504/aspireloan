package com.quix.aspireloan.repository;

import com.quix.aspireloan.domain.TermsCondition;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the TermsCondition entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TermsConditionRepository extends JpaRepository<TermsCondition, Long> {

}
