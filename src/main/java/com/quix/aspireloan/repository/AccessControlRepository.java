package com.quix.aspireloan.repository;

import com.quix.aspireloan.domain.AccessControl;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the AccessControl entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AccessControlRepository extends JpaRepository<AccessControl, Long> {

}
