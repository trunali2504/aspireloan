package com.quix.aspireloan.repository;

import com.quix.aspireloan.domain.RebateStatus;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the RebateStatus entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RebateStatusRepository extends JpaRepository<RebateStatus, Long> {

}
