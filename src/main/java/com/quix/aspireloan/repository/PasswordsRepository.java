package com.quix.aspireloan.repository;

import com.quix.aspireloan.domain.Passwords;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Passwords entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PasswordsRepository extends JpaRepository<Passwords, Long> {

}
