package com.quix.aspireloan.service.mapper;

import com.quix.aspireloan.domain.*;
import com.quix.aspireloan.service.dto.TermsConditionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity TermsCondition and its DTO TermsConditionDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TermsConditionMapper extends EntityMapper<TermsConditionDTO, TermsCondition> {

    

    

    default TermsCondition fromId(Long id) {
        if (id == null) {
            return null;
        }
        TermsCondition termsCondition = new TermsCondition();
        termsCondition.setId(id);
        return termsCondition;
    }
}
