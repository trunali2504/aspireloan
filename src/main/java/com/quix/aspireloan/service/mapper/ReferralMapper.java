package com.quix.aspireloan.service.mapper;

import com.quix.aspireloan.domain.*;
import com.quix.aspireloan.service.dto.ReferralDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Referral and its DTO ReferralDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ReferralMapper extends EntityMapper<ReferralDTO, Referral> {

    

    

    default Referral fromId(Long id) {
        if (id == null) {
            return null;
        }
        Referral referral = new Referral();
        referral.setId(id);
        return referral;
    }
}
