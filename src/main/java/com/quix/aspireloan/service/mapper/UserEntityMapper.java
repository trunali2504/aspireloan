package com.quix.aspireloan.service.mapper;

import com.quix.aspireloan.domain.*;
import com.quix.aspireloan.service.dto.UserEntityDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity UserEntity and its DTO UserEntityDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface UserEntityMapper extends EntityMapper<UserEntityDTO, UserEntity> {

    

    

    default UserEntity fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserEntity userEntity = new UserEntity();
        userEntity.setId(id);
        return userEntity;
    }
}
