package com.quix.aspireloan.service.mapper;

import com.quix.aspireloan.domain.*;
import com.quix.aspireloan.service.dto.PasswordsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Passwords and its DTO PasswordsDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PasswordsMapper extends EntityMapper<PasswordsDTO, Passwords> {

    

    

    default Passwords fromId(Long id) {
        if (id == null) {
            return null;
        }
        Passwords passwords = new Passwords();
        passwords.setId(id);
        return passwords;
    }
}
