package com.quix.aspireloan.service.mapper;

import com.quix.aspireloan.domain.*;
import com.quix.aspireloan.service.dto.AccessControlDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity AccessControl and its DTO AccessControlDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AccessControlMapper extends EntityMapper<AccessControlDTO, AccessControl> {

    

    

    default AccessControl fromId(Long id) {
        if (id == null) {
            return null;
        }
        AccessControl accessControl = new AccessControl();
        accessControl.setId(id);
        return accessControl;
    }
}
