package com.quix.aspireloan.service.mapper;

import com.quix.aspireloan.domain.*;
import com.quix.aspireloan.service.dto.PackagesDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Packages and its DTO PackagesDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PackagesMapper extends EntityMapper<PackagesDTO, Packages> {

    

    

    default Packages fromId(Long id) {
        if (id == null) {
            return null;
        }
        Packages packages = new Packages();
        packages.setId(id);
        return packages;
    }
}
