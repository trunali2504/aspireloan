package com.quix.aspireloan.service.mapper;

import com.quix.aspireloan.domain.*;
import com.quix.aspireloan.service.dto.RebateStatusDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity RebateStatus and its DTO RebateStatusDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RebateStatusMapper extends EntityMapper<RebateStatusDTO, RebateStatus> {

    

    

    default RebateStatus fromId(Long id) {
        if (id == null) {
            return null;
        }
        RebateStatus rebateStatus = new RebateStatus();
        rebateStatus.setId(id);
        return rebateStatus;
    }
}
