package com.quix.aspireloan.service.mapper;

import com.quix.aspireloan.domain.*;
import com.quix.aspireloan.service.dto.ReferralStatusDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ReferralStatus and its DTO ReferralStatusDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ReferralStatusMapper extends EntityMapper<ReferralStatusDTO, ReferralStatus> {

    

    

    default ReferralStatus fromId(Long id) {
        if (id == null) {
            return null;
        }
        ReferralStatus referralStatus = new ReferralStatus();
        referralStatus.setId(id);
        return referralStatus;
    }
}
