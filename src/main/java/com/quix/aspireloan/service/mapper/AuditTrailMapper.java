package com.quix.aspireloan.service.mapper;

import com.quix.aspireloan.domain.*;
import com.quix.aspireloan.service.dto.AuditTrailDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity AuditTrail and its DTO AuditTrailDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AuditTrailMapper extends EntityMapper<AuditTrailDTO, AuditTrail> {

    

    

    default AuditTrail fromId(Long id) {
        if (id == null) {
            return null;
        }
        AuditTrail auditTrail = new AuditTrail();
        auditTrail.setId(id);
        return auditTrail;
    }
}
