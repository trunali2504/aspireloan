package com.quix.aspireloan.service.mapper;

import com.quix.aspireloan.domain.*;
import com.quix.aspireloan.service.dto.RebateDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Rebate and its DTO RebateDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RebateMapper extends EntityMapper<RebateDTO, Rebate> {

    

    

    default Rebate fromId(Long id) {
        if (id == null) {
            return null;
        }
        Rebate rebate = new Rebate();
        rebate.setId(id);
        return rebate;
    }
}
