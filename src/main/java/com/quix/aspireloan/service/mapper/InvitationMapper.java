package com.quix.aspireloan.service.mapper;

import com.quix.aspireloan.domain.*;
import com.quix.aspireloan.service.dto.InvitationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Invitation and its DTO InvitationDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface InvitationMapper extends EntityMapper<InvitationDTO, Invitation> {

    

    

    default Invitation fromId(Long id) {
        if (id == null) {
            return null;
        }
        Invitation invitation = new Invitation();
        invitation.setId(id);
        return invitation;
    }
}
