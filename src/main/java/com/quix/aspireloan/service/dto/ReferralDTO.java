package com.quix.aspireloan.service.dto;


import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Referral entity.
 */
public class ReferralDTO implements Serializable {

    private Long id;

    private Integer numberofsuccessfullreferral;

    private String referralbonus;

    private LocalDate validityfromdate;

    private LocalDate validitytodate;

    private String createdBy;

    private LocalDate createdDate;

    private String updatedBy;

    private LocalDate updatedDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumberofsuccessfullreferral() {
        return numberofsuccessfullreferral;
    }

    public void setNumberofsuccessfullreferral(Integer numberofsuccessfullreferral) {
        this.numberofsuccessfullreferral = numberofsuccessfullreferral;
    }

    public String getReferralbonus() {
        return referralbonus;
    }

    public void setReferralbonus(String referralbonus) {
        this.referralbonus = referralbonus;
    }

    public LocalDate getValidityfromdate() {
        return validityfromdate;
    }

    public void setValidityfromdate(LocalDate validityfromdate) {
        this.validityfromdate = validityfromdate;
    }

    public LocalDate getValiditytodate() {
        return validitytodate;
    }

    public void setValiditytodate(LocalDate validitytodate) {
        this.validitytodate = validitytodate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public LocalDate getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(LocalDate updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReferralDTO referralDTO = (ReferralDTO) o;
        if(referralDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), referralDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ReferralDTO{" +
            "id=" + getId() +
            ", numberofsuccessfullreferral='" + getNumberofsuccessfullreferral() + "'" +
            ", referralbonus='" + getReferralbonus() + "'" +
            ", validityfromdate='" + getValidityfromdate() + "'" +
            ", validitytodate='" + getValiditytodate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            "}";
    }
}
