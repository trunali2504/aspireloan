package com.quix.aspireloan.service.dto;


import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Packages entity.
 */
public class PackagesDTO implements Serializable {

    private Long id;

    private LocalDate validityfromdate;

    private LocalDate validitytodate;

    private String typeOfProduct;

    private Integer interestrate;

    private Long maxloanamount;

    private Long minloanamount;

    private Integer maxtenure;

    private Integer mintenure;

    private String totalpayout;

    private String createdBy;

    private LocalDate createdDate;

    private String updatedBy;

    private LocalDate updatedDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getValidityfromdate() {
        return validityfromdate;
    }

    public void setValidityfromdate(LocalDate validityfromdate) {
        this.validityfromdate = validityfromdate;
    }

    public LocalDate getValiditytodate() {
        return validitytodate;
    }

    public void setValiditytodate(LocalDate validitytodate) {
        this.validitytodate = validitytodate;
    }

    public String getTypeOfProduct() {
        return typeOfProduct;
    }

    public void setTypeOfProduct(String typeOfProduct) {
        this.typeOfProduct = typeOfProduct;
    }

    public Integer getInterestrate() {
        return interestrate;
    }

    public void setInterestrate(Integer interestrate) {
        this.interestrate = interestrate;
    }

    public Long getMaxloanamount() {
        return maxloanamount;
    }

    public void setMaxloanamount(Long maxloanamount) {
        this.maxloanamount = maxloanamount;
    }

    public Long getMinloanamount() {
        return minloanamount;
    }

    public void setMinloanamount(Long minloanamount) {
        this.minloanamount = minloanamount;
    }

    public Integer getMaxtenure() {
        return maxtenure;
    }

    public void setMaxtenure(Integer maxtenure) {
        this.maxtenure = maxtenure;
    }

    public Integer getMintenure() {
        return mintenure;
    }

    public void setMintenure(Integer mintenure) {
        this.mintenure = mintenure;
    }

    public String getTotalpayout() {
        return totalpayout;
    }

    public void setTotalpayout(String totalpayout) {
        this.totalpayout = totalpayout;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public LocalDate getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(LocalDate updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PackagesDTO packagesDTO = (PackagesDTO) o;
        if(packagesDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), packagesDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PackagesDTO{" +
            "id=" + getId() +
            ", validityfromdate='" + getValidityfromdate() + "'" +
            ", validitytodate='" + getValiditytodate() + "'" +
            ", typeOfProduct='" + getTypeOfProduct() + "'" +
            ", interestrate='" + getInterestrate() + "'" +
            ", maxloanamount='" + getMaxloanamount() + "'" +
            ", minloanamount='" + getMinloanamount() + "'" +
            ", maxtenure='" + getMaxtenure() + "'" +
            ", mintenure='" + getMintenure() + "'" +
            ", totalpayout='" + getTotalpayout() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            "}";
    }
}
