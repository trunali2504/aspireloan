package com.quix.aspireloan.service.dto;


import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the RebateStatus entity.
 */
public class RebateStatusDTO implements Serializable {

    private Long id;

    private Integer applicationId;

    private Integer rebateamount;

    private String status;

    private String lastremark;

    private LocalDate applydate;

    private String createdBy;

    private LocalDate createdDate;

    private String updatedBy;

    private LocalDate updatedDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Integer applicationId) {
        this.applicationId = applicationId;
    }

    public Integer getRebateamount() {
        return rebateamount;
    }

    public void setRebateamount(Integer rebateamount) {
        this.rebateamount = rebateamount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLastremark() {
        return lastremark;
    }

    public void setLastremark(String lastremark) {
        this.lastremark = lastremark;
    }

    public LocalDate getApplydate() {
        return applydate;
    }

    public void setApplydate(LocalDate applydate) {
        this.applydate = applydate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public LocalDate getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(LocalDate updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RebateStatusDTO rebateStatusDTO = (RebateStatusDTO) o;
        if(rebateStatusDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), rebateStatusDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RebateStatusDTO{" +
            "id=" + getId() +
            ", applicationId='" + getApplicationId() + "'" +
            ", rebateamount='" + getRebateamount() + "'" +
            ", status='" + getStatus() + "'" +
            ", lastremark='" + getLastremark() + "'" +
            ", applydate='" + getApplydate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            "}";
    }
}
