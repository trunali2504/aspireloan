package com.quix.aspireloan.service.dto;


import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Rebate entity.
 */
public class RebateDTO implements Serializable {

    private Long id;

    private Integer loanFrom;

    private String loanTo;

    private Integer loanPercentage;

    private Integer maxamount;

    private LocalDate validityfromdate;

    private LocalDate validitytodate;

    private String product;

    private String createdBy;

    private LocalDate createdDate;

    private String updatedBy;

    private LocalDate updatedDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getLoanFrom() {
        return loanFrom;
    }

    public void setLoanFrom(Integer loanFrom) {
        this.loanFrom = loanFrom;
    }

    public String getLoanTo() {
        return loanTo;
    }

    public void setLoanTo(String loanTo) {
        this.loanTo = loanTo;
    }

    public Integer getLoanPercentage() {
        return loanPercentage;
    }

    public void setLoanPercentage(Integer loanPercentage) {
        this.loanPercentage = loanPercentage;
    }

    public Integer getMaxamount() {
        return maxamount;
    }

    public void setMaxamount(Integer maxamount) {
        this.maxamount = maxamount;
    }

    public LocalDate getValidityfromdate() {
        return validityfromdate;
    }

    public void setValidityfromdate(LocalDate validityfromdate) {
        this.validityfromdate = validityfromdate;
    }

    public LocalDate getValiditytodate() {
        return validitytodate;
    }

    public void setValiditytodate(LocalDate validitytodate) {
        this.validitytodate = validitytodate;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public LocalDate getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(LocalDate updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RebateDTO rebateDTO = (RebateDTO) o;
        if(rebateDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), rebateDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RebateDTO{" +
            "id=" + getId() +
            ", loanFrom='" + getLoanFrom() + "'" +
            ", loanTo='" + getLoanTo() + "'" +
            ", loanPercentage='" + getLoanPercentage() + "'" +
            ", maxamount='" + getMaxamount() + "'" +
            ", validityfromdate='" + getValidityfromdate() + "'" +
            ", validitytodate='" + getValiditytodate() + "'" +
            ", product='" + getProduct() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            "}";
    }
}
