package com.quix.aspireloan.service.dto;


import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the AuditTrail entity.
 */
public class AuditTrailDTO implements Serializable {

    private Long id;

    private String userCode;

    private LocalDate logDate;

    private String logTime;

    private String moduleName;

    private String action;

    private String actionstatus;

    private String itemcode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public LocalDate getLogDate() {
        return logDate;
    }

    public void setLogDate(LocalDate logDate) {
        this.logDate = logDate;
    }

    public String getLogTime() {
        return logTime;
    }

    public void setLogTime(String logTime) {
        this.logTime = logTime;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getActionstatus() {
        return actionstatus;
    }

    public void setActionstatus(String actionstatus) {
        this.actionstatus = actionstatus;
    }

    public String getItemcode() {
        return itemcode;
    }

    public void setItemcode(String itemcode) {
        this.itemcode = itemcode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AuditTrailDTO auditTrailDTO = (AuditTrailDTO) o;
        if(auditTrailDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), auditTrailDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AuditTrailDTO{" +
            "id=" + getId() +
            ", userCode='" + getUserCode() + "'" +
            ", logDate='" + getLogDate() + "'" +
            ", logTime='" + getLogTime() + "'" +
            ", moduleName='" + getModuleName() + "'" +
            ", action='" + getAction() + "'" +
            ", actionstatus='" + getActionstatus() + "'" +
            ", itemcode='" + getItemcode() + "'" +
            "}";
    }
}
